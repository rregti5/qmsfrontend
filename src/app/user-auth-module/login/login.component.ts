import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AppRoutesConstant} from "../../_constants/app-routes";
import { UserService } from '../../services/user/user.service';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { environment } from '@env/environment';
import { TranslateService } from '@ngx-translate/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  environnement: string = 'demo';
 language: string = 'fr';
  _email : string; 
  _password: string;
  email = new FormControl("",Validators.compose([
    Validators.required,
    Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z]+[.]+[a-zA-Z0-9.-]+$')
  ]));
  password = new FormControl("", Validators.compose([
    Validators.required,
    Validators.minLength(8),
  ]))
  errorMessage :string = null;
  nameForm: FormGroup;

  constructor(
    private _router : Router, 
    private userService : UserService,public translate: TranslateService) {      
      if(!Cookie.get('language')){
        Cookie.set('language', "fr");
      }
      
      translate.setDefaultLang('fr');
     
     }

  ngOnInit() {
    this.errorMessage = null;
    //this.nameForm = new FormGroup({
      
  }

  onUserList = () => {
    environment.env = this.environnement
    this.userService.authenticate(this._email.toLowerCase(),this._password).subscribe(data=>{
      //console.log(data.message!=null);
      if(!(data.message!=null)){
        this._router.navigate(['/' + AppRoutesConstant.DASHBOARD]);
        this.userService.token = data.token;
        Cookie.set('token',data.token,10);
        this.userService.currentUser.Email = data.Email;
        this.userService.currentUser.Name = data.Name;
        this.userService.currentUser._id = data._id;
        this.userService.currentUser.Entity = data.Entity;
        this.userService.currentUser.Status = data.Status;
        this.userService.currentUser.Role = data.Role;
        if(!Cookie.get('currentUserEmail')){
          Cookie.set('currentUserEmail', data.Email);
        }
        if(!Cookie.get('currentUserName')){
          Cookie.set('currentUserName', data.Name);
        }
        Cookie.set('currentUserId',data._id);
        Cookie.set('currentUserEntity',data.Entity);
        Cookie.set('currentUserStatus',data.Status);
        Cookie.set('currentUserRole',data.Role);
        Cookie.set('language',this.language);
      }
      else{
        this.errorMessage = data.message;
      }
    })

    //this._router.navigate(['/' + AppRoutesConstant.SELF_ASSESSMENT_VIEW]);
  }

}
