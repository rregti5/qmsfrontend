import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpInterceptors} from './http-interceptors/index-Interceptor';
import {SharedService} from '@services/shared.service';
import {APIManager} from '@services/apimanager.service';
import {SharedUserService} from '@services/shared-user.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {ConfirmationModalComponent, ProgressHudComponent} from "./shared-component";
import {MaterialModule} from "../material-module/material.module";
import {TruncatePipe} from "./pipes";
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule } from '@ngx-translate/core';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MaterialModule,
        TranslateModule
    ],
    declarations: [
        ProgressHudComponent,
        ConfirmationModalComponent,
        TruncatePipe,
    ],
    exports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        MaterialModule,
        ProgressHudComponent,
        TruncatePipe,
        ConfirmationModalComponent
    ],
    providers: [
        HttpInterceptors,
        SharedService,
        SharedUserService,
        APIManager
    ]
})
export class UtilityModule {
}
export function setTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
    }
