import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import {AppRoutesConstant} from "./_constants/app-routes";

const routes: Routes = [
  {
    path: '',
    redirectTo: AppRoutesConstant.HOME,
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: AppRoutesConstant.HOME,
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
  
 }
