import { Component, OnInit } from '@angular/core';
import { SelfAssessmentService } from 'app/feature-module/services';
import { AppRoutesConstant } from 'app/_constants/app-routes';
import { Router } from '@angular/router';
import { ScoreDescirptionColorList } from 'app/utility-module/shared-functions/common-functions';

@Component({
  selector: 'app-guide-qms',
  templateUrl: './guide-qms.component.html',
  styleUrls: ['./guide-qms.component.scss']
})
export class GuideQmsComponent implements OnInit {

  maximumTextLength = 140;
  panelOpenState = false;
  entityList = [];
  users: Array<any> = [];
  allUsers: Array<any> = [];

  readMoreDescription: boolean = false;;

  assessment: any;

  selectedElement: any = {index: 0, axe: 0};

  
  colorList = ScoreDescirptionColorList;

  constructor(
    private _selfAssessmentService : SelfAssessmentService,
    private _router : Router,
  ) { }

  ngOnInit() {
    //console.log("init")

    this._selfAssessmentService.getGuideQMSAssessment().subscribe(response=>{
      //console.log(response)

      if(!response.error) {
        this.assessment = response.data;
        this.assessment.Axes = this.assessment.Axes.filter((elem,indx)=>{
          return (elem.Visible == null || elem.Visible === true);
        })
        //console.log(this.assessment);
        this.assessment.Axes.sort((a, b) => {
          return compare(a.Description, b.Description, true);
        })
      }
    })
  }

  selectElement(i, element, index) {
    this.selectedElement = {
        index: index,
        element: element,
        axe: i
    }
  }

  onBack = () => {
    this._router.navigate(['/' + AppRoutesConstant.SELF_ASSESSMENT_LIST]);
  }

  onReadMore = (event, score) => {
      event.stopPropagation();
      this.readMoreDescription = score;
  }

  tabChanged(event) {
    this.selectedElement = {axe: event.index, index: 0};
  }

}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
