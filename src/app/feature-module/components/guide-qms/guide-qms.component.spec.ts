import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuideQmsComponent } from './guide-qms.component';

describe('GuideQmsComponent', () => {
  let component: GuideQmsComponent;
  let fixture: ComponentFixture<GuideQmsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuideQmsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuideQmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
