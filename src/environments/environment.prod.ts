export const environment = {
    production: true,
    env: 'demo',
    //apiUrl: 'http://localhost:2082/backend',
    //apiUrl: 'https://qms-ms-backend.herokuapp.com/backend',
    // apiUrl: 'https://qms.um6p.ma/backend', 
    //apiUrl: 'https://qmsbackend.ocpms.net/backend', 
    //apiUrl: 'http://10.16.128.24/backend', 
    apiUrl: 'https://qms-v1.um6p.ma/backend',
    //apiUrl: 'https://vibline.ocpms.net:2083/backend',
    //token:'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1YzhjZWUwZDc2ZmRiYTAwMTczMjdiZmQiLCJpYXQiOjE1NTI3NTgxMDd9.kpd-Zhpk6cW-xtmiAPqQfR-xiCXMJOxGVpu_uJSotqA'

    firebase: {
        apiKey: "AIzaSyAyFkFrk6dfk3J2yRhG3mAOXXyQqtqiC48",
        authDomain: "qm-qms.firebaseapp.com",
        databaseURL: "https://qm-qms.firebaseio.com",
        projectId: "qm-qms",
        storageBucket: "qm-qms.appspot.com",
        messagingSenderId: "533503653759"
    }
};
