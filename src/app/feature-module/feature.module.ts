import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { FileUploadModule } from 'ng2-file-upload';
import {FeatureRoutingModule} from './feature-routing.module';


import {ChartsModule} from "ng2-charts";

import { SlickCarouselModule } from 'ngx-slick-carousel';
import {
    AssessmentPlanDetailsComponent,
    AssessmentPlanListComponent,
    AxesDetailsComponent,
    AxesListComponent,
    AxesUpdateComponent,
    DashboardComponent,
    EntitiesDetailsComponent,
    EntitiesListComponent,
    HeaderComponent,
    ChangePasswordModalComponent,
    SelfAssessmentListComponent,
    SelfAssessmentViewComponent,
    UserDetailsComponent,
    UserListComponent,
    ActionsComponent
} from './components';

import {FormsModule} from '@angular/forms';
import {UtilityModule} from "@utility-module";

import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import { NgxEchartsModule } from 'ngx-echarts';
import { HttpModule } from '@angular/http';
import { GuideQmsComponent } from './components/guide-qms/guide-qms.component';
import { BestPracticeComponent } from './components/best-practice/best-practice.component';
import { MaterialModule } from 'app/material-module/material.module';
import { HttpClient,HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule } from '@ngx-translate/core';

export function setTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
    }
@NgModule({
    declarations: [
        UserListComponent,
        UserDetailsComponent,
        HeaderComponent,
        ActionsComponent,
        AxesListComponent,
        AxesDetailsComponent,
        AxesUpdateComponent,
        EntitiesListComponent,
        EntitiesDetailsComponent,
        AssessmentPlanListComponent,
        AssessmentPlanDetailsComponent,
        SelfAssessmentViewComponent,
        DashboardComponent,
        SelfAssessmentListComponent,
        GuideQmsComponent,
        BestPracticeComponent,
        ChangePasswordModalComponent
    ],
    entryComponents: [ChangePasswordModalComponent],
    imports: [
        SlickCarouselModule,
        FileUploadModule,
        UtilityModule,
        CommonModule,
        FeatureRoutingModule,
        FormsModule,
        NgxDatatableModule,
        HttpModule,
        NgxEchartsModule,
        MaterialModule,
        TranslateModule,
        ChartsModule
    ],
    exports: [
        HeaderComponent
    ]
})
export class FeatureModule {
}
