import { Component, OnInit } from '@angular/core';
import {AppRoutesConstant} from "../_constants/app-routes";
import {Entity} from "../models/Entity";
import {UserService} from "../services/user/user.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  isMenuCollapse = false;
  connected = false;
  slides = [
    {img: 'assets/images/home/slide14.JPG'},
    {img: 'assets/images/home/slide1.jpg'},
    {img: 'assets/images/home/slide2.JPG'},
    {img: 'assets/images/home/slide3.JPG'},
    {img: 'assets/images/home/slide4.JPG'},
    {img: 'assets/images/home/slide5.JPG'},
    {img: 'assets/images/home/slide6.JPG'},
    {img: 'assets/images/home/slide7.JPG'},
    {img: 'assets/images/home/slide8.JPG'},
    {img: 'assets/images/home/slide9.JPG'},
    {img: 'assets/images/home/slide10.JPG'},
    {img: 'assets/images/home/slide11.JPG'},
    {img: 'assets/images/home/slide12.JPG'},
    {img: 'assets/images/home/slide13.JPG'}

  ];
  slideConfig = {  infinite: true,
    autoplay: true,
    speed: 1000,
    dots: true,
    arrows: false,
    slidesToShow: 1,
    adaptiveHeight: true};
  slideConfigTesti = {
    slidesToShow: 3,
    arrows: false,
    slidesToScroll: 1,
    autoplay: true,
    responsive: [{
      breakpoint: 850,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
      }
    }]
  };
  constructor(private _userService: UserService
  ) { }
  ngOnInit() {

    this._userService.getCurrentUser().subscribe((user) => {
      if (user) {
          this.connected = true;
      } else {

      }
    });
  }
  get login() {
    return ['/' + AppRoutesConstant.LOGIN];
  }
  get dash() {
    return ['/' + AppRoutesConstant.DASHBOARD];
  }
  downloadFile(){
    let link = document.createElement("a");
    link.download = "Booklet Management de Qualite.pdf";
    link.href = "assets/files/Booklet Management de Qualite.pdf";
    link.click();
  }

}
