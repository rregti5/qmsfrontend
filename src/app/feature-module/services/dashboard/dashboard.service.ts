import { Injectable } from '@angular/core';
import {throwError as observableThrowError, Observable, Subject} from 'rxjs';
import {HttpMethodsTypeEnum, QMSApi} from "@constants/*";
import {APIManager} from "@services/apimanager.service";

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
      private _apiManager: APIManager
  ) { }




  getAssessmentsScores = (Assessments): Observable<any> => {
    return this._apiManager.httpHelperMethod(
        HttpMethodsTypeEnum.POST, QMSApi.DASHBOARD_SCORE, {'Assessments':Assessments},
        this._apiManager.Authorised_HttpOptions, false, true);
  };
}

