import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AxesPdfComponent } from './axes-pdf.component';

describe('AxesPdfComponent', () => {
  let component: AxesPdfComponent;
  let fixture: ComponentFixture<AxesPdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AxesPdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AxesPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
