import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {APIManager} from "@services/apimanager.service";
import {HttpMethodsTypeEnum, QMSApi} from "@constants/*";

@Injectable({
    providedIn: 'root'
})
export class EntitiesService {

    constructor(private _apiManager: APIManager) {
    }

    getAllEntities = (): Observable<any> => {
        return this._apiManager.httpHelperMethod(
            HttpMethodsTypeEnum.GET, QMSApi.ENTITY_ALL, {},
            this._apiManager.Authorised_HttpOptions, false, true);
    };


    getEntityById = (id): Observable<any> => {
        return this._apiManager.httpHelperMethod(
            HttpMethodsTypeEnum.GET, `${QMSApi.ENTITY_BYID}/${id}`, {},
            this._apiManager.Authorised_HttpOptions, false, true);
    };

    getEntitiesById = (params): Observable<any> => {
        return this._apiManager.httpHelperMethod(
            HttpMethodsTypeEnum.POST, `${QMSApi.ENTITY_BY_IDS}`, params,
            this._apiManager.Authorised_HttpOptions, false, true);
    };
}
