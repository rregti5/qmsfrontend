import {Injectable} from '@angular/core';
import {Http, RequestOptionsArgs, Headers} from '@angular/http';
import {throwError as observableThrowError, Observable, Subject} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import { Action } from 'app/models/action';
import { environment } from '@env/environment';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Injectable({
  providedIn: 'root'
})
export class ActionsService {

  constructor(
    private http: Http
  ) { }

  url = environment.apiUrl + '/actions/';

  createAction(action:Action){
    let options:RequestOptionsArgs = {
      headers: new Headers({
        Authorization : 'Bearer '+Cookie.get('token'),
        env : environment.env
      })
    }
    let body = {
      Attachment: action.Attachment,
      Comment: action.Comment,
      CreatedBy: action.CreatedBy,
      CreatedDate: action.CreatedDate,
      Description: action.Description,
      DueDate: action.DueDate,
      Axe: action.Axe,
      Element: action.Element,
      Entity: action.Entity,
      Status: action.Status,
      Who: action.Who,
    }

    console.log(body)

    return this.http.post(this.url+'action_Create',body,options).pipe(
      map((res: any) => {
        return res.json();
      }),
      catchError((error: any) => {
          return observableThrowError(error.json ? error.json().error : error || 'server error');
      }),
    )
  }

  getAllActions(){
    let options:RequestOptionsArgs = {
      headers: new Headers({
        Authorization : 'Bearer '+Cookie.get('token'),
        env : environment.env
      })
    }

    return this.http.post(this.url+'action_List',{},options).pipe(
      map((res: any) => {
        return res.json();
      }),
      catchError((error: any) => {
          return observableThrowError(error.json ? error.json().error : error || 'server error');
      }),
    )
  }

  getActions(actions){
    let options:RequestOptionsArgs = {
      headers: new Headers({
        Authorization : 'Bearer '+Cookie.get('token'),
        env : environment.env
      })
    }
    let body = {
      actions : actions
    }

    return this.http.post(this.url+'action_List',body,options).pipe(
      map((res: any) => {
        return res.json();
      }),
      catchError((error: any) => {
          return observableThrowError(error.json ? error.json().error : error || 'server error');
      }),
    )
  }

  getActionsByAssessment(assessment){
    let options:RequestOptionsArgs = {
      headers: new Headers({
        Authorization : 'Bearer '+Cookie.get('token'),
        env : environment.env
      })
    }


    return this.http.get(this.url+'assessment/' + assessment,options).pipe(
      map((res: any) => {
        return res.json();
      }),
      catchError((error: any) => {
          return observableThrowError(error.json ? error.json().error : error || 'server error');
      }),
    )
  }

  updateAction(action){
    let options:RequestOptionsArgs = {
      headers: new Headers({
        Authorization : 'Bearer '+Cookie.get('token'),
        
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
        env : environment.env
      })
    }

    return this.http.put(this.url+'action_update/'+action._id,action,options).pipe(
      map((res: any) => {
        return res.json();
      }),
      catchError((error: any) => {
          return observableThrowError(error.json ? error.json().error : error || 'server error');
      }),
    )
  }

  updateActionAndEmail(action){
    let options:RequestOptionsArgs = {
      headers: new Headers({
        Authorization : 'Bearer '+Cookie.get('token'),
        
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
        env : environment.env
      })
    }

    return this.http.put(this.url+'action_update_and_email/'+action._id,action,options).pipe(
      map((res: any) => {
        return res.json();
      }),
      catchError((error: any) => {
          return observableThrowError(error.json ? error.json().error : error || 'server error');
      }),
    )
  }

  updateActions(acts){
    let actions = [];
    acts.forEach(action => {
      actions.push({
        "Attachment": action.Attachment,
        "Submitted": action.Submitted,
        "Comment": action.Comment,
        "CreatedBy": action.CreatedBy,
        "CreatedDate": action.AttachmentCReatedDate,
        "Description": action.Description,
        "DueDate": action.DueDate,
        "Entity": action.Entity,
        "Axe": action.Axe,
        "WhoMail":action.WhoMail,
        "Element": action.Element,
        "Status": action.Status,
        "Who": action.Who,
        "id": action.id,
        "_id": action._id,
      })
    });
    let options:RequestOptionsArgs = {
      headers: new Headers({
        Authorization : 'Bearer '+Cookie.get('token'),
        env : environment.env,
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest'
      })
    }
    let body={
      actions: actions
    }

    return this.http.post(this.url+'actions_update',body,options).pipe(
      map((res: any) => {
        return res.json();
      }),
      catchError((error: any) => {
          return observableThrowError(error.json ? error.json().error : error || 'server error');
      }),
    )
  }

  updateActionsAndEmail(acts){
    let actions = [];
    acts.forEach(action => {
      actions.push({
        "Attachment": action.Attachment,
        "Comment": action.Comment,
        "Submitted": action.Submitted,
        "CreatedBy": action.CreatedBy,
        "CreatedDate": action.AttachmentCReatedDate,
        "Description": action.Description,
        "DueDate": action.DueDate,
        "Entity": action.Entity,
        "Axe": action.Axe,
        "Element": action.Element,
        "Status": action.Status,
        "Who": action.Who,
        "WhoMail":action.WhoMail,
        "id": action.id,
        "_id": action._id,
      })
    });
    let options:RequestOptionsArgs = {
      headers: new Headers({
        Authorization : 'Bearer '+Cookie.get('token'),
        env : environment.env,
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest'
      })
    }
    let body={
      actions: actions
    }

    return this.http.post(this.url+'actions_update_and_email',body,options).pipe(
      map((res: any) => {
        return res.json();
      }),
      catchError((error: any) => {
          return observableThrowError(error.json ? error.json().error : error || 'server error');
      }),
    )
  }

  deleteAction(params){
    let options:RequestOptionsArgs = {
      headers: new Headers({
        Authorization : 'Bearer '+Cookie.get('token'),
        env : environment.env,
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest'
      })
    }
    return this.http.delete(this.url+'action_delete/'+params,options).pipe(
      map((res: any) => {
        return res.json();
      }),
      catchError((error: any) => {
          return observableThrowError(error.json ? error.json().error : error || 'server error');
      }),
    )
  }

  getEntityOpenActions(entity){
    let options:RequestOptionsArgs = {
      headers: new Headers({
        Authorization : 'Bearer '+Cookie.get('token'),
        env : environment.env
      })
    }

    return this.http.get(this.url+'open/'+entity,options).pipe(
      map((res: any) => {
        return res.json();
      }),
      catchError((error: any) => {
          return observableThrowError(error.json ? error.json().error : error || 'server error');
      }),
    )
  }

  getEntityActions(entity){
    let options:RequestOptionsArgs = {
      headers: new Headers({
        Authorization : 'Bearer '+Cookie.get('token'),
        env : environment.env
      })
    }

    return this.http.get(this.url+'entity/'+entity,options).pipe(
      map((res: any) => {
        return res.json();
      }),
      catchError((error: any) => {
          return observableThrowError(error.json ? error.json().error : error || 'server error');
      }),
    )
  }

  getEntitiesActions(entities){
    let body={
      entities: entities
    }
    let options:RequestOptionsArgs = {
      headers: new Headers({
        Authorization : 'Bearer '+Cookie.get('token'),
        env : environment.env,
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest'
      })
    }

    return this.http.post(this.url+'entities',body,options).pipe(
      map((res: any) => {
        return res.json();
      }),
      catchError((error: any) => {
          return observableThrowError(error.json ? error.json().error : error || 'server error');
      }),
    )
  }
}
