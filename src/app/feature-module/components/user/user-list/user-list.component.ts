import { Component, OnInit, ViewChild } from '@angular/core';
import {Router} from "@angular/router";
import {AppRoutesConstant} from "../../../../_constants/app-routes";
import { UserService } from '../../../../services/user/user.service';
import { User } from '../../../../models/user';
import { EntititesService } from '../../../../services/entities/entitites-service.service';
import { PAGE_SIZE_OPTIONS } from '@constants/*';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { TranslateService } from '@ngx-translate/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';

/* interface PeriodicElement {
  position: number;
  name: string;
  email: string;
  role: string;
  entity: string;
  status: string;
  action: any;
} */

const connectedContactsTableColumns = [
  { name: 'Name' },
  { name: 'Email' },
  { 
    name: 'Groups',
    sortable: false 
  },
  { 
    name: 'Device',
    sortable: false
  },
  { name: 'State' },
  {
    name: 'Action',
    resizeable: false,
    sortable: false
  }
];

const ELEMENT_DATA: User[] =[] /* [
  {position: 1, name: 'John Doe', email: 'johndoe@gmail.com', role: 'Admin', entity : 'Entity name 1', status: 'Active', action: ''},
  {position: 2, name: 'John Doe', email: 'johndoe@gmail.com', role: 'Admin', entity : 'Entity name 2', status: 'Active', action: ''},
  {position: 3, name: 'John Doe', email: 'johndoe@gmail.com', role: 'Admin', entity : 'Entity name 3', status: 'Active', action: ''},
  {position: 4, name: 'John Doe', email: 'johndoe@gmail.com', role: 'Admin', entity : 'Entity name 4', status: 'In active', action: ''},
  {position: 5, name: 'John Doe', email: 'johndoe@gmail.com', role: 'Global Admin', entity : 'Entity name 5', status: 'Active', action: ''},
  {position: 6, name: 'John Doe', email: 'johndoe@gmail.com', role: 'Admin', entity : 'Entity name 6', status: 'Active', action: ''},
  {position: 7, name: 'John Doe', email: 'johndoe@gmail.com', role: 'Admin', entity : 'Entity name 7', status: 'Active', action: ''},
  {position: 8, name: 'John Doe', email: 'johndoe@gmail.com', role: 'User', entity : 'Entity name 8', status: 'Active', action: ''},
  {position: 9, name: 'John Doe', email: 'johndoe@gmail.com', role: 'Admin', entity : 'Entity name 9', status: 'Active', action: ''},
  {position: 10, name: 'John Doe', email: 'johndoe@gmail.com', role: 'Admin', entity : 'Entity name 10', status: 'Active', action: ''}
]; */

@Component({
  selector: 'app-user-list',
  templateUrl: 'user-list.component.html',
  styleUrls: ['user-list.component.scss']
})

export class UserListComponent implements OnInit {

  displayedColumns: string[] = ['Name', 'Email', 'Role', 'Entity', 'Status', 'Action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);
  //hasPermission = true;
  isDeleteUser : boolean = false;
  currentUser : User;
  entitiesCodes: Array<any>=[];

  isLoadingResults = true;
  pageSizeOptions = PAGE_SIZE_OPTIONS;
  pageSize = PAGE_SIZE_OPTIONS[1];
  pageIndex = 0;
  resultsLength = 0;

  @ViewChild(MatSort) sort: MatSort;

  color : string;

  scrollBarHorizontal = (window.innerWidth < 860);
  //  columnModeSetting = (window.innerWidth < 860) ? 'standard':'force';
    columnModeSetting = 'force';
  
  
    messages = {
      // Message to show when array is presented
      // but contains no values
      emptyMessage: 'No users to show',
    ​
      // Footer total message
      totalMessage: ' Users'
    };

  constructor(private _router : Router, 
    public userService:UserService,
    private entitiesService: EntititesService,public translate: TranslateService) {
      if(Cookie.get('language')!==null)
      translate.setDefaultLang(Cookie.get('language')); }

  ngOnInit() {
    this.userService.getCurrentUser().subscribe(res=>{
      //console.log(this.userService.currentUser);
      this.userService.currentUser = res;
      if(this.userService.currentUser.Role==3){
        //console.log(this.userService.currentUser.Role);
        this.entitiesService.getAllEntities().subscribe(data=>{
          //console.log(data);
          this.updateUsersList();
        })
        //this.updateUsersList();
      }
      else{
        //this.hasPermission = false;
        this.entitiesService.getAllEntities().subscribe(data=>{
          let entities = [];
          data.entities.forEach(entity=>{
            if(this.userService.currentUser.Entity.includes(entity._id)){
              entities.push(entity);
            }
          })
          this.entitiesService.entities = entities;
          this.updateUsersList();
        })
      }
    })
  }

  onAddsUser = (type,user?) => {
    this.userService.addType = type;
    if(user!=null){
      this.userService.userToEdit = user;
    }
    this._router.navigate(['/' + AppRoutesConstant.USER_DETAILS])
  }
  
  updateUsersList(){
    let users: Array<User> = [];
    this.userService.getAllUsers().subscribe(data=>{
      this.resultsLength = data.length;
      
      data.forEach((element,i) => {
        let user: User = new User(element.Name,element.Email,element._id,element.Entity,element.Role,null,element.Status,element.LikedAttachments)
        if(this.userService.currentUser.Role === 3) users.push(user);
        else{
          let flag = false;
          this.entitiesService.entities.forEach(entity=>{
            if( (user.Entity.includes(entity._id) && user.Role == 1 ) || user._id === this.userService.currentUser._id ) flag=true;
          });
          if (flag) users.push(user);
        }
        
        if(i === ( data.length - 1 ) ){
          this.userService.users = users;
          //console.log(this.userService.users)
          this.dataSource.data = this.userService.users;
          this.dataSource.sort = this.sort;
          this.getEntityName(users);
          this.dataSource.data = this.userService.users.filter((elem, index) => {
            return index < this.pageSize
          })
          //console.log(this.entitiesCodes)
        }
      });
    })

    
  }

  deleteUser(){
    this.userService.deleteUSer(this.currentUser).subscribe(data=>{
      //console.log(data);
      this.updateUsersList();
      this.isDeleteUser = false;
    })
  }

  getEntityName(users){
    this.entitiesCodes = [];
    let codes = [];
    this.entitiesService.getAllEntities().subscribe((data)=>{
      users.forEach(user=>{
        if (user.Entity){
          user.Entity.forEach(userEntity=>{
            data.entities.forEach(entity=>{
              if(entity._id === userEntity){
                codes.push(entity.Code);
              }
            })
          })
          this.entitiesCodes.push(codes.join(' | '));
          codes = [];
        }
        else{
          this.entitiesCodes.push("");
        }
      })

    })
    
  }

  changePage = (event) => {
    const {pageIndex, pageSize} = event;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.bindDisplayAssessmentsList();
  }

  bindDisplayAssessmentsList = () => {
      this.dataSource.data = this.userService.users.filter((elem, index) => {
          return index >= this.pageIndex * this.pageSize && index < ((this.pageIndex + 1) * this.pageSize);
      })
    this.dataSource.sort = this.sort;
  }

  sortData(sort: Sort) {
    const data = this.userService.users.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource.data = data;
      this.bindDisplayAssessmentsList();
      return;
    }

    this.dataSource.data = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'Name': return compare(a.Name, b.Name, isAsc);
        case 'Role': return compare(a.Role, b.Role, isAsc);
        case 'Email': return compare(a.Email, b.Email, isAsc);
        default: return 0;
      }
    });
    this.bindDisplayAssessmentsList();
    
  }

}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
