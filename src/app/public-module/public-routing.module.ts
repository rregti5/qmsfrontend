import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AxesPdfComponent} from "./axes-pdf/axes-pdf.component";
import {AppRoutesConstant} from "../_constants/app-routes";

const routes: Routes = [
  {
    path: AppRoutesConstant.PDF_AXES_REPORT,
    component: AxesPdfComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
