import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AxesDetailsComponent } from './axes-details.component';

describe('AxesDetailsComponent', () => {
  let component: AxesDetailsComponent;
  let fixture: ComponentFixture<AxesDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AxesDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AxesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
