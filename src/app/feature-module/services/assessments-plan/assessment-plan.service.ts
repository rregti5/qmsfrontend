import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {APIManager} from "@services/apimanager.service";
import {HttpMethodsTypeEnum, QMSApi} from "@constants/*";

@Injectable({
    providedIn: 'root'
})
export class AssessmentPlanService {

    constructor(private _apiManager: APIManager) {
    }

    assessmentPlans : Array<any> =[];

    createUpdateSelfAssessmentPlan = (params, uuid: string): Observable<any> => {
        const methodType = uuid ? HttpMethodsTypeEnum.PUT : HttpMethodsTypeEnum.POST;
        const endPoint = uuid ? `${QMSApi.ASSESSMENT_PLAN_UPDATE}/${uuid}` : QMSApi.ASSESSMENT_PLAN_CREATE;
        return this._apiManager.httpHelperMethod(
            methodType, endPoint, params,
            this._apiManager.Authorised_HttpOptions, true, true);
    };


    getSingleAssessmentPlanDetail = (id): Observable<any> => {
        return this._apiManager.httpHelperMethod(
            HttpMethodsTypeEnum.GET, `${QMSApi.ASSESSMENT_PLAN_SINGLE}/${id}`, {},
            this._apiManager.Authorised_HttpOptions, false, true);
    };


    removeAssessmentPlan = (id): Observable<any> => {
        return this._apiManager.httpHelperMethod(
            HttpMethodsTypeEnum.DELETE, `${QMSApi.ASSESSMENT_PLAN_REMOVE}/${id}`, {},
            this._apiManager.Authorised_HttpOptions_JSON, true, true);
    };


    getAllAssessmentPlans = (): Observable<any> => {
        return this._apiManager.httpHelperMethod(
            HttpMethodsTypeEnum.GET, QMSApi.ASSESSMENT_PLAN_ALL, {},
            this._apiManager.Authorised_HttpOptions, false, true);
    };
}
