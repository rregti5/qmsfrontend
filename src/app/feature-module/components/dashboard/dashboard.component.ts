import {Component, OnInit, ViewChild, OnDestroy} from '@angular/core';
import {EChartOption} from 'echarts';
import {forkJoin, Observable} from "rxjs";
import {DashboardService, EntitiesService, SelfAssessmentService, AssessmentPlanService} from "../../services";
import {UserService} from "../../../services/user/user.service";

import {EntityModel} from "../../models";
import { Router } from '@angular/router';
import { MatTreeFlatDataSource, MatTreeFlattener, MatTableDataSource, MatSort } from '@angular/material';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Entity } from 'app/models/Entity';
import { EntititesService } from 'app/services/entities/entitites-service.service';
import { PAGE_SIZE_OPTIONS } from '@constants/*';
import { ActionsService } from 'app/services/actions/actions.service';
import { TranslateService } from '@ngx-translate/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';

interface Action {
    Axe: string;
    Element: string;
    Description: string;
    Who: string;
    DueDate: string;
    Status: boolean;
    Comment:boolean;
}

const ELEMENT_DATA: Action[] = [];

interface Entit {
    entity: Entity;
    children?: Entit[];
}

var TREE_DATA: Entit[] = [];

/** Flat node with expandable and level information */
interface ExampleFlatNode {
    expandable: boolean;
    name: string;
    level: number;
}

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
    selectedValueAxe = 0;
    selectedAssess = 0;
    Entities: EntityModel[] = [];
    Axes: any
    user: any
    displayedColumns: string[] = [ 'axe', 'elements', 'action', 'who', 'dueDate','status','comment'];
    dataSource = new MatTableDataSource(ELEMENT_DATA);

      // Pagination Variables
    isLoadingResults = true;
    pageSizeOptions = PAGE_SIZE_OPTIONS;
    pageSize = PAGE_SIZE_OPTIONS[0];
    pageIndex = 0;
    resultsLength = 0;

    chartOption: EChartOption;
    chartElements: EChartOption;
    dataAxe = [];
    goalAxeValues = [];
    dataAxeValues = [];
    goalElementValues = [];
    dataElement = [];
    dataElementValues = [];
    Assessments = [];
    selectedEntity:string = 'all';
    actions : Array<any> = [];

    public currUser;
     
    @ViewChild(MatSort) sort: MatSort;

    constructor(private _selfAssessmentService: SelfAssessmentService,
                private _dashboardService: DashboardService, private _entitieService: EntitiesService,
                private _userService: UserService,
                private _entitiesService: EntititesService,
                private _assessmentPlanService : AssessmentPlanService,
                public _actionsService : ActionsService,
                public router:Router,public translate: TranslateService) {
                    if(Cookie.get('language')!==null)
                    translate.setDefaultLang(Cookie.get('language'));
                   

    }

    ngOnInit() {
      
        this._userService.getCurrentUser().subscribe((user) => {
            this.user = user;
            this.currUser = user;
            if (this.user.Role <= 2) {
                const entityObj = {entities: this.user.Entity};
                this.getEntitiesByIds(entityObj).subscribe((response) => {
                    this.handleEntitiesResponse(response);
                    let entities = [];
                    response.entities.forEach(entity=>{
                        if(entity)entities.push(entity._id)
                    })
                    this.displayLastFiveAssessments(null,entities);
                });
            } else {
                this.getEntities().subscribe((response) => {
                    this.handleEntitiesResponse(response);
                    let entities = [];
                    response.entities.forEach(entity=>{
                        entities.push(entity._id)
                    })
                    this.displayLastFiveAssessments(null,null);
                    
                });
            }

            

            this._assessmentPlanService.getAllAssessmentPlans().subscribe(data=>{
                //console.log(data);
                this._assessmentPlanService.assessmentPlans = data.data;
                this._entitiesService.getAllEntities().subscribe((data)=>{
                    this._entitiesService.userEntities = [];
                    data.entities.forEach(element => {
                        let ent = new Entity(element._id, element.Code, element.Description, element.Type, element.SubEntities, element.AssessmentPlan, element.SelfAssessment, element.Score, element.CalculatedScore, element.ScoreGoal);
                        if(this.user.Role > 2 || ( this.user.Role <= 2 && this.user.Entity.includes(element._id) ) ) {
                            this._entitiesService.userEntities.push(ent);
                        }
                    });
                    this._entitiesService.entities = this._entitiesService.userEntities;
                     //console.log(this._entitiesService.entities);
                    //console.log(data); 
                    
                    this.initData();
                });
            })
        });
    }

    handleEntitiesResponse = (response) => {
        this.Entities = response.entities;
    }

    onSelectionChangeEntity = (event) => {
        this.selectedEntity = event.value;
        if(event.value === 'all'){
            /* let entities = [];
            this.Entities.forEach(entity=>{
                entities.push(entity._id)
            }) */
            this.displayLastFiveAssessments(null,null);
            this.selectedAssess = 0;
        }
        else{
            this.displayLastFiveAssessments(event.value);
            this.selectedAssess = 0;

        }
    }

    displayLastFiveAssessments = (entity: string, entities?:string[]) => {
        if (entity) {
            this.getLastFiveAssessmentsByEntityId({entity:entity}).subscribe(response => {
                let assessments = [];
                if(response.assesments.length != 0){
                    response['assesments'].forEach((dat) => {
                        assessments.push(dat.id)
                    });
                    this._dashboardService.getAssessmentsScores(assessments).subscribe(response => {
                        assessments = response.data;
                        //console.log(assessments)
                     
                        const entityObj = {
                            "entities": [assessments.map(elem => elem.assesment_entity)][0]
                        };
                        this.getEntitiesByIds(entityObj).subscribe((response) => {
                            assessments = assessments.map((elem, index) => {
                                elem.assesment_entity = response['entities'][index].Description;
                                return elem
                            });
                            this.Assessments = assessments;
                            this.bindLastAssessments();
                            const dataAxe = (this.dataAxe && this.dataAxe[0]) || [];
                            const dataAxeValues = (this.dataAxeValues && this.dataAxeValues[0]) || [];
                            const goalAxeValues = (this.goalAxeValues && this.goalAxeValues[0]) || [];
                            const dataElement = (this.dataElement && this.dataElement[0] && this.dataElement[0][0]) || [];
                            const dataElementValues = (this.dataElementValues && this.dataElementValues[0] && this.dataElementValues[0][0]) || [];
                            const goalElementValues = (this.goalElementValues && this.goalElementValues[0] && this.goalElementValues[0][0]) || [];

                            this.chartOption = this.chartOptionSet(this.chartOption, dataAxe, dataAxeValues, goalAxeValues)
                            this.chartElements = this.chartOptionSet(this.chartElements, dataElement, dataElementValues, goalElementValues)
                        });
                        
                    });
                }
                else{
                    this.Assessments = assessments;
                    
                    
                    /* this.bindLastAssessments();
                    const dataAxe = [];
                    const dataAxeValues = [];
                    const dataElement = [];
                    const dataElementValues = [];
                    this.chartOption = this.chartOptionSet(this.chartOption, dataAxe, dataAxeValues, this.goalAxeValues)
                    this.chartElements = this.chartOptionSet(this.chartElements, dataElement, dataElementValues, null) */
                }
            });
        }
        else if(entities){
            this.getLastFiveAssessmentsByEntitiesId({entities: entities}).subscribe(response => {
                let assessments = []
                response['data'].forEach((dat) => {
                    assessments.push(dat.id)
                });
                this._dashboardService.getAssessmentsScores(assessments).subscribe(response => {
                    assessments = response.data;
                    const entityObj = {
                        "entities": [assessments.map(elem => elem.assesment_entity)][0]
                    };
                    this.getEntitiesByIds(entityObj).subscribe((response) => {

                        assessments = assessments.map((elem, index) => {
                            elem.assesment_entity = response['entities'][index].Description;
                            return elem
                        });
                        this.Assessments = assessments;
                        console.log(this.Assessments[0]);
                        console.log(this.Assessments[0]['createdBy']);
                        this.bindLastAssessments();
                        const dataAxe = (this.dataAxe && this.dataAxe[0]) || [];
                        const dataAxeValues = (this.dataAxeValues && this.dataAxeValues[0]) || [];
                        const goalAxeValues = (this.goalAxeValues && this.goalAxeValues[0]) || [];
                        const dataElement = (this.dataElement && this.dataElement[0] && this.dataElement[0][0]) || [];
                        const dataElementValues = (this.dataElementValues && this.dataElementValues[0] && this.dataElementValues[0][0]) || [];
                        const goalElementValues = (this.goalElementValues && this.goalElementValues[0] && this.goalElementValues[0][0]) || [];

                        this.chartOption = this.chartOptionSet(this.chartOption, dataAxe, dataAxeValues, goalAxeValues)
                        this.chartElements = this.chartOptionSet(this.chartElements, dataElement, dataElementValues, goalElementValues)
                    });
                });
            });
        } else{
            this.getLastFiveAssessmentsByEntitiesId({}).subscribe(response => {
                let assessments = []
                response['All assesments'].forEach((dat) => {
                    assessments.push(dat.id)
                });
                this._dashboardService.getAssessmentsScores(assessments).subscribe(response => {
                    assessments = response.data;
                    const entityObj = {
                        "entities": [assessments.map(elem => elem.assesment_entity)][0]
                    };
                    this.getEntitiesByIds(entityObj).subscribe((response) => {

                        assessments = assessments.map((elem, index) => {
                            elem.assesment_entity = response['entities'][index].Description;
                            return elem
                        });
                        this.Assessments = assessments;
                        console.log(this.Assessments[0]);
                        console.log(this.Assessments[0]['createdBy']);
                        this.bindLastAssessments();
                        const dataAxe = (this.dataAxe && this.dataAxe[0]) || [];
                        const dataAxeValues = (this.dataAxeValues && this.dataAxeValues[0]) || [];
                        const goalAxeValues = (this.goalAxeValues && this.goalAxeValues[0]) || [];
                        const dataElement = (this.dataElement && this.dataElement[0] && this.dataElement[0][0]) || [];
                        const dataElementValues = (this.dataElementValues && this.dataElementValues[0] && this.dataElementValues[0][0]) || [];
                        const goalElementValues = (this.goalElementValues && this.goalElementValues[0] && this.goalElementValues[0][0]) || [];

                        this.chartOption = this.chartOptionSet(this.chartOption, dataAxe, dataAxeValues, goalAxeValues)
                        this.chartElements = this.chartOptionSet(this.chartElements, dataElement, dataElementValues, goalElementValues)
                    });
                });
            });
        }

    }

    reloadChartElements(id) {
        this.chartElements = this.chartOptionSet(this.chartElements, this.dataElement[this.selectedAssess][id], this.dataElementValues[this.selectedAssess][id], this.goalElementValues[this.selectedAssess][id])
    }

    reloadChartAxe(assessment) {
        if(assessment == 4){
            this.navigateAssessment();
        }
        else{
            this.selectedAssess = assessment;
            this.Axes = []
            this.dataAxe[assessment].forEach((dat) => {
                this.Axes.push(dat.text)
            })
            this.Axes = this.Axes.sort((a,b)=>{
                compare(a, b, true);
            })
            this.chartOption = this.chartOptionSet(this.chartOption, this.dataAxe[assessment], this.dataAxeValues[assessment], this.goalAxeValues[assessment])
            this.chartElements = this.chartOptionSet(this.chartElements, this.dataElement[assessment][0], this.dataElementValues[assessment][0], this.goalElementValues[assessment][0])
            this.getActions();
        }
    }

    getEntities() {
        return this._entitieService.getAllEntities()
    }

    getEntitiesByIds(params) {
        return this._entitieService.getEntitiesById(params)
    }

    getLastAssessment = (params): Observable<any> => {
        return this._selfAssessmentService.getLastAssessment();
    }

    getLastFiveAssessmentsByEntityId = (params) => {
        return this._selfAssessmentService.getLastFiveAssessmentsByEntitiesId(params);
    }

    getLastFiveAssessmentsByEntitiesId = (params) => {
        return this._selfAssessmentService.getLastFiveAssessmentsByEntitiesId(params);
    }


    // Call method when we want to change charts data
    chartOptionSet(option, data, values, values2) {
        let op = option;
        if(Cookie.get('language')==="en")
        {
            
            op = {
            // Global text styles
            textStyle: {
                fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                fontSize: 13,

            },
            // @ts-ignore
            tooltip: {
                trigger: 'item',
                backgroundColor: 'rgba(0,0,0,0.75)',
                padding: 5,
                textStyle: {
                    fontSize: 13,
                    fontFamily: 'Roboto, sans-serif'
                },
                axisPointer:{
                    type: 'shadow',
                    shadowStyle: {
                        color: 'rgba(150,150,150,0.3)',
                        width: 'auto',
                        type: 'default'
                    }
                }   
                
            },
            legend: {
                orient : 'vertical',
                x : 'right',
                y : 'bottom',
                data:['Actual Score','Goal']
            },
            toolbox: {
                show: true,
                feature: {
                    mark: {show: true},
                    dataView: {show: false},
                    restore: {show: false},
                    saveAsImage: {
                        show: true, title: "save", name: 'image',
                    }
                }
            },
            color:[
                '#256bc2','#3ca14a'
            ],

            // Setup polar coordinates
            polar: [{
                radius: '80%',
                center: ['50%', '50%'],
                name: {
                    color: '#777'
                },
                indicator: data
            }],
            /* polar:[
                {
                    indicator: data
                }
            ], */

            // Add series
            series: [{
                name: 'Evaluation',
                type: 'radar',
                symbolSize: 7,
                itemStyle: {
                    normal: {
                        borderWidth: 2
                    }
                },
                areaStyle: {
                    normal: {
                        opacity: 0.3,
                    }
                },
                data: [
                    {
                        value: values,
                        name : 'Actual Score'
                    },
                    {
                        value: values2,
                        name : 'Goal'                      
                    }
                ]
            }]
        } 
        }
        else{
            op = {
                // Global text styles
                textStyle: {
                    fontFamily: 'Roboto, Arial, Verdana, sans-serif',
                    fontSize: 13
                },
                // @ts-ignore

                tooltip: {
                    trigger: 'item',
                    backgroundColor: 'rgba(0,0,0,0.75)',
                    padding: 5,
                    textStyle: {
                        fontSize: 13,
                        fontFamily: 'Roboto, sans-serif'
                    },
                    axisPointer:{
                        type: 'shadow',
                        shadowStyle: {
                            color: 'rgba(150,150,150,0.3)',
                            width: 'auto',
                            type: 'default'
                        }
                    }   
                    
                },
                legend: {
                    orient : 'vertical',
                    x : 'right',
                    y : 'bottom',
                    data:['Note actuelle','Objectif']
                },
                toolbox: {
                    show: true,
                    feature: {
                        mark: {show: true},
                        dataView: {show: false},
                        restore: {show: false},
                        saveAsImage: {
                            show: true, title: "save", name: 'image',
                        }
                    }
                },
                color:[
                    '#256bc2','#3ca14a'
                ],
    
                // Setup polar coordinates
                polar: [{
                    radius: '80%',
                    center: ['50%', '50%'],
                    name: {
                        color: '#777',
                        fontSize : 12,
                        formatter: function (value, indicator) {
                            let w=12;
                            let t=value.split(' ');
                            let l=0;
                            let o='';
                            for (var tw of t) {
                                if (l+tw.length >= w) {
                                    o += (l?'\n':'')+tw+' '; l=tw.length+1;
                                } else {
                                    o += tw+' ';
                                    l += tw.length+1;
                                }
                            }
                            return o;
                        }
                    },
                    indicator: data
                }],
                /* polar:[
                    {
                        indicator: data
                    }
                ], */
    
                // Add series
                series: [{
                    name: 'Evaluation',
                    type: 'radar',
                    symbolSize: 7,
                    itemStyle: {
                        normal: {
                            borderWidth: 2
                        }
                    },
                    areaStyle: {
                        normal: {
                            opacity: 0.3,
                        }
                    },
                    data: [
                        {
                            value: values,
                            name : 'Note actuelle'
                        },
                        {
                            value: values2,
                            name : 'Objectif'                      
                        }
                    ]
                }]
            }
        }
       
        return op
    }

    bindLastAssessments = () => {
        this.dataAxe = new Array(this.Assessments.length);
        this.dataElement = new Array(this.Assessments.length);
        this.dataAxeValues = new Array(this.Assessments.length)
        this.dataElementValues = new Array(this.Assessments.length)

        this.goalAxeValues = new Array(this.Assessments.length)
        this.goalElementValues = new Array(this.Assessments.length)

        this.Assessments.forEach((dat, i) => {
            this.dataAxe[i] = []
            this.dataElement[i] = new Array(dat.assesment_score.axes.length);
            this.dataElementValues[i] = new Array(dat.assesment_score.axes.length);
            this.dataAxeValues[i] = []

            this.goalElementValues[i] = new Array(dat.assesment_score.axes.length);
            this.goalAxeValues[i] = []

            this.Assessments[i]['perc'] = 0
            //console.log(dat.assesment_score.axes)
            dat.assesment_score.axes.forEach((elms, j) => {
                let perc = 0;

                let scoreAxe = 0;
                this.dataElement[i][j] = []
                this.dataElementValues[i][j] = []
                this.goalElementValues[i][j] = []
                elms.elements.forEach((elm,k) => {
                    this.dataElement[i][j].push({text: elm.description, max: 5})
                    if (elm.score != 0 && dat.assesment_goals.axes[j].elements[k].goal > 0)
                        perc++;
                        this.dataElementValues[i][j].push(elm.score)
                        this.goalElementValues[i][j].push(dat.assesment_goals.axes[j].elements[k].goal)
                        scoreAxe += elm.score;
                })
                this.Assessments[i]['perc'] += ((perc * 100) / elms.elements.length)
                //this.dataAxeValues[i].push(scoreAxe / elms.elements.length)
                this.dataAxeValues[i].push(elms.score);
                this.goalAxeValues[i].push(dat.assesment_goals.axes[j].goal)
                this.dataAxe[i].push({text: elms.description, max: 5})

            })
            this.Assessments[i]['perc'] /= this.Assessments[i].assesment_score.axes.length
            this.Assessments[i]['perc'] = Math.round(this.Assessments[i]['perc']);

        })
        this.Axes = [];

        if (Array.isArray(this.dataAxe[0])) {
            this.dataAxe[0].forEach((dat) => {
                this.Axes.push(dat.text)
            })
        }

        this._userService.getAllUsers().subscribe(data=>{
            this._userService.users = data;
            this.getActions();
        })
    }

    getActions(){
        /* this._actionsService.getEntityActions(this.Assessments[this.selectedAssess].assesment_entity_code).subscribe(response=>{
            let dat = [];
            this.actions = [];
            response.actions.forEach((resp,i)=>{
                resp.DueDate = this.changeDateFormat(resp.DueDate)
                if ( resp.dueDate == '1970-01-01 00:00:00') resp.dueDate = '';
                this._userService.users.forEach(user=>{
                    if (user._id === resp.Who) resp.Who = user.Name + ' | ' + user.Email;
                })
                dat.push(resp);
            })
            this.actions = dat
            this.dataSource.data = dat;
            this.resultsLength = dat.length;

            this.dataSource.data = dat.filter((elem, index) => {
                return index < this.pageSize
            })
            this.dataSource.data = dat.filter((elem, index) => {
                return index >= this.pageIndex * this.pageSize && index < ((this.pageIndex + 1) * this.pageSize);
            })
            this.dataSource.sort = this.sort;
        }); */
        this._actionsService.getActionsByAssessment(this.Assessments[this.selectedAssess].assesment_id).subscribe(response=>{
            let dat = [];
            this.actions = [];
            response.data.forEach((resp,i)=>{
                resp.DueDate = this.changeDateFormat(resp.DueDate)
                if ( resp.DueDate == '1970-01-01') resp.DueDate = '';
                this._userService.users.forEach(user=>{
                    if (user._id === resp.Who) resp.Who = user.Name + ' | ' + user.Email;
                })
                dat.push(resp);
            })
            this.actions = dat
            this.dataSource.data = dat;
            this.resultsLength = dat.length;

            this.dataSource.data = dat.filter((elem, index) => {
                return index < this.pageSize
            })
            this.dataSource.data = dat.filter((elem, index) => {
                return index >= this.pageIndex * this.pageSize && index < ((this.pageIndex + 1) * this.pageSize);
            })
            this.dataSource.sort = this.sort;
        })
    }

    navigateAssessment(){
        this.router.navigate(['/self-assessment/list']);
    }

    editAssessment(i){
        
    this._selfAssessmentService.getSelfAssessment(this.Assessments[i].assesment_id).subscribe(response=>{
        //console.log(this.Assessments[i].assesment_id);
        //console.log(response.data)
        //console.log(response)
            SelfAssessmentService.selfAssessmentToEdit = response;
            //console.log(SelfAssessmentService.selfAssessmentToEdit)
            this.router.navigate(['/self-assessment/details']);  
        })
    }

    publish(i){
        
        this._selfAssessmentService.getSelfAssessment(this.Assessments[i].assesment_id).subscribe(response=>{
            if(this.Assessments[i].assesment_status === 'Finished') response.Status = 2;
            else response.Status = 1;
            this._selfAssessmentService.updatSelfAssessment(response).subscribe(data=>{
                this.onSelectionChangeEntity({value: this.selectedEntity})
            })
        })
    }


    private transformer = (node: Entit, level: number) => {
        return {
            expandable: !!node.children && node.children.length > 0,
            name: ''+node.entity.Code+' | '+node.entity.Description,
            entity: node.entity,
            level: level
        };
    }

    treeControl = new FlatTreeControl<ExampleFlatNode>(
        node => node.level, node => node.expandable);

    treeFlattener = new MatTreeFlattener(
        this.transformer, node => node.level, node => node.expandable, node => node.children);

    treeSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    hasChild = (_: number, node: ExampleFlatNode) => node.expandable;
 

    initData(){
        this.treeSource.data = [];
        TREE_DATA = [];
        let flag = false;
        this._entitiesService.entities.forEach(entity=>{
            if(entity.Type === "Site"){
                flag = true;
                TREE_DATA.push({entity: entity,children:[]});
                this._entitiesService.entities.forEach(sub=>{
                    for(var i =0 ; i<TREE_DATA[TREE_DATA.length-1].entity.SubEntities.length; i++ ){
                        if(sub._id === TREE_DATA[TREE_DATA.length-1].entity.SubEntities[i]){
                            TREE_DATA[TREE_DATA.length-1].children.push({entity:sub,children:[]});
                            let chilLen = TREE_DATA[TREE_DATA.length-1].children.length;
                            this._entitiesService.entities.forEach(sub1=>{
                                let len = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities.length;                                
                                for(var j = 0 ; j<len; j++ ){
                                    if(sub1._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities[j]){
                                        TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.push({entity:sub1,children:[]});
                                        let chilLen1 = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.length;
                                        this._entitiesService.entities.forEach(sub2=>{
                                            let len2 = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].entity.SubEntities.length;                                
                                            for(var k = 0 ; k<len2; k++ ){
                                                if(sub2._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].entity.SubEntities[k]){
                                                    TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].children.push({entity:sub2,children:[]});
                                                }
                                            }
                                        })
                                    } 
                                }
                            })
                        }    
                    }
                })
            }
        })
        if(!flag){
            this._entitiesService.entities.forEach(entity=>{
                if(entity.Type === "Usine"){
                    flag = true;
                    TREE_DATA.push({entity: entity,children:[]});
                    this._entitiesService.entities.forEach(sub=>{
                        for(var i =0 ; i<TREE_DATA[TREE_DATA.length-1].entity.SubEntities.length; i++ ){
                            if(sub._id === TREE_DATA[TREE_DATA.length-1].entity.SubEntities[i]){
                                TREE_DATA[TREE_DATA.length-1].children.push({entity:sub,children:[]});
                                let chilLen = TREE_DATA[TREE_DATA.length-1].children.length;
                                this._entitiesService.entities.forEach(sub1=>{
                                    let len = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities.length;                                
                                    for(var j = 0 ; j<len; j++ ){
                                        if(sub1._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities[j]){
                                            TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.push({entity:sub1,children:[]});
                                        } 
                                    }
                                })
                            }    
                        }
                    })
                }
            });
        }
        if(!flag){
            this._entitiesService.entities.forEach(entity=>{
                if(entity.Type === "Ligne"){
                    flag = true;
                    TREE_DATA.push({entity: entity,children:[]});
                    this._entitiesService.entities.forEach(sub=>{
                        for(var i =0 ; i<TREE_DATA[TREE_DATA.length-1].entity.SubEntities.length; i++ ){
                            if(sub._id === TREE_DATA[TREE_DATA.length-1].entity.SubEntities[i]){
                                TREE_DATA[TREE_DATA.length-1].children.push({entity:sub,children:[]});
                            }    
                        }
                    })
                }
            });
        }
        if(!flag){
            this._entitiesService.entities.forEach(entity=>{
                if(entity.Type === "Atelier"){
                    flag = true;
                    TREE_DATA.push({entity: entity,children:[]});
                }
            });
        }
        this.treeSource.data = [];
        this.treeSource.data = TREE_DATA;
    }


    changePage = (event) => {
        const {pageIndex, pageSize} = event;
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.dataSource.data = this.actions.filter((elem, index) => {
            return index >= this.pageIndex * this.pageSize && index < ((this.pageIndex + 1) * this.pageSize);
        })
        this.dataSource.sort = this.sort;
    }

    changeDateFormat(DATE){
        let date = new Date(DATE);
        let years = date.getFullYear();

        var months,hours,minutes,seconds,days;

        if(date.getMonth()<9){
            months = '0'+ (date.getMonth()+1);
        } else{
            months = (date.getMonth()+1);
        }
        if(date.getDate()<10){
            days = '0'+ (date.getDate());
        } else{
            days = (date.getDate());
        }

        let stringDate = years +'-'+months+"-"+days;

        return stringDate;
    }


}


function compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
  
