import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppRoutesConstant} from "../_constants/app-routes";
import {LoginComponent} from "./login/login.component";

const routes: Routes = [
  {
    path: AppRoutesConstant.LOGIN,
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserAuthRoutingModule { }
