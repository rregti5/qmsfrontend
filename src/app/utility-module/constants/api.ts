import {environment} from '@env/environment';

const BASE_URL = environment.apiUrl;

export class QMSApi {

    // Axes
    public static AXES_ADD = `${BASE_URL}/axes/Axe_Create`;
    public static AXES_UPDATE = `${BASE_URL}/axes/Axe_Update`;
    public static AXES_ALL = `${BASE_URL}/axes/Axe_List`;
    public static AXES_REMOVE = `${BASE_URL}/axes/Axe_Delete`;
    public static AXES_SINGLE = `${BASE_URL}/axes`;

    // Entities
    public static ENTITY_ALL = `${BASE_URL}/entity/all`;
    public static ENTITY_BYID = `${BASE_URL}/Entity/entity`;
    public static ENTITY_BY_IDS = `${BASE_URL}/entity/entities`;

    // Self Assessment
    public static SELF_ASSESSMENT_CREATE = `${BASE_URL}/assessment/Assessment_Create`;
    public static SELF_ASSESSMENT_LAST = `${BASE_URL}/assessment/Last_Assessment`;
    public static SELF_ASSESSMENT = `${BASE_URL}/assessment`;

    // Assessment Plan
    public static ASSESSMENT_PLAN_CREATE = `${BASE_URL}/assessment_Plan/assPlan_create`;
    public static ASSESSMENT_PLAN_UPDATE = `${BASE_URL}/assessment_Plan/assPlan_update`;
    public static ASSESSMENT_PLAN_REMOVE = `${BASE_URL}/assessment_Plan/assPlan_delete`;
    public static ASSESSMENT_PLAN_ALL = `${BASE_URL}/assessment_plan/assPlan_List`;
    public static ASSESSMENT_PLAN_SINGLE = `${BASE_URL}/assessment_plan/assPlan`;


    // Dashboard
    public static DASHBOARD_SCORE = `${BASE_URL}/dashboard/calculate_score`;


    // Attachments
    public static ATTACHMENT_ADD = `${BASE_URL}/attachments/Add_attachment`;
    public static ATTACHMENT_UPDATE = `${BASE_URL}/attachments/Update_Attachment`;
    public static SEARCH = `${BASE_URL}/attachments/Search`;
    public static ATTACHMENT = `${BASE_URL}/attachments/`;
    public static ATTACHMENT_DELETE_PATH = `${BASE_URL}/attachments/delete_Url`;
    public static ATTACHMENT_GET_URL = `${BASE_URL}/attachments/url`;
    

}

