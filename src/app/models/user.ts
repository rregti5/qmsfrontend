export class User{
    constructor(
        public Name: string,
        public Email: string,
        public _id: string,
        public Entity: Array<string>,
        public Role: number,
        public Password: string,
        public Status: number,
        public LikedAttachments: Array<string>,
    ){}
}