import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor() {
  }

  // public backendUrl = "http://localhost:4000/";
  public backendUrl = "https://self01-backend.herokuapp.com/";

  public isCollapseMenu = false;

}
