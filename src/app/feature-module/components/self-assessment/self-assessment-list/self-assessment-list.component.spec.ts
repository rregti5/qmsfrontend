import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfAssessmentListComponent } from './self-assessment-list.component';

describe('SelfAssessmentListComponent', () => {
  let component: SelfAssessmentListComponent;
  let fixture: ComponentFixture<SelfAssessmentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfAssessmentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfAssessmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
