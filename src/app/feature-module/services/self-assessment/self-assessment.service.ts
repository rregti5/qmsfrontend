import {Injectable} from '@angular/core';
import {APIManager} from "@services/apimanager.service";
import {HttpMethodsTypeEnum, QMSApi} from "@constants/*";
import {Headers, Http, RequestOptionsArgs} from '@angular/http';
import {catchError, map} from 'rxjs/operators';
import {Observable, throwError as observableThrowError} from 'rxjs';
import { environment } from '@env/environment';
import { Cookie } from 'ng2-cookies/ng2-cookies';


@Injectable({
    providedIn: 'root'
})
export class SelfAssessmentService {

    static selfAssessmentToEdit: any = null;
    assessments: Array<any>;
    url = environment.apiUrl + '/assessment/';

    constructor(private _apiManager: APIManager,
                private http: Http) {
    }

    // setSelfAsse = (selfAssessmentToEdit) => {
    //     Cookie.set('sa', JSON.stringify(selfAssessmentToEdit));
    // }
    //
    // getSelfAsse = () => {
    //     return JSON.parse(Cookie.get('sa'));
    // }

    createSelfAssessment = (params): Observable<any> => {
        return this._apiManager.httpHelperMethod(
            HttpMethodsTypeEnum.POST, QMSApi.SELF_ASSESSMENT_CREATE, params,
            this._apiManager.Authorised_HttpOptions, true, true);
    };

    updatSelfAssessment(assessment) {

        let body = {
            Actions: assessment.Actions,
            AssessmentId: assessment.AssessmentId,
            Axes: assessment.Axes,
            Code: assessment.Code,
            CreatedBy: assessment.CreatedBy,
            CreatedOn: assessment.CreatedOn,
            Description: assessment.Description,
            Entity: assessment.Entity,
            Status: assessment.Status,
            id: assessment.id,
            PublishedOn: assessment.PublishedOn,
            _id: assessment._id
        }
        let options: RequestOptionsArgs = {
            headers: new Headers({
                
                Authorization: 'Bearer ' + Cookie.get('token'),
                env : environment.env,
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
               
            })
        }

        return this.http.put(this.url + 'Assessment_Update/' + assessment.id, body, options)
       
    }

    getAllSelfAssessment() {
        let options: RequestOptionsArgs = {
            headers: new Headers({
                Authorization: 'Bearer ' + Cookie.get('token'),
                env : environment.env
            })
        }

        return this.http.get(this.url + 'Assessment_List', options).pipe(
            map((res: any) => {
                return res.json();
            }),
            catchError((error: any) => {
                return observableThrowError(error.json ? error.json().error : error || 'server error');
            }),
        );
    }

    getAllSelfAssessmentShort(pageIndex?,pageSize?) {
        let options: RequestOptionsArgs = {
            headers: new Headers({
                Authorization: 'Bearer ' + Cookie.get('token'),
                env : environment.env
            })
        }


        let url = this.url + 'Assessment_short_List';
        if(pageIndex!=null){
            url = url +"?pageIndex="+pageIndex+"&pageSize="+pageSize;
        }

        return this.http.get(url, options).pipe(
            map((res: any) => {
                return res.json();
            }),
            catchError((error: any) => {
                return observableThrowError(error.json ? error.json().error : error || 'server error');
            }),
        );
    }

    getLastAssessment = (): Observable<any> => {
        return this._apiManager.httpHelperMethod(
            HttpMethodsTypeEnum.GET, QMSApi.SELF_ASSESSMENT_LAST, {},
            this._apiManager.Authorised_HttpOptions, false, true);
    };

    getLastFiveAssessmentsByEntitiesId = (params): Observable<any> => {
        return this._apiManager.httpHelperMethod(
            HttpMethodsTypeEnum.POST, QMSApi.SELF_ASSESSMENT_LAST, params,
            this._apiManager.Authorised_HttpOptions, false, true);
    };

    deleteSelfAssessment(assessment) {
        let options: RequestOptionsArgs = {
            headers: new Headers({
                Authorization: 'Bearer ' + Cookie.get('token'),
                env: environment.env,
                'Content-Type': 'application/json'
            })
        }

        return this.http.delete(this.url + 'Assessment_Delete/' + assessment._id, options)
        
    }
    getSelfAssessment(id) {
        let options: RequestOptionsArgs = {
            headers: new Headers({
                Authorization: 'Bearer ' + Cookie.get('token'),
                env: environment.env
            })
        }

        return this.http.get(this.url + id, options).pipe(
            map((res: any) => {
                return res.json();
            }),
            catchError((error: any) => {
                return observableThrowError(error.json ? error.json().error : error || 'server error');
            }),
        );
    }
    getSelfAssessmentsByEntities(entities,pageIndex?,pageSize?){
        let options: RequestOptionsArgs = {
            headers: new Headers({
                Authorization: 'Bearer ' + Cookie.get('token'),
                env: environment.env
            })
        }
        let body={
            entities : entities
        }

        let url = this.url + 'Entity_Assessments';
        if(pageIndex != null){
            url = url +"?pageIndex="+pageIndex+"&pageSize="+pageSize;
        }

        return this.http.post(url, body, options).pipe(
            map((res: any) => {
                return res.json();
            }),
            catchError((error: any) => {
                return observableThrowError(error.json ? error.json().error : error || 'server error');
            }),
        );
    }

    getGuideQMSAssessment(){
        let options: RequestOptionsArgs = {
            headers: new Headers({
                Authorization: 'Bearer ' + Cookie.get('token'),
                env: environment.env
            })
        }

        return this.http.get(this.url+'Guide_Assessment', options).pipe(
            map((res: any) => {
                return res.json();
            }),
            catchError((error: any) => {
                return observableThrowError(error.json ? error.json().error : error || 'server error');
            }),
        );
    }

    getLastPublishedAssessmentScore(entities){
        let options: RequestOptionsArgs = {
            headers: new Headers({
                Authorization: 'Bearer ' + Cookie.get('token'),
                env: environment.env
            })
        }

        let body = {
            entities: entities
        }

        return this.http.post(this.url+'LastPublishedAssessment',body, options).pipe(
            map((res: any) => {
                return res.json();
            }),
            catchError((error: any) => {
                return observableThrowError(error.json ? error.json().error : error || 'server error');
            }),
        );
    }

    getAssessmentState(entityId){
        let options: RequestOptionsArgs = {
            headers: new Headers({
                Authorization: 'Bearer ' + Cookie.get('token'),
                env: environment.env
            })
        }

        return this.http.get(this.url+'state/'+entityId, options).pipe(
            map((res: any) => {
                return res.json();
            }),
            catchError((error: any) => {
                return observableThrowError(error.json ? error.json().error : error || 'server error');
            }),
        );
    }
}
