import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GloassaireComponent } from './gloassaire.component';

describe('GloassaireComponent', () => {
  let component: GloassaireComponent;
  let fixture: ComponentFixture<GloassaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GloassaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GloassaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
