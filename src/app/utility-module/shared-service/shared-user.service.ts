import {Injectable} from '@angular/core';
import {User} from '../shared-model/shared-user.model';
import {APPStorage} from '../constants/storage';
import {EncryptionFunctions} from '../shared-functions/encryption-functions';
import {BehaviorSubject} from 'rxjs';
import {Observable} from 'rxjs';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Injectable()

export class SharedUserService {
  
  private userFlag: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  
  constructor() {
  }
  
  private _user: User;
  
  getUser(): User {
    if (!this._user) {
      this._user = EncryptionFunctions.DECRYPT_OBJ(Cookie.get(APPStorage.USER));
    }
    return this._user;
  }
  
  setUser(value: User): void {
    Cookie.set(APPStorage.USER, EncryptionFunctions.ENCRYPT_OBJ(value));
    this._user = value;
  }
  
  /* Shared User detailChangeFlag for update status */
  
  setUserDetailCall(value: boolean): void {
    this.userFlag.next(value);
  }
  
  getUserDetailCall(): Observable<boolean> {
    return this.userFlag.asObservable();
  }
}
