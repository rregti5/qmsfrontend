export * from './header/header.component';
export * from './user/user-list/user-list.component';
export * from './user/user-details/user-details.component';
export * from './axes/axes-list/axes-list.component';
export * from './axes/axes-details/axes-details.component';
export * from './enitities/entities-list/entities-list.component';
export * from './enitities/entities-details/entities-details.component';
export * from './assessments-plan/assessment-plan-list/assessment-plan-list.component';
export * from './assessments-plan/assessment-plan-details/assessment-plan-details.component';
export * from './self-assessment/self-assessment-view/self-assessment-view.component';
export * from './dashboard/dashboard.component';
export * from './self-assessment/self-assessment-list/self-assessment-list.component';
export * from './axes/axes-update/axes-update.component';
export * from './actions/actions.component'