import {Component, HostListener, OnInit, OnDestroy} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from "@angular/forms";
import {Observable} from "rxjs";
import {AxesService, EntitiesService, SelfAssessmentService, AssessmentPlanService} from "../../../services";
import {AppRoutesConstant} from "../../../../_constants/app-routes";
import {Router} from "@angular/router";
import {UserService} from 'app/services/user/user.service';
import {DashboardService} from 'app/feature-module/services/dashboard/dashboard.service';
import {Action} from 'app/models/action';
import {MatTableDataSource} from '@angular/material/table';
import {ActionsService} from 'app/services/actions/actions.service';
import {AngularFireStorage, AngularFireUploadTask} from '@angular/fire/storage';
import {finalize} from 'rxjs/operators';
import { AttachmentService } from 'app/feature-module/services/attachments/attachment.service';
import { EntititesService } from 'app/services/entities/entitites-service.service';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlattener, MatTreeFlatDataSource, MatSnackBar } from '@angular/material';
import { Entity } from 'app/models/Entity';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '@env/environment';
import { ConfigService } from 'app/services/config/config-service.service';
import { ScoreDescirptionColorList } from 'app/utility-module/shared-functions/common-functions';
import { Cookie } from 'ng2-cookies/ng2-cookies';

export interface Attachment {
    Name: string;
    Type: string;
    URL: string;
    Description : string;
}

const ELEMENT_DATA: Action[] = [];
const ATTACHMENTS: Attachment[] = []

interface Entit {
    entity: Entity;
    children?: Entit[];
}

var TREE_DATA: Entit[] = [];

/** Flat node with expandable and level information */
interface ExampleFlatNode {
    expandable: boolean;
    name: string;
    level: number;
}


@Component({
    selector: 'app-self-assessment-view',
    templateUrl: './self-assessment-view.component.html',
    styleUrls: ['./self-assessment-view.component.scss']
})
export class SelfAssessmentViewComponent implements OnInit,OnDestroy {

    color: string;
    maximumTextLength = 130;
    panelOpenState = false;
    isAssessmentForm = false;
    displayedColumns: string[] = ['Position', 'Description', 'Who', 'DueDate', 'Status', 'Comment', 'Attachment','Action'];
    dataSource = new MatTableDataSource(ELEMENT_DATA);
    selfAssessmentForm: FormGroup;
    entityList = [];
    users: Array<any> = [];
    allUsers: Array<any> = [];
    isAttach: boolean = false;
    displayerr:boolean=false;
    updatedPourcentages = false;
    readMoreDescription;
    readMoreDescriptionIndex;
    isScore = true;

    assessment: any;

    pourcentages: any = {};

    selectedElement: any = {index: 0, axe: 0};
    isLoading: boolean = false;

    attachments = new MatTableDataSource(ATTACHMENTS);
    displayAttachmentsColumns: string[] = ['Position', 'Type', 'Name','Description', 'Download', 'Delete']

    actionIndex: number = null;
    colorList = ScoreDescirptionColorList

    attachType:string;

    isBestInClass = false;
    attachmentAlertText : string="";

    constructor(private _entitiesService: EntitiesService,
                private entitiesService : EntititesService,
                private _selfAssessmentService: SelfAssessmentService,
                private _fb: FormBuilder,
                public _configService: ConfigService,
                private _userService: UserService,
                private _axesService: AxesService,
                private _dashboardService: DashboardService,
                private _actionService: ActionsService,
                private _storage: AngularFireStorage,
                private _router: Router,
                private _snackbar : MatSnackBar,
                private _assessmentPlanService: AssessmentPlanService,
                private _attachmentService: AttachmentService,public translate: TranslateService) {
                   if(Cookie.get('language')!==null)
                   translate.setDefaultLang(Cookie.get('language'));
    }

    ngOnDestroy(){
        SelfAssessmentService.selfAssessmentToEdit = null;
    }

    ngOnInit() {
        this.createSelfAssessmentInitForm();
        this._userService.getCurrentUser().subscribe(data=>{
            this._userService.currentUser = data;
            if (data.Entity == null ){
                this._userService.currentUser.Entity = [];
            }

            if(data.Role === 3){
                this._userService.getAllUsers().subscribe(data => {
                    this.users = data;
                })
            }else{
                this._userService.getSubUsers().subscribe(data=>{
                    //console.log(data);
                    this.users = data.users;
                })
            }
        })

        this._axesService.getAllAxes().subscribe(response => {
            this._axesService.axes = response.data;
        })

        if (SelfAssessmentService.selfAssessmentToEdit != null) {
            //console.log("JsonHttp");
            this.isAssessmentForm = true;
            this.assessment = SelfAssessmentService.selfAssessmentToEdit;
            this.assessment.Axes = this.assessment.Axes.sort((a, b) => {
                return compare(a.Description, b.Description, true);
            })

            
            this.updateAssessment(0, 0);

            /*this._entitiesService.getEntityById(this.assessment.Entity).subscribe(data=>{
                this.assessment.Goal = data.entity.ScoreGoal;
            })*/

        } else {
            this._userService.getCurrentUser().subscribe(data=>{
                this._userService.currentUser = data;
                this._assessmentPlanService.getAllAssessmentPlans().subscribe(data=>{
                    //console.log(data);
                    this._assessmentPlanService.assessmentPlans = data.data;
                    this.entitiesService.getAllEntities().subscribe((data)=>{
                        /* console.log(this.entitiesService.entities);
                        console.log(data); */
                        if(this._userService.currentUser.Role < 3){
                            let ents = []
                            this.entitiesService.entities.forEach(entity=>{
                                if(this._userService.currentUser.Entity.includes(entity._id)){
                                    ents.push(entity);
                                }
                            });
                            this.entitiesService.entities = ents;
                            //console.log(this.entitiesService.entities)
                        }
                        this.initData();
                    });
                })
                //this.bindEntities();
               
            })
        }
        //console.log(this._axesService.axes)
    }

    createSelfAssessmentInitForm = () => {
        this.selfAssessmentForm = new FormGroup({
            Entity: new FormControl()
         });
        this.selfAssessmentForm = this._fb.group({
            Entity: ['', [
                <any>Validators.required
            ]],
        })
    };

    bindEntities = () => {
        this.getEntities().subscribe(response => {
            this.entityList = response.entities;
        })
    }

    getEntities = (): Observable<any> => {
        return this._entitiesService.getAllEntities();
    }

    onSubmitSelfAssessmentInitForm = (form: FormGroup) => {
        if (form.valid) {
            const params = form.value;
            //console.log(params)
            this._selfAssessmentService.createSelfAssessment(params).subscribe(response => {
                //console.log(response)
                if (!response.error) {

                    this.isAssessmentForm = true;

                    this.assessment = response.assessment;
                    this.assessment.Axes = this.assessment.Axes.sort((a, b) => {
                        return compare(a.Description, b.Description, true);
                    })

                    this.updateAssessment(0, 0);

                    this._entitiesService.getEntityById(this.assessment.Entity).subscribe(data=>{
                        this.assessment.Goal = data.entity.ScoreGoal;
                    })

                }
                
            },err=>{
                this.displayerr=true;setTimeout( () => {this.displayerr=false;console.log("err") }, 5000 );
            }
            )
        }

    }

    onSelect = (id) => {
        document.getElementById(id).click();
    };

    onFileSelect = (id) => {
        document.getElementById(id).click();
    };

    onFileGetName = (event, type: string) => {

        this.isLoading = true;

        var myReader: FileReader = new FileReader();

        const file = event.target.files[0];
        let filePath = "";


        (myReader.readAsDataURL(file));

        myReader.onloadend = (e)=>{
            //console.log(myReader.result);
        

       /*  if (type === 'element') {
            filePath = 'Files/' + this.assessment._id + '-' + new Date() + '-' + event.target.files[0].name;
        } else {
            filePath = 'Files/' + this.dataSource.data[this.actionIndex]._id + '-' + new Date() + '-' + event.target.files[0].name;
        }
 */
            let date = new Date();
            if (type === 'element') {
                filePath = Date.parse(date.toString()) + '_' + event.target.files[0].name;
            } else {
                filePath = Date.parse(date.toString()) + '_' + event.target.files[0].name;
            }

            const fileRef = this._storage.ref(filePath);
            const ref = this._storage.ref(filePath);

            this._attachmentService.createFile(myReader.result,filePath).subscribe(data=>{
                console.log(data);
                //let type = result.metadata.contentType.split('/')[1].toUpperCase();

                if(!!data){
                    let dat = {
                        Type: file.name.split('.')[file.name.split('.').length-1],
                        Name: event.target.files[0].name,
                        URL: environment.apiUrl+'/attachments/file/'+filePath,
                        Description: "",
                    }
                    let atchmnts = this.attachments.data;
    
                    if (type === 'element') {
                        this.assessment.Axes[this.selectedElement.axe].Elements[this.selectedElement.index].Attachment.push({url:filePath,name:event.target.files[0].name,description:""});
                        this.save(this.selectedElement.axe, this.selectedElement.index);
                    } else {
                        this.dataSource.data[this.actionIndex].Attachment.push({url:filePath,name:event.target.files[0].name,description:""});
                        this.saveActions();
                    }
                    atchmnts.push(dat);
                    this.attachments.data = atchmnts;
                    this.isLoading = false;
                    let typ = "Element";
                    if(type != 'element'){
                        typ = "Action"
                    }
                    let attach = {
                        Path:filePath,
                        DocumentName:event.target.files[0].name,
                        DocumentType:event.target.files[0].name.split('.')[event.target.files[0].name.split('.').length - 1],
                        Type: typ,
                        Axe: {_id: this.assessment.Axes[this.selectedElement.axe]._id , Description: this.assessment.Axes[this.selectedElement.axe].Description},
                        Element: this.assessment.Axes[this.selectedElement.axe].Elements[this.selectedElement.index].Description,
                        AssessmentId: this.assessment._id,
                        Description: "",
                        Uploader: this._userService.currentUser + ' | ' + this._userService.currentUser.Email,
                        Date: new Date()
                    }
                    this._attachmentService.addAttachment(attach).subscribe(data=>{
                    })    
                }else{
                    alert("Problème d'importation du fiochier")
                }
                

            })            

        }


    }

    openAttach(type, indx?: number) {

        this.attachType = type;

        this.isLoading = true;
        this.attachments.data = [];
        this.isAttach = true;
        if (type === 'element') {
            this.actionIndex = null;
            let attachments = this.assessment.Axes[this.selectedElement.axe].Elements[this.selectedElement.index].Attachment;
            let atchmnts = [];
            if (attachments.length == 0) this.isLoading = false;
            attachments.forEach(element => {
                const filePath = element.url || element;
                const fileRef = this._storage.ref(filePath);
                //let type = response.contentType.split('/')[1].toUpperCase();
                let type = element.name.split('.')[element.name.split('.').length-1];
                let description = element.description || "";

                atchmnts.push({Type: type, Name: element.name || element.url.split('_')[2] || element.split('_')[2] ,Description : description, URL:  environment.apiUrl+'/attachments/file/'+filePath});
                this.attachments.data = atchmnts;
                this.isLoading = false;

                //fileRef.getDownloadURL().subscribe(data => {

                
                //})
                
            });
        } else {
            this.actionIndex = indx;
            let attachments = this.dataSource.data[indx].Attachment;
            if (attachments.length == 0) this.isLoading = false;
            let atchmnts = [];
            /* attachments.forEach(element => {
                const filePath = element.url || element;
                const fileRef = this._storage.ref(filePath);
                fileRef.getMetadata().subscribe(response => {
                    //let type = response.contentType.split('/')[1].toUpperCase();
                    let type = response.type;
                    let description = element.description || "";
                    fileRef.getDownloadURL().subscribe(data => {

                        atchmnts.push({Type: type, Name: element.name || element.split('_')[2],Description : description, URL: data});
                        this.attachments.data = atchmnts;
                        this.isLoading = false;
                    })
                })
            }); */
            attachments.forEach(element => {
                const filePath = element.url || element;
                const fileRef = this._storage.ref(filePath);
                //let type = response.contentType.split('/')[1].toUpperCase();
                let type = element.name.split('.')[element.name.split('.').length-1];
                let description = element.description || "";

                atchmnts.push({Type: type, Name: element.name || element.url.split('_')[2] || element.split('_')[2] ,Description : description, URL:  environment.apiUrl+'/attachments/file/'+filePath});
                this.attachments.data = atchmnts;
                this.isLoading = false;
                
                //fileRef.getDownloadURL().subscribe(data => {

                
                //})
                
            });
        }

    }

    updateAttachments(){

        if(this.attachType === 'element'){
            let attachments = this.assessment.Axes[this.selectedElement.axe].Elements[this.selectedElement.index].Attachment;
            attachments.forEach((attachment,i)=>{
                attachment.description = this.attachments.data[i].Description;
            })
            this.save(this.selectedElement.axe,this.selectedElement.index);
        }else{
            let attachments = this.dataSource.data[this.actionIndex].Attachment;
            attachments.forEach((attachment,i)=>{
                attachment.description = this.attachments.data[i].Description;
            })
            this.saveActions();
        }

        this.attachments.data.forEach((attachment,i)=>{
            let filePath;
            let typ = "Element";
            let name : string;
            if(this.attachType === 'element'){
                filePath = this.assessment.Axes[this.selectedElement.axe].Elements[this.selectedElement.index].Attachment[i].url || this.assessment.Axes[this.selectedElement.axe].Elements[this.selectedElement.index].Attachment[i];
            }else{
                typ = 'Action';
                filePath = this.dataSource.data[this.actionIndex].Attachment[i].url ||this.dataSource.data[this.actionIndex].Attachment[i];
            }
            name = filePath.slice(14);
            //console.log(name)

            let attach = {
                Path:filePath,
                DocumentName : name,
                DocumentType : name.split('.')[name.split('.').length - 1],
                Type: typ,
                Axe: {_id: this.assessment.Axes[this.selectedElement.axe]._id , Description: this.assessment.Axes[this.selectedElement.axe].Description},
                Element: this.assessment.Axes[this.selectedElement.axe].Elements[this.selectedElement.index].Description,
                AssessmentId: this.assessment._id,
                Description: attachment.Description,
                Uploader: Cookie.get('currentUserName') + ' | ' + Cookie.get('currentUserEmail'),
                Date: new Date()
            }
            this._attachmentService.updateAttachment(attach).subscribe(data=>{
            })
        })

        

        
    }


    onClickScore = (i, j, element, score) => {
        if (this.isScore) {
            this.onScoreUpdate(i, j, score)
        } else {
            this.goalChange(i, j, element, score)
        }
    }

    onScoreUpdate = (i, j, score) => {
        this.updatedPourcentages = false;

        this.assessment.Axes[i].Elements[j].Scores.forEach(elmt => {
            elmt.Selected = false;
            if (elmt.Note === score.Note) {
                elmt.Selected = true;
            }
        })

        if (this.assessment.Axes[i].Elements[j].Goal >= score.Note) {
            this.updateAssessment(i, j);
        } else {
            setTimeout(() => {
                this.assessment.Axes[i].Elements[j].Goal = -1;
                this.updateAssessment(i, j);
            }, 1000);
        }


    }

    goalChange(axeIndex, elementIndex, element, event) {
        element.Goal = event.Note;
        this.updatedPourcentages = false;

        if (event.Note >= this.assessment.Score.axes[axeIndex].elements[elementIndex].score) {
            this.updateAssessment(axeIndex, elementIndex);
        } else {
            setTimeout(() => {
                this.assessment.Axes[axeIndex].Elements[elementIndex].Goal = -1;
                this.updateAssessment(axeIndex, elementIndex);
            }, 1000);
        }
    }

    attachmentCheck(i){
        let scoredElements = 0;
        let totalCount = 0;
        let attachmentsCount = 0;
        this.assessment.Score.axes[i].elements.forEach((element,j) => {
            //if (element.score != 0 && this.assessment.Axes[i].Elements[j].Goal >= element.score && (this.assessment.Axes[i].Elements[j].AttachementControl > element.score || this.assessment.Axes[i].Elements[j].Attachment.length > 0)) {
            if (element.score != 0 && this.assessment.Axes[i].Elements[j].Goal >= element.score ) {
                scoredElements++;
                if(this.assessment.Axes[i].Elements[j].AttachementControl <= element.score){
                    totalCount++;
                    if(this.assessment.Axes[i].Elements[j].Attachment.length > 0){
                        attachmentsCount++;
                    }
                }
            }
        });

        if(totalCount>0 && ( scoredElements === this.assessment.Score.axes[i].elements.length ) ){
            this.isBestInClass = true;
            this.attachmentAlertText = "Vous venez d'évaluer l'axe "+this.assessment.Axes[i].Description+", vous avez partagé "+ attachmentsCount +" bonnes pratiques et vous êtes identifié comme détenteur de "+totalCount+" bonnes pratiques, voulez-vous valider l'autoévaluation de cet axe et passer à l'axe suivant!";
        }
    }

    save(axeIndex, elementIndex) {
        this.updatedPourcentages = false;

        if (this.assessment.Axes[axeIndex].Elements[elementIndex].Goal >= this.assessment.Score.axes[axeIndex].elements[elementIndex].score) {
            this.updateAssessment(axeIndex, elementIndex);
        } else {
            setTimeout(() => {
                this.assessment.Axes[axeIndex].Elements[elementIndex].Goal = -1;
                this.updateAssessment(axeIndex, elementIndex);
            }, 1000);
        }
    }

    updateAssessment(i, j) {
        let acts = []
        if (this.assessment.Axes[i].Elements[j].Actions) {
            this.assessment.Axes[i].Elements[j].Actions.forEach(act => {
                if (act != null) {
                    if (act._id != null) acts.push(act._id);
                    if (typeof (act) === 'string') {
                        acts.push(act);
                    }
                }
            })
        }

        this.assessment.Axes[i].Elements[j].Actions = acts;
        this._selfAssessmentService.updatSelfAssessment(this.assessment).subscribe(data => {
            //console.log(data)
            this._dashboardService.getAssessmentsScores([this.assessment._id]).subscribe(res => {
                this.assessment.Score = res.data[0].assesment_score;
                this.assessment.Goal = res.data[0].assesment_goals.goal;
                let Actions = this.assessment.Axes[i].Elements[j].Actions;
                
                this.initActions(Actions);
                if (this.pourcentages.assessment !== 100 || !this.pourcentages) {
                    this.updatePourcentage();
                }
                
                this.attachmentCheck(i);
            })
        })
    }

    initActions(Actions) {
        if (Actions && Actions.length > 0) {

            this._actionService.getActions(Actions).subscribe(r => {
                console.log('Actions :')
                console.log(r.data);
                this.dataSource.data = r.data;
                //console.log(this.dataSource.data)
            })

        } else {
            this.assessment.Axes[this.selectedElement.axe].Elements[this.selectedElement.index].Actions = [];
            this.dataSource.data = [];
        }
    }

    selectElement(i, element, index) {
        this.selectedElement = {
            index: index,
            element: element,
            axe: i
        }
        //console.log(this.assessment.Axes[i].Elements[index]);
        this.isScore = true;
        let Actions = this.assessment.Axes[i].Elements[index].Actions;
        this.initActions(Actions);
    }

    tabChanged(event) {
        this.selectedElement = {axe: event.index, index: 0};
        let Actions = this.assessment.Axes[event.index].Elements[0].Actions;
        this.isScore = true;
        this.initActions(Actions);
    }

    updatePourcentage() {

        let totalCount = 0;
        let attachmentsCount = 0;

        let assessmentPourcentage = 0;
        let axesPourcentage = [];
        let axe_count = 0;
        let i = 0;
        this.assessment.Score.axes.forEach(axis => {
            let el_count = 0;
            let j = 0;
            axis.elements.forEach(element => {
                totalCount = 0;
                attachmentsCount = 0;
                //if (element.score != 0 && this.assessment.Axes[i].Elements[j].Goal >= element.score && (this.assessment.Axes[i].Elements[j].AttachementControl > element.score || this.assessment.Axes[i].Elements[j].Attachment.length > 0)) {
                if (element.score != 0 && this.assessment.Axes[i].Elements[j].Goal >= element.score ) {
                    el_count++;
                    if(this.assessment.Axes[i].Elements[j].AttachementControl <= element.score){
                        totalCount++;
                        if(this.assessment.Axes[i].Elements[j].Attachment.length > 0){
                            attachmentsCount++;
                        }
                    }
                }
                
                j++;
            });
            axe_count += Math.round(el_count / axis.elements.length * 100);
            axesPourcentage.push(Math.round(el_count / axis.elements.length * 100));
            i++;
        });
        assessmentPourcentage = Math.round(axe_count / this.assessment.Score.axes.length);
        this.pourcentages = {assessment: assessmentPourcentage, axes: axesPourcentage};


        if (assessmentPourcentage === 100) {
            this.assessment.Status = 1;
            this.updateAssessment(this.selectedElement.axe, this.selectedElement.index);
            /* if(totalCount>0){
                this.isBestInClass = true;
                this.attachmentAlertText = "Vous êtes identifié comme étant « Best-In-class » dans "+totalCount+" élements!\nVous avez partagé "+ attachmentsCount +" bonne pratique !";
            } */
        } else if (this.assessment.Status != 0) {
            this.assessment.Status = 0;
            this.updateAssessment(this.selectedElement.axe, this.selectedElement.index);
        }

    }

    addAction(i, j) {

        let action = new Action('', '', '','',
            this._userService.currentUser._id, new Date(), null, this.assessment.Axes[i].Description, this.assessment.Axes[i].Elements[j].Description,
            this.assessment.Entity, 'In Progress', '', [],false);
            //console.log(action)

        this._actionService.createAction(action).subscribe(response => {
           //console.log(response.data)

            action = response.data;
            this.assessment.Axes[i].Elements[j].Actions.push(action._id);

            let actions = this.dataSource.data;
            actions.push(action)
            this.dataSource.data = actions;

            this.save(i, j);

        })
    }

    deleteAction(k){
       //console.log(this.assessment._id)
        this._actionService.deleteAction(this.dataSource.data[k]._id).subscribe(data=>{
            this.assessment.Axes[this.selectedElement.axe].Elements[this.selectedElement.index].Actions.splice(k,1);
            this.save(this.selectedElement.axe,this.selectedElement.index)
            this.dataSource.data = this.assessment.Axes[this.selectedElement.axe].Elements[this.selectedElement.index].Actions
        })
    }

    saveActions() {
        this.dataSource.data.forEach(action=>{
            if (action.Who != 'Mail'){
                action.WhoMail = '';
            }
        })

        this._actionService.updateActions(this.dataSource.data).subscribe(response => {
            this.dataSource.data = response.data;
        })

    }

    saveActionsAndEmail() {
        this.dataSource.data.forEach(action=>{
            if (action.Who != 'Mail'){
                action.WhoMail = '';
            }
        })
        this._actionService.updateActionsAndEmail(this.dataSource.data).subscribe(response => {
            this.dataSource.data = response.data;
            this._snackbar.open("Les actions ont été envoyées !","Fermer",{
                duration: 3000
            })
        })
    }

    deleteAttachment(element, i) {
        if(this.actionIndex == null){
            let filePath = this.assessment.Axes[this.selectedElement.axe].Elements[this.selectedElement.index].Attachment[i].url || this.assessment.Axes[this.selectedElement.axe].Elements[this.selectedElement.index].Attachment[i];
            //let fileRef = this._storage.ref(filePath);
            //fileRef.delete().subscribe((data) => {
            this._attachmentService.deleteFile(filePath).subscribe( (data)=>{

                this.assessment.Axes[this.selectedElement.axe].Elements[this.selectedElement.index].Attachment.splice(i, 1);
                this.save(this.selectedElement.axe, this.selectedElement.index);
                this.attachments.data.splice(i, 1);
                this.attachments.data = [...this.attachments.data]
                this._attachmentService.deleteAttachment({Path:filePath}).subscribe(data=>{
                })
            })
        }else{
            const filePath = this.dataSource.data[this.actionIndex].Attachment[i];

            //const fileRef = this._storage.ref(filePath);
            //fileRef.delete().subscribe((data) => {
            this._attachmentService.deleteFile(filePath).subscribe( (data)=>{

                let atchmnts = this.dataSource.data;
                if(atchmnts.length == 1) {atchmnts[this.actionIndex].Attachment = [];}
                else {atchmnts[this.actionIndex].Attachment.splice(i, 1);}
                this.dataSource.data = atchmnts;
                this.saveActions();
                this.attachments.data.splice(i, 1);
                this.attachments.data = [...this.attachments.data]

                this._attachmentService.deleteAttachment({Path:filePath}).subscribe(data=>{
                })
            })
        }
    }

    downloadAttachment(element, i) {
        let filePath;
        this.isLoading = true;
        //let fileRef;
        if(this.actionIndex == null){
            filePath = this.assessment.Axes[this.selectedElement.axe].Elements[this.selectedElement.index].Attachment[i];
            //fileRef = this._storage.ref(filePath);
        }else{
            filePath = this.dataSource.data[this.actionIndex].Attachment[i];
            //fileRef = this._storage.ref(filePath);
        }

        let url = environment.apiUrl+'/attachments/get_file';
        //fileRef = this._storage.ref(filePath);
        //fileRef.getDownloadURL().subscribe(function (url) {
        // console.log(url)
        // `url` is the download URL for 'images/stars.jpg'

        var data = '{"name":"'+filePath.url+'"}';

        // This can be downloaded directly:
        var xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);
        xhr.responseType = "blob";
        xhr.setRequestHeader('env',environment.env);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.setRequestHeader('Authorization', 'Bearer '+Cookie.get('token'));
        xhr.onload = () => {
            
            this.isLoading = false;
            var urlCreator = window.URL;
            var imageUrl = urlCreator.createObjectURL(xhr.response);
            var tag = document.createElement('a');
            tag.href = imageUrl;
            tag.download = element.Name;
            document.body.appendChild(tag);
            tag.click();
            document.body.removeChild(tag);
            this.isLoading = false;
        }
        xhr.send(data);
        //})
    }

    onBack = () => {
        this._router.navigate(['/' + AppRoutesConstant.SELF_ASSESSMENT_LIST]);
    }

    onReadMore = (event, score) => {
        event.stopPropagation();
        this.readMoreDescription = score;
        
    }

    @HostListener('window:keydown', ['$event'])
    keyboardInput(event: any) {
        if (event.keyCode === 27) {
            this.readMoreDescription = null;
        }
    }

    private transformer = (node: Entit, level: number) => {
        return {
            expandable: !!node.children && node.children.length > 0,
            name: ''+node.entity.Code+' | '+node.entity.Description,
            entity: node.entity,
            level: level,
        };
    }

    treeControl = new FlatTreeControl<ExampleFlatNode>(
        node => node.level, node => node.expandable);

    treeFlattener = new MatTreeFlattener(
        this.transformer, node => node.level, node => node.expandable, node => node.children);

    treeSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

    initData(){
        this.treeSource.data = [];
        TREE_DATA = [];
        let flag = false;
        this.entitiesService.entities.forEach(entity=>{
            if(entity.Type === "Site"){
                flag = true;
                TREE_DATA.push({entity: entity,children:[]});
                this.entitiesService.entities.forEach(sub=>{
                    for(var i =0 ; i<TREE_DATA[TREE_DATA.length-1].entity.SubEntities.length; i++ ){
                        if(sub._id === TREE_DATA[TREE_DATA.length-1].entity.SubEntities[i]){
                            TREE_DATA[TREE_DATA.length-1].children.push({entity:sub,children:[]});
                            let chilLen = TREE_DATA[TREE_DATA.length-1].children.length;
                            this.entitiesService.entities.forEach(sub1=>{
                                let len = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities.length;                                
                                for(var j = 0 ; j<len; j++ ){
                                    if(sub1._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities[j]){
                                        TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.push({entity:sub1,children:[]});
                                        let chilLen1 = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.length;
                                        this.entitiesService.entities.forEach(sub2=>{
                                            let len2 = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].entity.SubEntities.length;                                
                                            for(var k = 0 ; k<len2; k++ ){
                                                if(sub2._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].entity.SubEntities[k]){
                                                    TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].children.push({entity:sub2,children:[]});
                                                }
                                            }
                                        })
                                    } 
                                }
                            })
                        }    
                    }
                })
            }
        });
        if(!flag){
            this.entitiesService.entities.forEach(entity=>{
                if(entity.Type === "Usine"){
                    flag = true;
                    TREE_DATA.push({entity: entity,children:[]});
                    this.entitiesService.entities.forEach(sub=>{
                        for(var i =0 ; i<TREE_DATA[TREE_DATA.length-1].entity.SubEntities.length; i++ ){
                            if(sub._id === TREE_DATA[TREE_DATA.length-1].entity.SubEntities[i]){
                                TREE_DATA[TREE_DATA.length-1].children.push({entity:sub,children:[]});
                                let chilLen = TREE_DATA[TREE_DATA.length-1].children.length;
                                this.entitiesService.entities.forEach(sub1=>{
                                    let len = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities.length;                                
                                    for(var j = 0 ; j<len; j++ ){
                                        if(sub1._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities[j]){
                                            TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.push({entity:sub1,children:[]});
                                        } 
                                    }
                                })
                            }    
                        }
                    })
                }
            });
        }
        if(!flag){
            this.entitiesService.entities.forEach(entity=>{
                if(entity.Type === "Ligne"){
                    flag = true;
                    TREE_DATA.push({entity: entity,children:[]});
                    this.entitiesService.entities.forEach(sub=>{
                        for(var i =0 ; i<TREE_DATA[TREE_DATA.length-1].entity.SubEntities.length; i++ ){
                            if(sub._id === TREE_DATA[TREE_DATA.length-1].entity.SubEntities[i]){
                                TREE_DATA[TREE_DATA.length-1].children.push({entity:sub,children:[]});
                            }    
                        }
                    })
                }
            });
        }
        if(!flag){
            this.entitiesService.entities.forEach(entity=>{
                if(entity.Type === "Atelier"){
                    flag = true;
                    TREE_DATA.push({entity: entity,children:[]});
                }
            });
        }
        this.treeSource.data = [];
        this.treeSource.data = TREE_DATA;
    }
}


function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
