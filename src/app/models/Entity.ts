export class Entity {
    constructor(
        public _id:string,
        public Code: string,
        public Description: string,
        public Type: string,
        public SubEntities: Array<string>,
        public AssessmentPlan: string,
        public SelfAssesment: Array<string>,
        public Score: number,
        public CalculatedScore: number,
        public ScoreGoal: number
        ){ }
}