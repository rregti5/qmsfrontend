export class AppRoutesConstant {
    public static LOGIN = 'login';

    public static USER_LIST = 'users';
    public static USER_DETAILS = 'users/details';

    public static AXES_LIST = 'axes';
    public static AXES_DETAILS = 'axes/details';

    public static ENTITY_LIST = 'entities';
    public static ENTITY_DETAILS = 'entities/details';

    public static ASSESSMENT_PLAN_LIST = 'assessment';
    public static ASSESSMENT_DETAILS = 'assessment/details';

    public static SELF_ASSESSMENT_VIEW = 'self-assessment';

    public static DASHBOARD = 'dashboard';
}
