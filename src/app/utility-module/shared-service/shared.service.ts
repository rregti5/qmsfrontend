import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Router} from '@angular/router';
import {SharedUserService} from './shared-user.service';
import {APPStorage} from '@constants/storage';
import {ToastStatus} from '@constants/base-constants';
import {User} from '../shared-model/shared-user.model';
import {EncryptionFunctions} from '../shared-functions/encryption-functions';
import {environment} from "@env/environment";
import {AppRoutesConstant} from "../../_constants/app-routes";
import { UserService } from 'app/services/user/user.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Injectable()
export class SharedService extends SharedUserService {

    helper = new JwtHelperService();
    private taskCount = 0;
    private _token = '';
    private msgBody: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    private isLoginRequired: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    private isLoading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor(private router: Router,private userService:UserService) {
        super();
    }

    /* Shared Loader Param */

    getLoader(): Observable<boolean> {
        return this.isLoading.asObservable();
    }

    setToken(value: string): void {
        Cookie.set(APPStorage.TOKEN, EncryptionFunctions.ENCRYPT_OBJ(value),10);
        this._token = value;
    }

    getToken(): string {
        this._token = Cookie.get('token');
        // this._token = EncryptionFunctions.DECRYPT_OBJ(Cookie.get(APPStorage.TOKEN));
        return this._token;
    }

    /* Shared User Token Param */
    isLoggedIn(): boolean {
        return this.IsValidToken(this.getToken()) && this.isValidUser(this.getUser());
    }

    isValidUser(user: User): boolean {
        return !!(user);
    }

    IsValidToken(token: string): boolean {
        let isValid = true;
        try {
            const decodeToken = this.helper.decodeToken(token);
            // const isTokenExpired = this.helper.isTokenExpired();
            let isTokenExpired = false;
            if (decodeToken && decodeToken.hasOwnProperty('exp')) {
                isTokenExpired = this.helper.isTokenExpired();
            }
            if (isTokenExpired) {
                isValid = false;
                // this.setToastMessage('Your session has been expired. Please Login again.', ToastStatus.ERROR);
                this.clearSession();
            }
        } catch (e) {
            isValid = false;
        }
        return isValid;
    }

    setLoader(val: boolean): void {
        if (val) {
            this.taskCount += 1;
        } else {
            this.taskCount -= 1;
            if (this.taskCount !== 0) {
                val = true;
            }
        }
        this.isLoading.next(val);
    }


    getToastMessage(): Observable<any> {
        return this.msgBody.asObservable();
    }

    clearSession() {
        this.setToken(null);
        this.setUser(null);
        this.setLoginRequired(false);
        Cookie.deleteAll();
        localStorage.clear();
    }

    logout(): void {
        this.clearSession();
        Cookie.set('token', null,10);
        this.userService.token = null;
        this.router.navigate(['/' + AppRoutesConstant.LOGIN]);
    }

    /* setting route */
    public setToastMessage(message: any, type: ToastStatus, title = '') {
        let body = null;
        if (message) {
            body = {
                message,
                type,
                title
            };
        }
        this.msgBody.next(body);
    }

    /* Shared LoggedIn Param */

    getLoginRequired(): Observable<boolean> {
        return this.isLoginRequired.asObservable();
    }

    setLoginRequired(val: boolean): void {
        this.isLoginRequired.next(val);
    }

} 
