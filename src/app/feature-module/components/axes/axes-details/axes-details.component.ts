import {Component, OnInit} from '@angular/core';
import {AppRoutesConstant} from "../../../../_constants/app-routes";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {CommonRegexp} from "@constants/*";
import {AxesService, EntitiesService} from "../../../services";
import {addElement} from "../../../../utility-module/shared-functions/common-functions";
import { TranslateService } from '@ngx-translate/core';
import { environment } from '@env/environment';
import {  FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';
import { Cookie } from 'ng2-cookies/ng2-cookies';

const URL = environment.apiUrl+"/axes/Axe_upload"

@Component({
    selector: 'app-axes-details',
    templateUrl: './axes-details.component.html',
    styleUrls: ['./axes-details.component.scss']
})
export class AxesDetailsComponent implements OnInit {

    axesForm: FormGroup;
   // url = environment.apiUrl
    public uploader: FileUploader = new FileUploader({url: URL, itemAlias: 'axeExcel'});
    constructor(private _router: Router,
                private _axesService: AxesService,
                private _fb: FormBuilder,public translate: TranslateService) {
                    if(Cookie.get('language')!==null)
                    translate.setDefaultLang(Cookie.get('language'));
    }

    /**
     * convenience getter for easy access to form fields
     */
    get formControls() {
        return this.axesForm.controls;
    }

    get axesElements() {
        return this.formControls['elements'];
    }

    ngOnInit() {
        this.createAxesForm();
         //override the onAfterAddingfile property of the uploader so it doesn't authenticate with //credentials.
       this.uploader.onAfterAddingFile = (file)=> { file.withCredentials = false; };
       //overide the onCompleteItem property of the uploader so we are 
       //able to deal with the server response.
       this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
            //console.log("ExcelUpload:uploaded:", item, status, response);
            this.onBack();
        };
        
    }

    createAxesForm = () => {
        this.axesForm = this._fb.group({
            description: ['', [
                <any>Validators.required
            ]],
            elements: ['', [
                <any>Validators.required,
                <any>Validators.minLength(1),
                <any>Validators.maxLength(2),
                <any>Validators.pattern(CommonRegexp.NUMERIC_REGEXP)
            ]]
        })
    };

    /**
     * submit axes method
     * @param form
     */
    onSubmitAxesForm = (form: FormGroup) => {
        if (form.valid) {
            const formValue = form.value;
            let params = {
                Description: formValue.description,
                Visible : true,
            };
            const numberOfElements: number = +formValue.elements;
            let elementsArray = [];
            if (!isNaN(numberOfElements)) {
                // elementsArray.length = numberOfElements;
                for (let i = 1; i <= numberOfElements; i++) {
                    elementsArray.push(addElement(i));
                }
                params['Elements'] = elementsArray;
            }
            this._axesService.saveAxes(params).subscribe(response => {
                // console.log(response);
                this.onBack();
            })
        }
    };

    onBack = () => {
        this._router.navigate(['/' + AppRoutesConstant.AXES_LIST]);
    }
}
