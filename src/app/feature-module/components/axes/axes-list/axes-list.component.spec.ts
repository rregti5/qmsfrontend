import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AxesListComponent } from './axes-list.component';

describe('AxesUpdateComponent', () => {
  let component: AxesListComponent;
  let fixture: ComponentFixture<AxesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AxesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AxesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
