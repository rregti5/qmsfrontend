import {Component, OnInit, HostListener} from '@angular/core';
import {Router} from "@angular/router";
import {AppRoutesConstant} from "../../../../_constants/app-routes";
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {FlatTreeControl} from '@angular/cdk/tree';
import { Entity } from '../../../../models/Entity';
import {EntititesService} from "../../../../services/entities/entitites-service.service";
import { AssessmentPlanService } from 'app/feature-module/services';
import { TranslateService } from '@ngx-translate/core';
import { FormControl, Validators } from '@angular/forms';
import { Cookie } from 'ng2-cookies/ng2-cookies';

interface Entit {
    entity: Entity;
    children?: Entit[];
}

var TREE_DATA: Entit[] = [];

/** Flat node with expandable and level information */
interface ExampleFlatNode {
    expandable: boolean;
    name: string;
    level: number;
}

@Component({
    selector: 'app-entities-details',
    templateUrl: './entities-details.component.html',
    styleUrls: ['./entities-details.component.scss']
})
export class EntitiesDetailsComponent implements OnInit {

    isEditable : boolean = false;
    isMenu = false;
    entityForm = false;
    formText: any = '';
    isDeleteEntity = false;
    public currentEntity: Entity = new Entity(null,null,null,null,null,null,null,null,null,null);
    parentEntityCode:string;
    public parentEntityType:string;

    code = new FormControl("",Validators.compose([
        Validators.required,
    ]));
    description = new FormControl("", Validators.compose([
        Validators.required,
    ]))
    scoreGoal = new FormControl("",Validators.compose([
        Validators.required,
    ]));
    assessmentPlan = new FormControl("", Validators.compose([
        Validators.required,
    ]));
    type = new FormControl("", Validators.compose([
        Validators.required,
    ]))

    constructor(private _router: Router,
        public entitiesService:EntititesService,
        public assessmentPlanService: AssessmentPlanService,public translate: TranslateService) {
            if(Cookie.get('language')!==null)
            translate.setDefaultLang(Cookie.get('language'));
        //this.dataSource.data = TREE_DATA;
    }

    ngOnInit() {
        
        this.assessmentPlanService.getAllAssessmentPlans().subscribe(data=>{
            //console.log(data);
            this.assessmentPlanService.assessmentPlans = data.data;
            this.entitiesService.getAllEntities().subscribe((data)=>{
                /* console.log(this.entitiesService.entities);
                console.log(data); */
                this.initData();
            });
        })
    }

    showInfo(type,entity?){
        //console.log(entity)
        this.parentEntityCode = null;
        this.parentEntityType = null;
        if(Cookie.get('language')=="fr" && type=="Add")
        this.formText = "AJOUTER";
        else if(Cookie.get('language')=="fr" && type=="Edit")
          this.formText="MODIFIER"
          else
          this.formText=type;
        switch (type) {
            case 'Add':
                this.isEditable = true;
                if(entity!=null) {
                    this.parentEntityCode = entity.Code;
                    this.parentEntityType = entity.Type;
                    //console.log(this.parentEntityType)
                }
                this.entityForm=true;
                this.currentEntity = new Entity(null,null,null,null,null,null,null,null,null,null);
                break;
            case 'Edit':
                this.isEditable = true;
                this.entityForm=true;
                this.currentEntity = entity;
                break;
            case 'Show':
                this.entityForm=true;
                this.currentEntity = entity;
                break;
            default:
                break;
        }

    }

    hideInfo = () => {

    }

    onBack = () => {
        this._router.navigate(['/' + AppRoutesConstant.ENTITY_LIST]);
    }

    private transformer = (node: Entit, level: number) => {
        return {
            expandable: !!node.children && node.children.length > 0,
            name: ''+node.entity.Code+' | '+node.entity.Description,
            entity: node.entity,
            level: level,
        };
    }

    treeControl = new FlatTreeControl<ExampleFlatNode>(
        node => node.level, node => node.expandable);

    treeFlattener = new MatTreeFlattener(
        this.transformer, node => node.level, node => node.expandable, node => node.children);

    dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

    @HostListener('window:keydown', ['$event'])
    keyboardInput(event: any) {
        if (event.keyCode === 27) {
            this.isDeleteEntity = false;
        }
    }

    initData(){
        this.dataSource.data = [];
        TREE_DATA = [];
        this.entitiesService.entities.forEach(entity=>{
            if(entity.Type === "Site"){
                TREE_DATA.push({entity: entity,children:[]});
                this.entitiesService.entities.forEach(sub=>{
                    for(var i =0 ; i<TREE_DATA[TREE_DATA.length-1].entity.SubEntities.length; i++ ){
                        if(sub._id === TREE_DATA[TREE_DATA.length-1].entity.SubEntities[i]){
                            TREE_DATA[TREE_DATA.length-1].children.push({entity:sub,children:[]});
                            let chilLen = TREE_DATA[TREE_DATA.length-1].children.length;
                            this.entitiesService.entities.forEach(sub1=>{
                                let len = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities.length;                                
                                for(var j = 0 ; j<len; j++ ){
                                    if(sub1._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities[j]){
                                        TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.push({entity:sub1,children:[]});
                                        let chilLen1 = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.length;
                                        this.entitiesService.entities.forEach(sub2=>{
                                            let len2 = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].entity.SubEntities.length;                                
                                            for(var k = 0 ; k<len2; k++ ){
                                                if(sub2._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].entity.SubEntities[k]){
                                                    TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].children.push({entity:sub2,children:[]});
                                                }
                                            }
                                        })
                                    } 
                                }
                            })
                        }    
                    }
                })
            }
        });
        this.dataSource.data = [];
        this.dataSource.data = TREE_DATA;
    }

    addEntity(){
        //console.log(this.currentEntity)
        if(this.formText==='Add' || this.formText==='AJOUTER'){
            //console.log(this.currentEntity)
            //console.log(this.parentEntityCode)
            this.entitiesService.addEntity(this.currentEntity).subscribe(data=>{
                //console.log(data);
                if(!data.error){
                    this.currentEntity._id=data.entity._id;
                    if(this.parentEntityCode != null){
                        this.entitiesService.entities.forEach(ent=>{
                            if(ent.Code === this.parentEntityCode){
                                ent.SubEntities.push(data.entity._id);
                                this.entitiesService.updateEntity(ent).subscribe(data=>{
                                    this.entitiesService.getAllEntities().subscribe((data)=>{
                                        //console.log(this.entitiesService.entities);
                                        this.initData();
                                        this.entityForm=false;
                                        //console.log(TREE_DATA);
                                    });
                                })
                            }
                        })
                    }else{
                        this.entitiesService.getAllEntities().subscribe((data)=>{
                            //console.log(this.entitiesService.entities);
                            this.initData();
                            this.entityForm=false;
                            //console.log(TREE_DATA);
                        });
                    }
    
                }
            })
        }else{
            this.entitiesService.updateEntity(this.currentEntity).subscribe(data=>{
                //console.log(data);
                this.entitiesService.getAllEntities().subscribe((data)=>{
                    //console.log(this.entitiesService.entities);
                    this.initData();
                    this.entityForm=false;
                    //console.log(TREE_DATA);
                });
            })
        }
        this.isEditable = false;
    }

    deleteEntity(){
        this.entitiesService.deleteEntity(this.currentEntity).subscribe(data=>{
            //console.log(data);
            this.entitiesService.getAllEntities().subscribe((data)=>{
                //console.log(this.entitiesService.entities);
                this.initData();
                this.isDeleteEntity = false;
                //console.log(TREE_DATA);
            });
        })
    }
}
