import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AppRoutesConstant} from "../../../../_constants/app-routes";
import { User } from '../../../../models/user';
import { UserService } from '../../../../services/user/user.service';
import { EntititesService } from '../../../../services/entities/entitites-service.service';
import { Entity } from 'app/models/Entity';
import { AssessmentPlanService } from 'app/feature-module/services';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material';
import { FlatTreeControl } from '@angular/cdk/tree';
import { TranslateService } from '@ngx-translate/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';


interface Entit {
  entity: Entity;
  children?: Entit[];
}

var TREE_DATA: Entit[] = [];

/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}



@Component({
  selector: 'app-user-details',
  templateUrl: 'user-details.component.html',
  styleUrls: ['user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
labeluser:String;
  constructor(private _router : Router, public userService:UserService,
    public entititesService:EntititesService,
    private _assessmentPlanService: AssessmentPlanService,public translate: TranslateService) {
      if(Cookie.get('language')!==null)
      translate.setDefaultLang(Cookie.get('language'));
    }

  currentUser: User=new User(null,null,null,[],null,this.makePassword(8),null,null);

  ngOnInit() {
    this.entititesService.getAllEntities().subscribe(data=>{
      if(this.userService.currentUser.Role <= 2 ){
        let entities = [];
        data.entities.forEach(entity=>{
          if(this.userService.currentUser.Entity.includes(entity._id)){
            entities.push(entity);
          }
        })
        this.entititesService.entities = entities;
      }
    })
    if(this.userService.addType == 'Edit'){
      this.currentUser = this.userService.userToEdit;
      if(Cookie.get('language')==='fr')
      this.labeluser = 'Modifier'
      else
      this.labeluser="Edit"
      //console.log(this.currentUser)
    }
    else{
      if(Cookie.get('language')==='fr')
      this.labeluser = 'Ajouter'
      else
      this.labeluser="Add"
    }
    this._assessmentPlanService.getAllAssessmentPlans().subscribe(data=>{
      //console.log(data);
      this._assessmentPlanService.assessmentPlans = data.data;
      this.entititesService.getAllEntities().subscribe((data)=>{
        if(this.userService.currentUser.Role <= 2 ){
          let entities = [];
          data.entities.forEach(entity=>{
            if(this.userService.currentUser.Entity.includes(entity._id)){
              entities.push(entity);
            }
          })
          this.entititesService.entities = entities;
        }
          this.initData();
      });
    })
  }

  makePassword(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }


  onBack = () => {
    this._router.navigate(['/' + AppRoutesConstant.USER_LIST]);
  }

  resetPassword(){
    console.log(this.currentUser)
    this.currentUser.Password = this.makePassword(8);
    this.userService.resetPassword(this.currentUser).subscribe(data=>{
      if(!data.error) this.onBack();
    })
  }

  addUser(){
    //console.log(this.currentUser)
   
    if(this.userService.addType==='Add'){ 
      this.currentUser.Email=this.currentUser.Email.toLowerCase();
      this.userService.addUser(this.currentUser).subscribe(data=>{
        if(!(data.status!=null)){
          this.onBack();
        }
      })
    }
    else{
      this.userService.updateUser(this.currentUser).subscribe(data=>{
        if(!(data.status!=null)){
          this.onBack();
        }
      })
    }
  }

  private transformer = (node: Entit, level: number) => {
    return {
        expandable: !!node.children && node.children.length > 0,
        name: ''+node.entity.Code+' | '+node.entity.Description,
        entity: node.entity,
        level: level,
    };
  }

treeControl = new FlatTreeControl<ExampleFlatNode>(
    node => node.level, node => node.expandable);

treeFlattener = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.children);

treeSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

initData(){
    this.treeSource.data = [];
    TREE_DATA = [];
    this.entititesService.entities.forEach(entity=>{
        if(entity.Type === "Site"){
            TREE_DATA.push({entity: entity,children:[]});
            this.entititesService.entities.forEach(sub=>{
                for(var i =0 ; i<TREE_DATA[TREE_DATA.length-1].entity.SubEntities.length; i++ ){
                    if(sub._id === TREE_DATA[TREE_DATA.length-1].entity.SubEntities[i]){
                        TREE_DATA[TREE_DATA.length-1].children.push({entity:sub,children:[]});
                        let chilLen = TREE_DATA[TREE_DATA.length-1].children.length;
                        this.entititesService.entities.forEach(sub1=>{
                            let len = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities.length;                                
                            for(var j = 0 ; j<len; j++ ){
                                if(sub1._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities[j]){
                                    TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.push({entity:sub1,children:[]});
                                    let chilLen1 = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.length;
                                    this.entititesService.entities.forEach(sub2=>{
                                        let len2 = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].entity.SubEntities.length;                                
                                        for(var k = 0 ; k<len2; k++ ){
                                            if(sub2._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].entity.SubEntities[k]){
                                                TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].children.push({entity:sub2,children:[]});
                                            }
                                        }
                                    })
                                } 
                            }
                        })
                    }    
                }
            })
        }
    });
    this.treeSource.data = [];
    this.treeSource.data = TREE_DATA;
  }

  entitySelected(event){
    console.log(event)
    this.entititesService.entities.forEach(entity=>{
      if(entity._id === event.value[event.value.length - 1]){
        entity.SubEntities.forEach(sub1=>{
          if(this.currentUser.Entity.indexOf(sub1) == -1) this.currentUser.Entity.push(sub1);
          this.entititesService.entities.forEach(subEnt=>{
            if(subEnt._id === sub1){
              subEnt.SubEntities.forEach(sub2=>{
                if(this.currentUser.Entity.indexOf(sub2) == -1) this.currentUser.Entity.push(sub2);
                this.entititesService.entities.forEach(subEnt2=>{
                  if(subEnt2._id === sub2){
                    subEnt2.SubEntities.forEach(sub3=>{
                      if(this.currentUser.Entity.indexOf(sub3) == -1) this.currentUser.Entity.push(sub3);
                    })
                  }
                })
              })
            }
          })
        })
      }
    })
  }

}
