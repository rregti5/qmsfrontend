import {  Component, NgModule, OnInit, ViewChild  } from '@angular/core';
import {  MatSort, MatTableDataSource, Sort, MatPaginator } from "@angular/material";
import {  Router  } from "@angular/router";
import {  AppRoutesConstant } from "../../../../_constants/app-routes";
import { SelfAssessmentService,AssessmentPlanService } from 'app/feature-module/services';
import { EntititesService } from 'app/services/entities/entitites-service.service';
import {  MatChipsModule } from "@angular/material";

import {PAGE_SIZE_OPTIONS} from "@constants/*";
import { DashboardService } from 'app/feature-module/services/dashboard/dashboard.service';
import { UserService } from 'app/services/user/user.service';
import {saveAs} from 'file-saver';
import {ReportsService} from "../../../services/reports/reports.service";
import { TranslateService } from '@ngx-translate/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';
interface Assessment {
  Description: string,
  CreatedBy: string,
  createdOn:string,
  entity:string,
  Entity: string,
  AssessmentId: string,
  Axes: Array<any>,
  Code: string,
  Status: number,
  Actions: Array<any>,
  CreatedOn: Date,
  _id: string;
}

const ELEMENT_DATA: Assessment[] = []
/*   {position: 1, id: '123456789', entity: 'SA-01MC', date: '18 March 2019', score : '3', goal : '5', action : ''},
  {position: 2, id: '123456789', entity: 'SA-01MC', date: '18 March 2019', score : '3', goal : '5', action : ''},
  {position: 3, id: '123456789', entity: 'SA-01MC', date: '18 March 2019', score : '3', goal : '5', action : ''},
  {position: 4, id: '123456789', entity: 'SA-01MC', date: '18 March 2019', score : '3', goal : '5', action : ''},
  {position: 5, id: '123456789', entity: 'SA-01MC', date: '18 March 2019', score : '3', goal : '5', action : ''},
  {position: 6, id: '123456789', entity: 'SA-01MC', date: '18 March 2019', score : '3', goal : '5', action : ''}
]; */

@Component({
  selector: 'app-self-assessment-list',
  templateUrl: './self-assessment-list.component.html',
  styleUrls: ['./self-assessment-list.component.scss']
})
@NgModule({
  imports: [MatChipsModule]
})
export class SelfAssessmentListComponent implements OnInit {

  color : string;

  isLoading:boolean = false;

  displayedColumns: string[] = ['Position', '_id', 'entity', 'createdOn', 'Score', 'Goal', 'Status', 'Action'];
  dataSource = new MatTableDataSource(ELEMENT_DATA);

  assessmentToDelete : any;
  isDeleteAssessment : boolean = false;

  // Pagination Variables
  isLoadingResults = true;
  pageSizeOptions = PAGE_SIZE_OPTIONS;
  pageSize = PAGE_SIZE_OPTIONS[1];
  pageIndex = 0;
  resultsLength = 0;
  filteredAssessments : Array<any> = [];
  data:any
  assess: any = null;
  assessment: any = null;
  assessments: Array<any>=[];

  date = new Date().toLocaleString();
  assessmentPlans : Array<any>;
  scores: any;
  goals: any;
  pourcentages:any;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  
    

  applyFilter(filterValue: string) {
    this.dataSource = new MatTableDataSource(this.assessments);
    this.resultsLength = this.filteredAssessments.length;
    this.filteredAssessments = this.assessments.filter((elem, index) => {
      return (elem.Code.toLowerCase().includes(filterValue.toLowerCase()) || elem.createdOn.toLowerCase().includes(filterValue.toLowerCase()) 
      || elem.entity.toLowerCase().includes(filterValue.toLowerCase()) )
    })
    this.resultsLength = this.filteredAssessments.length;
    this.dataSource.data = this.filteredAssessments.filter((elem, index) => {
      return index >= this.pageIndex * this.pageSize && index < ((this.pageIndex + 1) * this.pageSize);
    })
    this.dataSource.sort = this.sort;
  }

  constructor(private _router : Router,
    private _selfAssessmentService: SelfAssessmentService,
    private _entitiesService: EntititesService,
    private _userService: UserService,
    private _dashboardService: DashboardService, private _assPlanService : AssessmentPlanService,
    private _reportsService: ReportsService,public translate: TranslateService) {
      if(Cookie.get('language')!==null)
      translate.setDefaultLang(Cookie.get('language'));
     //console.log(Cookie.get('language'));
    this._userService.getAllUsers().subscribe(users=>{
      this._userService.users = users;
    })
    this._entitiesService.getAllEntities().subscribe(data=>{
      this._entitiesService.entities = data.entities;
    })
    this._assPlanService.getAllAssessmentPlans().subscribe(response=>{
      this.assessmentPlans = response.data
    })
    /* this._userService.getCurrentUser().subscribe(data=>{
      this._userService.currentUser = data;
      if(data.Role <= 2 && data.Entity.length>0){
        this._selfAssessmentService.getSelfAssessmentsByEntities(data.Entity).subscribe(data=>{
          this.assessments = data.assessments;
          //console.log(this.assessments.length)
          this.assessment = this.assessments[0];
        })
      }else{
        this._selfAssessmentService.getAllSelfAssessment().subscribe(data=>{
          this.assessments= data['All assesments'];
          this.assessment = this.assessments[0];
        })
      }

    }) */
  }
  public entities : Array<any>=[];

  public currUser;

  ngOnInit() {
    this._userService.getCurrentUser().subscribe(data=>{
      this.currUser = data;
      this.updateAssessmentsList();
      this.dataSource.sort = this.sort;
      
    });
  }
  generateExcel = (ele) => {
    //this.assessment = ele;
    this.isLoading = true;
    let data : any;

    /* if( Cookie.get('language') == 'en')
      data = this.loadAssessment(ele,"B1esUVWP84");
    else */
      data = this.loadAssessment(ele,"B1esUVWP50");

  };
  generatePdf = (ele) => {
    //console.log(this.assessment)
    //this.assessment = ele;
    //console.log(ele);
    this.isLoading = true;
    let data : any;
    //console.log(Cookie.get('language'));
    /* if( Cookie.get('language') == 'en')
      data = this.loadAssessment(ele,"B1gxGoRVYE");
    else */
      data = this.loadAssessment(ele,"B1gxGoRVYG");

    /*let body = {
      "template": {"shortid": "B1gxGoRVYE"},
      "data": this.data
    }

    this._reportsService.getAssessmentReport(body).subscribe((data)=> {
       saveAs(data, this.data.Code + '-' + this.data.date);
      setTimeout(function(){
        this.isLoading = false;
        this.isLoadingResults = false;
       //console.log("dddd")
        }, 1000);

      this.isLoading = false;


    }, err=> {
      alert("Problem while downloading the file.");
      console.error(err);
      this.isLoading = false;
    })*/


  };
  loadAssessment(assessment,shortId){

    this._selfAssessmentService.getSelfAssessment(assessment._id).subscribe(responseAssessment=>{
      assessment = responseAssessment;
      //let assessment = this.assessment;
      //this.assess = assessment._id
      this.date = new Date().toLocaleString();
      assessment.CreatedOn = this.changeDateFormat(assessment.CreatedOn);
      this._userService.users.forEach(user=>{
        if(user._id === assessment.CreatedBy) assessment.CreatedBy = user.Name + ' | ' +user.Email;
      })

      this.assessmentPlans.forEach(assPlan=>{
        if (assPlan._id === assessment.AssessmentId) assessment.AssessmentId = assPlan.Description;
      })
      this._dashboardService.getAssessmentsScores([assessment._id]).subscribe(response=>{
        this.scores = response.data[0].assesment_score;
        this.goals = response.data[0].assesment_goals;
        this._entitiesService.entities.forEach(entity=>{
          if (entity._id === assessment.Entity) {
            assessment.Entity = entity.Code + ' | ' +entity.Description;
            //this.goals.goal = entity.ScoreGoal || 4;
            this.goals.goal = this.goals.goal || 4;
          }
        })
        let axes = [];
        let ax_count = 0;
        this.scores.axes.forEach((axis,i) => {
          let el_count = 0;
          assessment.Axes[i].score = this.scores.axes[i].score;
          assessment.Axes[i].goal = this.goals.axes[i].goal;
          axis.elements.forEach((element,j) => {
            assessment.Axes[i].Elements[j].score = this.scores.axes[i].elements[j].score;
            assessment.Axes[i].Elements[j].goal = this.goals.axes[i].elements[j].goal;
            if(element.score > 0 && this.goals.axes[i].elements[j].goal > 0 ){
              el_count = el_count + 1;
            }
          });
          axes.push(Math.round(el_count / axis.elements.length * 1000)/10);
          assessment.Axes[i].pourcentage = axes[axes.length-1];
          if( el_count === axis.elements.length) ax_count++;
        });
        this.pourcentages = {pourcentage:Math.round(ax_count / this.scores.axes.length * 1000)/10,axes:axes};
        this.data = assessment;
        this.data.date = this.date;
        this.data.scores = this.scores;
        this.data.goals = this.goals.goal;
        this.data.pourcentages = this.pourcentages;
          
        let body = {
          "template": {"shortid": shortId},
          "data": this.data
        }

        this._reportsService.getAssessmentReport(body).subscribe((data)=> {
          //console.log(data);
          saveAs(data, this.data.Code + '-' + this.data.date);
          setTimeout(function(){
            this.isLoading = false;
            this.isLoadingResults = false;
          }, 1000);

          this.isLoading = false;


        }, err=> {
          alert("Problem while downloading the file :\n"+err);
        //console.log(err);
          this.isLoading = false;
        })

      // this.assessment = assessment;
      })
    });
  }
  updateAssessmentsList(){
    this.assessments =[];
    this.filteredAssessments = [];
    if(this._userService.currentUser.Role>2){
      this._selfAssessmentService.getAllSelfAssessmentShort(this.pageIndex,this.pageSize).subscribe(data=>{
        let assessments = data['All assesments'];
        //console.log(assessments)
        let asses = [];
        let ids =[];
        assessments.forEach(element => {
          let flag = false;
          this._entitiesService.entities.forEach(entity=>{
            if(entity._id === element.Entity) flag = true;
          })
          if(flag) {
            ids.push(element._id);
            asses.push(element)
          }
        });
        this._dashboardService.getAssessmentsScores(ids).subscribe(score=>{
          //console.log(score)
          let j = 0;
          this.assessments = []
          asses.forEach(ass => {
            this.assessments.push(ass);
            for( var i = 0; i < this._entitiesService.entities.length; i++){
              if (this._entitiesService.entities[i]._id === ass.Entity){
                ass.entity = ''+this._entitiesService.entities[i].Code+' | '+this._entitiesService.entities[i].Description;
                //ass.Goal = this._entitiesService.entities[i].ScoreGoal;
                ass.Goal = score.data[j].assesment_goals.goal;
                ass.createdOn = this.changeDateFormat(ass.CreatedOn);
                ass.Score = score.data[j].assesment_score.score;
                this.assessments[this.assessments.length-1] = ass;
                break;
              }
            }
            j++;
          });
          this.filteredAssessments = this.assessments
          this.dataSource = new MatTableDataSource(this.filteredAssessments);
          //this.resultsLength = this.filteredAssessments.length;
          this.resultsLength = data.count;
          this.dataSource.data= this.assessments.filter((elem, index) => {
              return index < this.pageSize
          })
          this.dataSource.sort = this.sort;
          /* this.sortData({active:'ModifiedOn',direction:'asc'})
          this.sortData({active:'createdOn',direction:'desc'})
          this.sortData({active:'createdOn',direction:''}) */
        })
      })
    }else{
      this._selfAssessmentService.getSelfAssessmentsByEntities(this._userService.currentUser.Entity,this.pageIndex,this.pageSize).subscribe(data=>{
        //console.log(data);
        let assessments = data.assessments;
        let ids =[];
        assessments.forEach(element => {
          ids.push(element._id)
        });
        this._dashboardService.getAssessmentsScores(ids).subscribe(score=>{
          let j = 0;
          this.assessments = []
          data['assessments'].forEach(ass => {
            this.assessments.push(ass);
            for( var i = 0; i < this._entitiesService.entities.length; i++){
              if (this._entitiesService.entities[i]._id === ass.Entity){
                ass.entity = ''+this._entitiesService.entities[i].Code+' | '+this._entitiesService.entities[i].Description;
                //ass.Goal = this._entitiesService.entities[i].ScoreGoal;
                ass.Goal = score.data[j].assesment_goals.goal;
                ass.createdOn = this.changeDateFormat(ass.CreatedOn);
                ass.Score = score.data[j].assesment_score.score;
                this.assessments[this.assessments.length-1] = ass;
                break;
              }
            }
            j++;
          });
          this.filteredAssessments = this.assessments;
          this.dataSource = new MatTableDataSource(this.assessments);

          //this.resultsLength = this.filteredAssessments.length;
          this.resultsLength = data.count;
          this.dataSource.data= this.assessments.filter((elem, index) => {
              return index < this.pageSize
          })
          this.dataSource.sort = this.sort;
          this.sortData({active:'createdOn',direction:'asc'})
          this.sortData({active:'createdOn',direction:'desc'})
          this.sortData({active:'createdOn',direction:''})

          document.getElementById('filterBox').setAttribute('value','');

        })
      })
    }
  }


  onAddSelfAssessment = () => {
    SelfAssessmentService.selfAssessmentToEdit = null;
    this._router.navigate(['/' + AppRoutesConstant.SELF_ASSESSMENT_VIEW])
  }

  onEditSelfAssessment(assessment){

    this._selfAssessmentService.getSelfAssessment(assessment._id).subscribe(data=>{
      SelfAssessmentService.selfAssessmentToEdit = data;
      this._router.navigate(['/' + AppRoutesConstant.SELF_ASSESSMENT_VIEW]);
    })
 
  }

  onDeleteAssessment(){
    this._selfAssessmentService.deleteSelfAssessment(this.assessmentToDelete).subscribe(data=>{
      //this.updateAssessmentsList()
      this.isDeleteAssessment = false;
      this.assessments.splice(this.assessments.indexOf(this.assessmentToDelete),1);
      this.filteredAssessments.splice(this.filteredAssessments.indexOf(this.assessmentToDelete),1);
      this.dataSource = new MatTableDataSource(this.filteredAssessments);
      this.resultsLength = this.filteredAssessments.length;
      this.dataSource.data= this.filteredAssessments.filter((elem, index) => {
          return index < this.pageSize
      })
      this.dataSource.sort = this.sort;
      this.sortData({active:'createdOn',direction:'asc'})
      this.sortData({active:'createdOn',direction:'desc'})
      this.sortData({active:'createdOn',direction:''})

      /* this.filteredAssessments = this.assessments;
      this.dataSource = new MatTableDataSource(this.assessments);
      this.resultsLength = this.assessments.length;
      this.dataSource.data= this.assessments.filter((elem, index) => {
          return index < this.pageSize
      })
      this.dataSource.sort = this.sort;
      this.sortData({active:'createdOn',direction:'asc'})
      this.sortData({active:'createdOn',direction:'desc'})
      this.sortData({active:'createdOn',direction:''}) */

      /* let inpt : HTMLInputElement = <HTMLInputElement> document.getElementById('filterBox');

     //console.log(inpt.value)
      inpt.value = "";
     //console.log(inpt.value) */
      
    })
  }


  changeDateFormat(DATE){
    let date = new Date(DATE);
        let years = date.getFullYear();

        var months,hours,minutes,seconds,days;

        if(date.getMonth()<9){
          months = '0'+ (date.getMonth()+1);
        } else{
          months = (date.getMonth()+1);
        }
        if(date.getDate()<10){
          days = '0'+ (date.getDate());
        } else{
           days = (date.getDate());
        }
        if(date.getMinutes()<10){
           minutes = '0'+ (date.getMinutes());
        } else{
           minutes = (date.getMinutes());
        }
        if(date.getSeconds()<10){
           seconds = '0'+ (date.getSeconds());
        } else{
           seconds = (date.getSeconds());
        }
        if(date.getHours()<10){
           hours = '0'+ (date.getHours());
        } else{
           hours = (date.getHours());
        }

        let stringDate = years +'-'+months+"-"+days+' '+hours+':'+minutes+':'+seconds;

        return stringDate;
  }

  changePage = (event) => {
    const {pageIndex, pageSize} = event;
    this.pageIndex = pageIndex;
    this.pageSize = pageSize;
    this.updateAssessmentsList();
    this.bindDisplayAssessmentsList();
  }

  bindDisplayAssessmentsList = () => {
    this.dataSource.data = this.filteredAssessments.filter((elem, index) => {
        return index >= this.pageIndex * this.pageSize && index < ((this.pageIndex + 1) * this.pageSize);
    })
    this.dataSource.sort = this.sort;
    //console.log(this.dataSource.data.length)
  }

  sortData(sort: Sort) {
    const data = this.filteredAssessments.slice();
    if (!sort.active || sort.direction === '') {
      this.dataSource.data = data;
      //console.log(this.dataSource.data.length)
      this.bindDisplayAssessmentsList();
      return;
    }

    this.filteredAssessments = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case '_id': return compare(a._id, b._id, isAsc);
        case 'entity': return compare(a.entity, b.entity, isAsc);
        case 'createdOn': return compare(a.createdOn, b.createdOn, isAsc);
        default: return 0;
      }
    });
    this.bindDisplayAssessmentsList();

  }

  publish(assessm){
    if(assessm.Status == 1) assessm.Status = 2;
    else assessm.Status = 1;
    assessm.PublishedOn = new Date();
    this._selfAssessmentService.updatSelfAssessment(assessm).subscribe(data=>{
      //console.log(data);
      
    },err=>{
     
        this.updateAssessmentsList();
        this.dataSource.sort = this.sort;
      
    })
  }
  

}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
