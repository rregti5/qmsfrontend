const express = require('express');
const http = require('http');
const path = require('path');
const secure = require('express-force-https');

//const api = require('./server/routes/api');
const app = express();
app.use(secure);

app.use(express.static(path.join(__dirname, 'dist')));

app.get('*', (req,res, next)=>{
    res.sendFile(path.join(__dirname, 'dist/index.html'));
});

const port = process.env.PORT || '3001';
console.log(port)
app.set('port', port);
const server = http.createServer(app);


server.listen(port,()=>console.log('Running'));