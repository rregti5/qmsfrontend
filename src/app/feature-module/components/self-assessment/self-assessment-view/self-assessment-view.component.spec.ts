import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfAssessmentViewComponent } from './self-assessment-view.component';

describe('SelfAssessmentViewComponent', () => {
  let component: SelfAssessmentViewComponent;
  let fixture: ComponentFixture<SelfAssessmentViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfAssessmentViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfAssessmentViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
