import { TestBed } from '@angular/core/testing';

import { AssessmentPlanService } from './assessment-plan.service';

describe('AssessmentPlanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AssessmentPlanService = TestBed.get(AssessmentPlanService);
    expect(service).toBeTruthy();
  });
});
