import { ScoreModelNew } from 'app/feature-module/models';

// declare const $;

export const isEmpty = (obj): boolean => Object.keys(obj).length === 0 && obj.constructor === Object;

export const AppLogger = (value: any) => {
    // console.log(`<------------------------------------ ${value} ------------------------------------>`);
};


export const ScoreDescirptionColorList = ['#FF0000','#2A2ADA','#007E00','#000000','#800909'];

export const addElement = (i?: number) => {
    const scores = []
    for(let j=1; j<=5; j++){
        scores[j-1]=createScoreArray(j)
    }
    return {
        Description: `${i ? `Element ${i}` : `New Element`}`,
        Comment: '',
        AttachementControl: 4,
        Attachment: [],
        Scores:scores
    }
}

export const createScoreArray = (note:number,scoreDesc='Score') : ScoreModelNew=>{
    const descriptions = []
    for(let i=1; i<=5; i++){
        descriptions[i-1]=`${scoreDesc} ${note}`
    }
    return {
        Note:note,Selected:false,Description:descriptions
    }
}