import { Injectable } from '@angular/core';
import { APIManager } from '@services/apimanager.service';
import {HttpMethodsTypeEnum, QMSApi} from "@constants/*";
import { Http, RequestOptionsArgs, Headers } from '@angular/http';
import {throwError as observableThrowError, Observable, Subject} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import { environment } from '@env/environment';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Injectable({
  providedIn: 'root'
})
export class AttachmentService {

  constructor(
    private http: Http,
    private _apiManager:APIManager
  ) { }

  url = environment.apiUrl + '/attachments/';


  addAttachment(params){
    
    return this._apiManager.httpHelperMethod(
        HttpMethodsTypeEnum.POST, QMSApi.ATTACHMENT_ADD, params,
        this._apiManager.Authorised_HttpOptions, false, true);
  };

  updateAttachment(params){
    return this._apiManager.httpHelperMethod(
        HttpMethodsTypeEnum.POST, QMSApi.ATTACHMENT_UPDATE, params,
        this._apiManager.Authorised_HttpOptions, false, true);
  };

  deleteAttachment(params): Observable<any>{
    return this._apiManager.httpHelperMethod(
        HttpMethodsTypeEnum.POST, QMSApi.ATTACHMENT_DELETE_PATH, params,
        this._apiManager.Authorised_HttpOptions, false, true);
  };

  search(params): Observable<any>{
    return this._apiManager.httpHelperMethod(
        HttpMethodsTypeEnum.POST, QMSApi.SEARCH, params,
        this._apiManager.Authorised_HttpOptions, false, true);
  };

  getByUrl(params): Observable<any>{
    return this._apiManager.httpHelperMethod(
        HttpMethodsTypeEnum.POST, QMSApi.ATTACHMENT_GET_URL,params,
        this._apiManager.Authorised_HttpOptions, false, true);
  };

  createFile(file,fileName) {

    let options:RequestOptionsArgs = {
      headers: new Headers({
        Authorization : 'Bearer '+Cookie.get('token'),
        env : environment.env
      })
    }
    let body = {
      file : file.split(';base64,').pop(),
      fileName : fileName
    }

    return this.http.post(this.url+'file/save',body,options).pipe(
      map((res: any) => {
        console.log("returned");
        console.log(res.json());
        return res.json();
      }),
      catchError((error: any) => {
          return observableThrowError(error.json ? error.json().error : error || 'server error');
      }),
    )
  }

  deleteFile(filePath){
    let options:RequestOptionsArgs = {
      headers: new Headers({
        Authorization : 'Bearer '+Cookie.get('token'),
        env : environment.env
      })
    }
    let body = {
      filePath : filePath
    }

    return this.http.post(this.url+'file/delete',body,options).pipe(
      map((res: any) => {
        return res.json();
      }),
      catchError((error: any) => {
          return observableThrowError(error.json ? error.json().error : error || 'server error');
      }),
    )
  }

}
