import { Component, OnInit } from '@angular/core';
import { AttachmentService } from 'app/feature-module/services/attachments/attachment.service';
import { MatTreeFlatDataSource, MatTreeFlattener, Sort } from '@angular/material';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Entity } from 'app/models/Entity';
import { EntititesService } from 'app/services/entities/entitites-service.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { SelfAssessmentService, AxesService } from 'app/feature-module/services';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '@env/environment';
import { UserService } from 'app/services/user/user.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';

interface Entit {
  entity: Entity;
  children?: Entit[];
}

var TREE_DATA: Entit[] = [];
/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  level: number;
}


@Component({
  selector: 'app-best-practice',
  templateUrl: './best-practice.component.html',
  styleUrls: ['./best-practice.component.scss']
})
export class BestPracticeComponent implements OnInit {
  

  files : Array<any> = [];
  panelOpenState:boolean = false;
  fileTypes : Array<string> = [];
  filterFileTypes : Array<string> = [];
  startDate : Date;
  endDate : Date;
  axes : Array<any>= [];
  elements : Array<any> = [];
  filterAxes : Array<any>= [];
  filterElements : Array<any> = [];
  filterEntities : Array<any> = [];
  filterFiles : Array<any> = [];

  isLoading : boolean = false;

  typeFilteredFiles = [];
  dateFilteredFiles = [];
  axeFilteredFiles = [];
  elementFilteredFiles = [];
  entityFilteredFiles = [];

  selectedEntities : Array<any> = [];

  Assessments: Array<any> = [];
  Entities : Array<any> = [];

  filteredElementsByAxe: Array<any> = [];
  allAxesNames: Array<any> = [];

  isDeleteAttachment:boolean = false;

  attachmentToDelete : any;


  constructor(
    private _attachmentService : AttachmentService,
    private _entitiesService : EntititesService,
    private _axesService : AxesService,
    public _userService: UserService,
    private _assessmentService : SelfAssessmentService,public translate: TranslateService) {
      if(Cookie.get('language')!==null)
      translate.setDefaultLang(Cookie.get('language')); 
      
    }

  ngOnInit() {
    this._userService.getCurrentUser().subscribe(data=>{
      this._userService.currentUser = data;
    })
    this._assessmentService.getAllSelfAssessmentShort().subscribe(response=>{
      this.Assessments = response['All assesments'];
      this._entitiesService.getAllEntities().subscribe(data=>{
        this.initData();
        this._attachmentService.search({query: ""}).subscribe(response=>{
          this.filterFiles = [];
          this.files = response.data;
          this.files.forEach(file=>{
            this.Assessments.forEach(ass=>{
              if(file.AssessmentCode !== 'Assessment Deleted' && ass.Code === file.AssessmentCode){
                this._entitiesService.entities.forEach(entity=>{
                  if(entity._id === ass.Entity){
                    file.Entity = entity;
                  }
                })
              }
            })
            if ( !this.fileTypes.includes('.'+file.DocumentType.toLowerCase())){
              this.fileTypes.push('.'+file.DocumentType.toLowerCase());
            }
            if ( !this.axes.includes(file.Axe.Description)){
              this.axes.push(file.Axe.Description);
            }
            if ( !this.elements.includes(file.Element)){
              this.elements.push(file.Element);
            }
          })
          this.filteredElementsByAxe = this.elements;
          this.filterFiles = this.files;
          this.axeFilteredFiles = this.files;
          this.elementFilteredFiles = this.files;
          this.typeFilteredFiles = this.files;
          this.entityFilteredFiles = this. files;
          this.dateFilteredFiles = this.files;
          //console.log(this.files)
        })
      });
    })
    
  }

  search(event){
    this.axes = [];
    this.elements = [];
    this.fileTypes = [];
    this._attachmentService.search({query: event.target.value}).subscribe(response=>{
      this.files = response.data;
      this.files.forEach(file=>{
        this.Assessments.forEach(ass=>{
          if(file.AssessmentCode !== 'Assessment Deleted' && ass.Code === file.AssessmentCode){
            this._entitiesService.entities.forEach(entity=>{
              if(entity._id === ass.Entity){
                file.Entity = entity;
              }
            })
          }
        })
        if ( !this.fileTypes.includes('.'+file.DocumentType.toLowerCase())){
          this.fileTypes.push('.'+file.DocumentType.toLowerCase());
        }
        if ( !this.axes.includes(file.Axe.Description)){
          this.axes.push(file.Axe.Description);
        }
        if ( !this.elements.includes(file.Element)){
          this.elements.push(file.Element);
        }
      })
      
      this.filteredElementsByAxe = this.elements;
      this.filterFiles = this.files;
      this.axeFilteredFiles = this.files;
      this.elementFilteredFiles = this.files;
      this.typeFilteredFiles = this.files;
      this.entityFilteredFiles = this. files;
      this.dateFilteredFiles = this.files;
    })
  }

  typeChange(i){

    if(this.filterFileTypes.includes(this.fileTypes[i])){
      this.filterFileTypes.splice(this.filterFileTypes.indexOf(this.fileTypes[i]),1)
    }else{
      this.filterFileTypes.push(this.fileTypes[i]);
    }
    this.filter('type');
  }

  dateChange(){
    
    this.filter('date')

  }

  axeChange(i){
    if(this.filterAxes.includes(this.axes[i])){
      this.filterAxes.splice(this.filterAxes.indexOf(this.axes[i]),1)
    }else{
      this.filterAxes.push(this.axes[i]);
    }
    this.filter('axe');
  }

  elementChange(elmnt){
    if(this.filterElements.includes(elmnt)){
      this.filterElements.splice(this.filterElements.indexOf(elmnt),1)
    }else{
      this.filterElements.push(elmnt);
    }
    this.filter('element');
  }

  entityChange(ent){
    ent.value.forEach(val=>{
      if(this.filterEntities.includes(val)){
      }else{
        this.filterEntities.push(val);
      }
    })
    if(ent.value.length < this.filterEntities.length){
      this.filterEntities.forEach((val,i)=>{
        if(!ent.value.includes(val)){
          this.filterEntities.splice(i,1)
        }
      })
    }
    this.filter('entity');
  }


  filter(opt){

    
    switch (opt) {
      case 'axe':
        this.axeFilteredFiles = this.files.filter((file,index,arr)=>{
          return this.filterAxes.includes(file.Axe.Description)
        })
        if(this.filterAxes.length == 0) this.axeFilteredFiles = this.files;
        break;
      case 'element':
        this.elementFilteredFiles = this.files.filter((file,index,arr)=>{
          return this.filterElements.includes(file.Element)
        })
        if(this.filterElements.length == 0) this.elementFilteredFiles = this.files;
        break;
      case 'type':
        this.typeFilteredFiles = this.files.filter((file,index,arr)=>{
          return this.filterFileTypes.includes('.'+file.DocumentType.toLowerCase())
        })
        if(this.filterFileTypes.length == 0) this.typeFilteredFiles = this.files;
        break;
      case 'date':
        if(!!this.startDate && !!this.endDate){
          this.dateFilteredFiles = this.files.filter((file,index,arr)=>{
            return ( this.toTimestamp(file.Date) >= this.toTimestamp(this.startDate)
            &&  this.toTimestamp(file.Date) <= this.toTimestamp(this.endDate));
          })
        }else if(!!this.startDate){
          this.dateFilteredFiles = this.files.filter((file,index,arr)=>{
            return ( this.toTimestamp(file.Date) >= this.toTimestamp(this.startDate));
          })
        }else if(!!this.endDate){
          this.dateFilteredFiles = this.files.filter((file,index,arr)=>{
            return ( this.toTimestamp(file.Date) <= this.toTimestamp(this.endDate));
          })
        }else{
          this.dateFilteredFiles = this.files;
        }
        break;
      case 'entity':
        this.entityFilteredFiles = this.files.filter((file,index,arr)=>{
          if(!!file.Entity){
            return this.filterEntities.includes(file.Entity._id)
          }
          else return false;
        })
        if(this.filterEntities.length == 0) this.entityFilteredFiles = this.files;
        break;

      default:
        break;
    }
    this.filterFiles = [];
    this.files.forEach(file=>{
      if(this.axeFilteredFiles.includes(file) && this.elementFilteredFiles.includes(file) && this.typeFilteredFiles.includes(file)
      && this.dateFilteredFiles.includes(file) && this.entityFilteredFiles.includes(file)){
        this.filterFiles.push(file);
      }
    });
    if(opt === 'axe'){
      this.checkElements();
    }
  }

  apply(i: number){
    if(!this.filterFiles[i].Applicators.includes(this._userService.currentUser._id)){
      this.filterFiles[i].Applicators.push(this._userService.currentUser._id);
      this._attachmentService.updateAttachment(this.filterFiles[i]).subscribe(data=>{
        this.search({target:{value:''}});
      });
    }else{
      this.filterFiles[i].Applicators.splice(this.filterFiles[i].Applicators.indexOf(this._userService.currentUser._id),1);
      this._attachmentService.updateAttachment(this.filterFiles[i]).subscribe(data=>{
        this.search({target:{value:''}});
      });
    }
  }

  like(i: number){
    if(!this._userService.currentUser.LikedAttachments.includes(this.filterFiles[i]._id)){
      this.filterFiles[i].Likes++;
      this._attachmentService.updateAttachment(this.filterFiles[i]).subscribe(data=>{
        this._userService.currentUser.LikedAttachments.push(this.filterFiles[i]._id);
        this._userService.updateUser(this._userService.currentUser).subscribe(data=>{
        })
      });
    }else{
      this.filterFiles[i].Likes--;
      this._attachmentService.updateAttachment(this.filterFiles[i]).subscribe(data=>{
        this._userService.currentUser.LikedAttachments.splice(this._userService.currentUser.LikedAttachments.indexOf(this.filterFiles[i]._id),1);
        this._userService.updateUser(this._userService.currentUser).subscribe(data=>{
        })
      });
    }
  }

  deleteFile(i: number) {
    console.log(this.filterFiles)
    this.attachmentToDelete = this.filterFiles[i];
    this.isDeleteAttachment = true;
    /* this._attachmentService.deleteAttachment(this.attachmentToDelete).subscribe(data=>{
      this.search({target:{value:''}});
      this.isLoading = false;
    }) */
  }

  confirmDelete(){
    this._attachmentService.deleteAttachment( this.attachmentToDelete).subscribe(data=>{
      this.search({target:{value:''}});
      this.isDeleteAttachment = false;
    })
  }

  downloadFile(i: number) {

    this.isLoading = true;
    //console.log(this.filterFiles)
    let file = this.filterFiles[i]
    let filePath;
    //let fileRef;
    filePath = file.Path;

    let url = environment.apiUrl+'/attachments/get_file';
    //fileRef = this._storage.ref(filePath);
    //fileRef.getDownloadURL().subscribe(function (url) {
     // console.log(url)
        // `url` is the download URL for 'images/stars.jpg'

        var data = '{"name":"'+filePath+'"}';

        // This can be downloaded directly:
        var xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);
        xhr.responseType = "blob";
        xhr.setRequestHeader('env',environment.env);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.setRequestHeader('Authorization', 'Bearer '+Cookie.get('token'));
        xhr.onload = () => {
            var urlCreator = window.URL;
            var imageUrl = urlCreator.createObjectURL(xhr.response);
            var tag = document.createElement('a');
            tag.href = imageUrl;
            tag.download = file.DocumentName;
            document.body.appendChild(tag);
            tag.click();
            document.body.removeChild(tag);
            this.isLoading = false;
        }
        xhr.send(data);
    //})

    this.filterFiles[i].Downloads++;
    this._attachmentService.updateAttachment(this.filterFiles[i]).subscribe(data=>{
      console.log(data);
    })
}
sortData(sort: Sort) {
  const data = this.filterFiles.slice();
  if (!sort.active || sort.direction === '') {
    return;
  }

  this.filterFiles = data.sort((a, b) => {
    const isAsc = sort.direction === 'asc';
    switch (sort.active) {
      case 'alpha': return compare(a.DocumentName, b.DocumentName, isAsc);
      case 'date': return compare(this.changeDateFormat(a.Date), this.changeDateFormat(b.Date), isAsc);
      default: return 0;
    }
  });

}

public changeDateFormat(DATE){
  let date = new Date(DATE);
      let years = date.getFullYear();

      var months,hours,minutes,seconds,days;

      if(date.getMonth()<9){
        months = '0'+ (date.getMonth()+1);
      } else{
        months = (date.getMonth()+1);
      }
      if(date.getDate()<10){
        days = '0'+ (date.getDate());
      } else{
         days = (date.getDate());
      }
      if(date.getMinutes()<10){
         minutes = '0'+ (date.getMinutes());
      } else{
         minutes = (date.getMinutes());
      }
      if(date.getSeconds()<10){
         seconds = '0'+ (date.getSeconds());
      } else{
         seconds = (date.getSeconds());
      }
      if(date.getHours()<10){
         hours = '0'+ (date.getHours());
      } else{
         hours = (date.getHours());
      }

      let stringDate = years +'-'+months+"-"+days+' '+hours+':'+minutes+':'+seconds;

      return stringDate;
}

  private transformer = (node: Entit, level: number) => {
    return {
        expandable: !!node.children && node.children.length > 0,
        name: ''+node.entity.Code+' | '+node.entity.Description,
        entity: node.entity,
        level: level
    };
}

treeControl = new FlatTreeControl<ExampleFlatNode>(
    node => node.level, node => node.expandable);

treeFlattener = new MatTreeFlattener(
    this.transformer, node => node.level, node => node.expandable, node => node.children);

treeSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

initData(){
  this.treeSource.data = [];
  TREE_DATA = [];
  this._entitiesService.entities.forEach(entity=>{
      if(entity.Type === "Site"){
          TREE_DATA.push({entity: entity,children:[]});
          this._entitiesService.entities.forEach(sub=>{
              for(var i =0 ; i<TREE_DATA[TREE_DATA.length-1].entity.SubEntities.length; i++ ){
                  if(sub._id === TREE_DATA[TREE_DATA.length-1].entity.SubEntities[i]){
                      TREE_DATA[TREE_DATA.length-1].children.push({entity:sub,children:[]});
                      let chilLen = TREE_DATA[TREE_DATA.length-1].children.length;
                      this._entitiesService.entities.forEach(sub1=>{
                          let len = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities.length;                                
                          for(var j = 0 ; j<len; j++ ){
                              if(sub1._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities[j]){
                                  TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.push({entity:sub1,children:[]});
                                  let chilLen1 = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.length;
                                  this._entitiesService.entities.forEach(sub2=>{
                                      let len2 = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].entity.SubEntities.length;                                
                                      for(var k = 0 ; k<len2; k++ ){
                                          if(sub2._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].entity.SubEntities[k]){
                                              TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].children.push({entity:sub2,children:[]});
                                          }
                                      }
                                  })
                              } 
                          }
                      })
                  }    
              }
          })
      }
  });
  this.treeSource.data = [];
  this.treeSource.data = TREE_DATA;
  }

  checkElements(){
    if(this.filterAxes.length !== 0 && this.filterAxes.length !== this.axes.length){
      let elements = [];
      this.files.forEach(file=>{
        if(!elements.includes(file.Element) && this.filterAxes.includes(file.Axe.Description)){
          elements.push(file.Element);
        }
      })
      this.filteredElementsByAxe = elements;
    }else{
      this.filteredElementsByAxe = this.elements;
    }
  }

  toTimestamp(strDate){
    var datum = Date.parse(strDate);
    return datum/1000;
  }

}

function compare(a: number | string, b: number | string, isAsc: boolean) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
