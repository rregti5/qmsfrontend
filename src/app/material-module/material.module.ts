import {NgModule, OnDestroy, OnInit} from '@angular/core';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatPaginatorModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatTableModule,
    MatTabsModule,
    MatTreeModule,
    MatDatepickerModule,
    MatSortModule,
    MatBadgeModule,
    MatCardModule,
    MatChipsModule,
    MatListModule,
    MatDialogModule,
    MatPaginatorIntl,
    MatSnackBarModule
    
    
} from "@angular/material";
import { Cookie } from 'ng2-cookies/ng2-cookies';
export class MyCustomPaginatorIntl  extends MatPaginatorIntl   {
    itemPage:string="Item per page";
    of:string="of"
    itemperpage():string{
        if(Cookie.get('language')==='fr')
      return this.itemPage="Elements par page";
       
       return this.itemPage
    }
   itemsPerPageLabel=this.itemperpage()
   
   
    }
@NgModule({
    imports: [
        MatRippleModule,
        MatPaginatorModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        MatTableModule,
        MatExpansionModule,
        MatTabsModule,
        MatTreeModule,
        MatIconModule,
        MatMenuModule,
        MatRadioModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatSortModule,
        MatBadgeModule,
        MatCardModule,
        MatChipsModule,
        MatListModule,
        MatDialogModule,
        MatSnackBarModule
    ],
    declarations: [],
    exports: [
        MatRippleModule,
        MatPaginatorModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        MatTableModule,
        MatExpansionModule,
        MatTabsModule,
        MatTreeModule,
        MatIconModule,
        MatMenuModule,
        MatRadioModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatSortModule,
        MatBadgeModule,
        MatCardModule,
        MatChipsModule,
        MatListModule,
        MatDialogModule,
        MatSnackBarModule
    ],
    providers: [ {provide: MatPaginatorIntl, useClass: MyCustomPaginatorIntl}]
})
export class MaterialModule  {
  
  
}

  
