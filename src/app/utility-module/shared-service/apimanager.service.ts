/**
 *
 * Author: smartSense Consulting Solutions Pvt. Ltd.
 * Website: https://smartsensesolutions.com
 * Date: Sep 24 2018 11:30 AM
 * Copyright @ 2018 smartSense Consulting Solutions Pvt. Ltd.
 *
 */
import {Injectable} from '@angular/core';
import {HttpHelperService} from '@services/http-helper.service';
import {SharedService} from '@services/shared.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {HttpMethodsTypeEnum} from '@constants/base-constants';
import {Observable} from 'rxjs';
import {AppLogger} from '../shared-functions/common-functions';
import { environment } from '@env/environment';

@Injectable()
export class APIManager extends HttpHelperService {

  constructor(_sharedService: SharedService,
              http: HttpClient) {
    super(_sharedService, http);
  }

  // return authorisation header
  get Authorised_HttpOptions_JSON() {
    const authToken = this._sharedService.getToken();
    const httpOptions = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${authToken}`,
      env : environment.env
    });
    return {headers: httpOptions};
  }

  // return authorisation header
  get Authorised_HttpOptions() {
    const authToken = this._sharedService.getToken();
    const httpOptions = new HttpHeaders({
      Authorization: `Bearer ${authToken}`,
      env: environment.env,
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest'
    });
    return {headers: httpOptions};
  }

  // return authorisation header with only content-type
  get Content_Type_HttpOptions() {
    const httpOptions = new HttpHeaders({
      'Content-Type': 'application/json',
      env: environment.env
    });
    return {headers: httpOptions};
  }

  // return authorisation header with content-type as x-www-form-urlencoded
  get Form_URL_Encoded_HttpOptions() {
    const authToken = this._sharedService.getToken();
    const httpOptions = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: `${authToken}`,
      env: environment.env
    });
    return {headers: httpOptions};
  }

  // return authorisation header with blob
  get Blob_HttpOptions(): any {
    const authToken = this._sharedService.getToken();
    return {
      headers: new HttpHeaders({
        Authorization: `Bearer ${authToken}`,
        env: environment.env
      }),
      responseType: 'blob'
    };
  }

  get Blob_HttpOptions_2(): any {
    const authToken = this._sharedService.getToken();
    return {
      headers: new HttpHeaders({}),
      responseType: 'blob',
      env: environment.env
    };
  }

  /**
   * method name : overridable httpHelperMethod
   * purpose : handle loader, and call overload in parent class for getting Observable of response
   * created : Sep 24 2018 11:30 AM
   * Revision :
   */
  httpHelperMethod(methodType: HttpMethodsTypeEnum, url: string, params = {},
                   httpOptions = this.Authorised_HttpOptions_JSON,
                   showToast, showLoader, searchParams = {}, filesObj?: any[]): Observable<any> {
    if (showLoader) {
      AppLogger(`<=====starting of api call=====> ${url}`);
      this._sharedService.setLoader(true);
    }
    if (methodType === HttpMethodsTypeEnum.POST_MULTIPART || methodType === HttpMethodsTypeEnum.PUT_MULTIPART) {
      params = this.createFormDataObject(params, filesObj);
    }
    return super.httpHelperMethod(methodType, url, params, httpOptions, showToast, showLoader, searchParams, filesObj);
  }

  getImage(imageUrl: string): Observable<Blob> {
    return this.http.get(imageUrl, {responseType: 'blob'});
  }

  // /**
  //  * return formData object from filesObject

  //  */
  createFormDataObject = (params, filesObj) => {
    const formData = new FormData();
    for (const obj of filesObj) {
      const imgFilesObj: File[] = obj.files;
      for (let i = 0; i < imgFilesObj.length; i++) {
        formData.append(obj.reqKey, imgFilesObj[i], imgFilesObj[i].name);
      }
    }
    if (params && (Object.keys(params).length)) {
      for (const docKey in params) {
        if (typeof params[docKey] === 'object') {
          formData.append(docKey, JSON.stringify(params[docKey]));
        } else {
          formData.append(docKey, params[docKey]);
        }
      }
    }
    return formData;
  };
}
