///<reference path="components/self-assessment/self-assessment-view/self-assessment-view.component.ts"/>
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AppRoutesConstant} from "../_constants/app-routes";
import {
    AssessmentPlanDetailsComponent,
    AssessmentPlanListComponent,
    AxesDetailsComponent,
    AxesListComponent,
    AxesUpdateComponent,
    DashboardComponent,
    EntitiesDetailsComponent,
    EntitiesListComponent,
    SelfAssessmentListComponent,
    SelfAssessmentViewComponent,
    UserDetailsComponent,
    UserListComponent,
    ActionsComponent
} from "./components";
import {AuthGuard} from '../auth/auth.guard';
import { GuideQmsComponent } from './components/guide-qms/guide-qms.component';
import { BestPracticeComponent } from './components/best-practice/best-practice.component';
import {HomeComponent} from "../home/home.component";
import {DefinitionsComponent} from "../definitions/definitions.component";
import {GloassaireComponent} from "../gloassaire/gloassaire.component";

const routes: Routes = [
    {
        path: AppRoutesConstant.USER_LIST,
        component: UserListComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.USER_DETAILS,
        component: UserDetailsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.AXES_LIST,
        component: AxesListComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.AXES_DETAILS,
        component: AxesDetailsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.AXES_UPDATE + '/:id',
        component: AxesUpdateComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.ENTITY_LIST,
        component: EntitiesListComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.ENTITY_DETAILS,
        component: EntitiesDetailsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.ASSESSMENT_PLAN_LIST,
        component: AssessmentPlanListComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.ASSESSMENT_PLAN_ADD,
        component: AssessmentPlanDetailsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.ASSESSMENT_PLAN_EDIT + '/:id',
        component: AssessmentPlanDetailsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.SELF_ASSESSMENT_VIEW,
        component: SelfAssessmentViewComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.DASHBOARD,
        component: DashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.SELF_ASSESSMENT_LIST,
        component: SelfAssessmentListComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.GUIDE_QMS,
        component: GuideQmsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.BEST_PRACTICE,
        component: BestPracticeComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.HOME,
        component: HomeComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.DEFINITIONS,
        component: DefinitionsComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.GLOSSAIRE,
        component: GloassaireComponent,
        canActivate: [AuthGuard]
    },
    {
        path: AppRoutesConstant.ACTIONS,
        component: ActionsComponent,
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FeatureRoutingModule {
}
