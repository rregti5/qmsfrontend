import {Component, OnDestroy, OnInit} from '@angular/core';
import {SharedService} from "@services/shared.service";

@Component({
    selector: 'app-progress-hud',
    templateUrl: './progress-hud.component.html',
    styleUrls: ['./progress-hud.component.scss']
})

export class ProgressHudComponent implements OnInit, OnDestroy {

    isLoading = false;
    $loaderSubscriber: any;

    constructor(private _sharedService: SharedService) {
    }

    ngOnInit() {
        this.$loaderSubscriber = this._sharedService.getLoader().subscribe((isLoading) => {
            this.isLoading = isLoading;
        });
    }

    ngOnDestroy() {
        if (this.$loaderSubscriber) {
            this.$loaderSubscriber.unsubscribe();
        }
    }
}
