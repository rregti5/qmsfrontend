export interface AxesModel {
    "Elements": ElementModel[],
    "_id": string;
    "id": string;
    "Description": string;
    "Visible": boolean;
    "createdDate": string;
    "__v": number;
}

export interface ElementModel {
    "Description": string;
    "Comment": string;
    "AttachementControl": number;
    "Attachment": any[];
    "Scores": ScoreModel[]
}

export interface ScoreModel {
    "Description": Array<string>;
    "Note": number;
    "Selected": boolean;
}

export interface AxesModelNew {
    "Elements": ElementModelNew[],
    "_id": string;
    "id": string;
    "Description": string;
    "Visible": boolean;
    "createdDate": string;
    "__v": number;
}

export interface ElementModelNew {
    "Description": string;
    "Comment": string;
    "AttachementControl": number;
    "Attachment": any[];
    "Scores": ScoreModelNew[];
}

export interface ScoreModelNew {
    "Description":Array<string>;
    "ScoreData"?:ScoreData[];
    "Note": number;
    "Selected": boolean;
}

export interface ScoreData { 
    "Description": string;
    "Color":string;
}