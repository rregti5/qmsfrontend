import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener, MatTableDataSource, MatSort } from '@angular/material';
import { FlatTreeControl } from '@angular/cdk/tree';
import { PAGE_SIZE_OPTIONS } from '@constants/*';

import { ActionsService } from 'app/services/actions/actions.service';
import { TranslateService } from '@ngx-translate/core';
import { MyCustomPaginatorIntl } from 'app/material-module/material.module';
import { Cookie } from 'ng2-cookies/ng2-cookies';
import { UserService } from 'app/services/user/user.service';
import { DashboardService, EntitiesService, SelfAssessmentService } from 'app/feature-module/services';
import { EntititesService } from 'app/services/entities/entitites-service.service';
import { Entity } from 'app/models/Entity';
import { EntityModel } from 'app/feature-module/models';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Label, MultiDataSet } from 'ng2-charts';
import { ChartType, ChartOptions } from 'chart.js';



interface Action {
    Axe: string;
    Element: string;
    Description: string;
    Who: string;
    DueDate: string;
    Status: boolean;
    Comment:boolean;
}

const ELEMENT_DATA: Action[] = [];
const ELEMENT_DATA_ASS : Array<any> = [];

interface Entit {
    entity: Entity;
    children?: Entit[];
}

var TREE_DATA: Entit[] = [];

/** Flat node with expandable and level information */
interface ExampleFlatNode {
    expandable: boolean;
    name: string;
    level: number;
}


@Component({
    selector: 'app-actions',
    templateUrl: './actions.component.html',
    styleUrls: ['./actions.component.scss']
})

export class ActionsComponent implements OnInit {

    user : any;

    selectedAssessment: any = 0;
    currentAssessment : any = {};
    
    //table
    displayedColumns : string[] = ['axe', 'element', 'action', 'who', 'dueDate','status','comment'];
    displayedColumnsAssessments : string[] = ['code', 'date', 'status'];
    displayedColumnsComments : string[] = ['axe', 'element','comment'];
    dataSource = new MatTableDataSource(ELEMENT_DATA);
    dataSourceAssessments = new MatTableDataSource(ELEMENT_DATA_ASS);
    dataSourceComments = new MatTableDataSource(ELEMENT_DATA_ASS);
    
    // Pagination Variables
    isLoadingResults = true;
    pageSizeOptions = PAGE_SIZE_OPTIONS;
    pageSize = PAGE_SIZE_OPTIONS[0];
    pageSizeAssessments = PAGE_SIZE_OPTIONS[0];
    pageSizeComments = PAGE_SIZE_OPTIONS[0];
    pageIndex = 0;
    resultsLength = 0;

    resultsLengthAssessments = 0;
    pageIndexAssessments = 0;

    resultsLengthComments = 0;
    pageIndexComments = 0;


    assessments : Array<any> = [];

    comments : Array<any> = [];

    actions : Array<any> = [];
    Entities: Array<any> = [];

    selectedEntity : any = 'all';
    
    @ViewChild(MatSort) sort: MatSort;

    public doughnutChartLabels: Label[] = ['Finished','In Progress'];
    public doughnutChartActions: MultiDataSet = [
        [ 0, 0 ]
    ];

    public doughnutChartType: ChartType = 'doughnut';
    public doughnutChartOptions : ChartOptions = {
        responsive: true,
        cutoutPercentage: 70,
    }
    public doughnutChartActionsColours = [
        {
            backgroundColor : ['#23ab27','#f18407']
        }
    ]

    constructor(
        private _userService : UserService,
        public _actionsService : ActionsService,
        private _dashboardService: DashboardService, 
        private _entitieService: EntitiesService,
        private _assessmentService: SelfAssessmentService,
        private _entitiesService: EntititesService,
        public translate: TranslateService
    ) {
        if(Cookie.get('language')!==null) translate.setDefaultLang(Cookie.get('language'));

        if( Cookie.get('language') === 'fr')     this.doughnutChartLabels = ['Terminé','En cours'];

        this._userService.getAllUsers().subscribe(data=>{
            this._userService.users = data;
        })
    }

    ngOnInit() {
        this._userService.getCurrentUser().subscribe((user) => {
            this.user = user;

            this._entitiesService.getAllEntities().subscribe((data)=>{
                this._entitiesService.userEntities = [];
                this.Entities = [];
                data.entities.forEach(element => {
                    let ent = new Entity(element._id, element.Code, element.Description, element.Type, element.SubEntities, element.AssessmentPlan, element.SelfAssessment, element.Score, element.CalculatedScore, element.ScoreGoal);
                    if(this.user.Role > 2 || ( this.user.Role <= 2 && this.user.Entity.includes(element._id) ) ) {
                        this._entitiesService.userEntities.push(ent);
                        this.Entities.push(ent._id);
                    }
                });
                this._entitiesService.entities = this._entitiesService.userEntities;
                    //console.log(this._entitiesService.entities);
                //console.log(data); 

                
                this.getAssessments();
                
                this.initData();
            });
        })
    }

    private transformer = (node: Entit, level: number) => {
        return {
            expandable: !!node.children && node.children.length > 0,
            name: ''+node.entity.Code+' | '+node.entity.Description,
            entity: node.entity,
            level: level
        };
    }

    treeControl = new FlatTreeControl<ExampleFlatNode>(
        node => node.level, node => node.expandable);

    treeFlattener = new MatTreeFlattener(
        this.transformer, node => node.level, node => node.expandable, node => node.children);

    treeSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

    /* getEntities() {
        return this._entitieService.getAllEntities()
    }

    getEntitiesByIds(params) {
        return this._entitieService.getEntitiesById(params)
    }

    handleEntitiesResponse = (response) => {
        this.Entities = response.entities;
    } */

    initData(){
        this.treeSource.data = [];
        TREE_DATA = [];
        let flag = false;
        this._entitiesService.entities.forEach(entity=>{
            if(entity.Type === "Site"){
                flag = true;
                TREE_DATA.push({entity: entity,children:[]});
                this._entitiesService.entities.forEach(sub=>{
                    for(var i =0 ; i<TREE_DATA[TREE_DATA.length-1].entity.SubEntities.length; i++ ){
                        if(sub._id === TREE_DATA[TREE_DATA.length-1].entity.SubEntities[i]){
                            TREE_DATA[TREE_DATA.length-1].children.push({entity:sub,children:[]});
                            let chilLen = TREE_DATA[TREE_DATA.length-1].children.length;
                            this._entitiesService.entities.forEach(sub1=>{
                                let len = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities.length;                                
                                for(var j = 0 ; j<len; j++ ){
                                    if(sub1._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities[j]){
                                        TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.push({entity:sub1,children:[]});
                                        let chilLen1 = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.length;
                                        this._entitiesService.entities.forEach(sub2=>{
                                            let len2 = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].entity.SubEntities.length;                                
                                            for(var k = 0 ; k<len2; k++ ){
                                                if(sub2._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].entity.SubEntities[k]){
                                                    TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].children.push({entity:sub2,children:[]});
                                                }
                                            }
                                        })
                                    } 
                                }
                            })
                        }    
                    }
                })
            }
        })
        if(!flag){
            this._entitiesService.entities.forEach(entity=>{
                if(entity.Type === "Usine"){
                    flag = true;
                    TREE_DATA.push({entity: entity,children:[]});
                    this._entitiesService.entities.forEach(sub=>{
                        for(var i =0 ; i<TREE_DATA[TREE_DATA.length-1].entity.SubEntities.length; i++ ){
                            if(sub._id === TREE_DATA[TREE_DATA.length-1].entity.SubEntities[i]){
                                TREE_DATA[TREE_DATA.length-1].children.push({entity:sub,children:[]});
                                let chilLen = TREE_DATA[TREE_DATA.length-1].children.length;
                                this._entitiesService.entities.forEach(sub1=>{
                                    let len = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities.length;                                
                                    for(var j = 0 ; j<len; j++ ){
                                        if(sub1._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities[j]){
                                            TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.push({entity:sub1,children:[]});
                                        } 
                                    }
                                })
                            }    
                        }
                    })
                }
            });
        }
        if(!flag){
            this._entitiesService.entities.forEach(entity=>{
                if(entity.Type === "Ligne"){
                    flag = true;
                    TREE_DATA.push({entity: entity,children:[]});
                    this._entitiesService.entities.forEach(sub=>{
                        for(var i =0 ; i<TREE_DATA[TREE_DATA.length-1].entity.SubEntities.length; i++ ){
                            if(sub._id === TREE_DATA[TREE_DATA.length-1].entity.SubEntities[i]){
                                TREE_DATA[TREE_DATA.length-1].children.push({entity:sub,children:[]});
                            }    
                        }
                    })
                }
            });
        }
        if(!flag){
            this._entitiesService.entities.forEach(entity=>{
                if(entity.Type === "Atelier"){
                    flag = true;
                    TREE_DATA.push({entity: entity,children:[]});
                }
            });
        }
        this.treeSource.data = [];
        this.treeSource.data = TREE_DATA;
    }

    getAssessments(){
        if(this.selectedEntity === 'all'){
            this.Entities.sort((a: any,b:any)=>{
                if(a.Type === 'Site'){
                        return 1
                }else if(a.Type === 'Usine'){
                    if(b.Type === 'Site')  return -1;
                    else return 1
                } else if (a.Type === 'Atelier'){
                    if(b.Type === 'Site' || b.Type === 'Usine')  return -1;
                    else return 1
                }else return -1;
            })
            if ( this.Entities.length>20 ) {
                this.Entities = this.Entities.slice(0,19);
            }
            
            this._assessmentService.getSelfAssessmentsByEntities(this.Entities).subscribe(data=>{
                this.assessments = data.assessments;
                this.assessments.forEach(assessment=>{
                    assessment.CreatedOn = this.changeDateFormat(assessment.CreatedOn,true);
                })
                this.dataSourceAssessments.data = this.assessments;
                this.resultsLengthAssessments = this.assessments.length;

                this.assessments = this.assessments.sort((a,b)=>{
                    return compare(a.CreatedOn,b.CreatedOn,false);
                })

                this.dataSourceAssessments.data = this.assessments.filter((elem, index) => {
                    return index < this.pageSizeAssessments;
                })
                this.selectedAssessment = 0;
                this.getActions();
                /* this.dataSourceAssessments.data = this.assessments.filter((elem, index) => {
                    return index >= this.pageIndex * this.pageSize && index < ((this.pageIndex + 1) * this.pageSize);
                }) */
            })
        } else{
            this._assessmentService.getSelfAssessmentsByEntities([this.selectedEntity]).subscribe(data=>{
                this.assessments = data.assessments;
                this.assessments.forEach(assessment=>{
                    assessment.CreatedOn = this.changeDateFormat(assessment.CreatedOn,true);
                })
                this.dataSourceAssessments.data = this.assessments;
                this.resultsLengthAssessments = this.assessments.length;

                this.dataSourceAssessments.data = this.assessments.filter((elem, index) => {
                    return index < this.pageSizeAssessments;
                })
                this.selectedAssessment = 0;
                this.getActions();
                /* this.dataSourceAssessments.data = this.assessments.filter((elem, index) => {
                    return index >= this.pageIndex * this.pageSize && index < ((this.pageIndex + 1) * this.pageSize);
                }) */
            })
        }

    }

    getActions(){
        /* if(this.selectedEntity != 'all'){
            this._actionsService.getEntitiesActions([this.selectedEntity]).subscribe(response=>{
                let dat = [];
                this.actions = [];
                response.actions.forEach((resp,i)=>{
                    resp.DueDate = this.changeDateFormat(resp.DueDate)
                    if ( resp.dueDate == '1970-01-01 00:00:00') resp.dueDate = '';
                    this._userService.users.forEach(user=>{
                        if (user._id === resp.Who) resp.Who = user.Name + ' | ' + user.Email;
                    })
                    dat.push(resp);
                })
                this.actions = dat
                this.dataSource.data = dat;
                this.resultsLength = dat.length;
    
                this.dataSource.data = dat.filter((elem, index) => {
                    return index < this.pageSize
                })
                this.dataSource.data = dat.filter((elem, index) => {
                    return index >= this.pageIndex * this.pageSize && index < ((this.pageIndex + 1) * this.pageSize);
                })
                this.dataSource.sort = this.sort;
            });
        }else{
            this._actionsService.getEntitiesActions(this.Entities).subscribe(response=>{
                let dat = [];
                this.actions = [];
                response.actions.forEach((resp,i)=>{
                    resp.DueDate = this.changeDateFormat(resp.DueDate)
                    if ( resp.dueDate == '1970-01-01 00:00:00') resp.dueDate = '';
                    this._userService.users.forEach(user=>{
                        if (user._id === resp.Who) resp.Who = user.Name + ' | ' + user.Email;
                    })
                    dat.push(resp);
                })
                this.actions = dat
                this.dataSource.data = dat;
                this.resultsLength = dat.length;
    
                this.dataSource.data = dat.filter((elem, index) => {
                    return index < this.pageSize
                })
                this.dataSource.data = dat.filter((elem, index) => {
                    return index >= this.pageIndex * this.pageSize && index < ((this.pageIndex + 1) * this.pageSize);
                })
                this.dataSource.sort = this.sort;
            });
        } */
        if(this.assessments.length > 0){
            this._actionsService.getActionsByAssessment(this.assessments[this.selectedAssessment]._id).subscribe(response=>{
                let dat = [];
                this.actions = [];
                response.data.forEach((resp,i)=>{
                    resp.DueDate = this.changeDateFormat(resp.DueDate);
                    if ( (resp.DueDate) == '1970-01-01') {
                        resp.DueDate = '';
                    }
                    let userFlag = false;
                    if(resp.Who != 'Mail'){
                        this._userService.users.forEach(user=>{
                            if (user._id === resp.Who) {
                                resp.Who = user.Name + ' | ' + user.Email;
                                userFlag = true;
                            }
                        })
                        if (!userFlag) resp.Who ="Utilisateur Supprimé"
                    }
                    dat.push(resp);
                })
                this.actions = dat;
                let doneActions = this.actions.filter((elem,index)=>{
                    return elem.Status === "Finished";
                });
                this.doughnutChartActions = [
                    [ doneActions.length, this.actions.length - doneActions.length ]
                ];
                this.dataSource.data = dat;
                this.resultsLength = dat.length;
    
                this.dataSource.data = dat.filter((elem, index) => {
                    return index < this.pageSize
                })
                this.dataSource.data = dat.filter((elem, index) => {
                    return index >= this.pageIndex * this.pageSize && index < ((this.pageIndex + 1) * this.pageSize);
                })
                this.dataSource.sort = this.sort;
            })
            this.getComments();   
        }else{
            this.actions = [];
            this.comments = [];
            this.dataSource.data = [];
            this.dataSourceComments.data = [];
        }

    }

    getComments() {
        this._assessmentService.getSelfAssessment(this.assessments[this.selectedAssessment]._id).subscribe(data=>{
            this.currentAssessment = data;

            this.currentAssessment.Axes.forEach(axe=>{
                axe.Elements.forEach(element=>{
                    if(element.Comment && element.Comment != ''){
                        this.comments.push({
                            Axe : axe.Description,
                            Element : element.Description,
                            Comment : element.Comment
                        })
                    }
                })
            })
            this.dataSourceComments.data = this.comments;
            this.resultsLengthComments = this.comments.length;

            this.dataSourceComments.data = this.comments.filter((elem, index) => {
                return index < this.pageSizeComments
            })
            this.dataSourceComments.data = this.comments.filter((elem, index) => {
                return index >= this.pageIndexComments * this.pageSizeComments && index < ((this.pageIndexComments + 1) * this.pageSizeComments);
            })
        })
    }

    onSelectionChangeEntity = (event) => {
        this.selectedEntity = event.value;
        /* if(event.value === 'all'){
            let entities = [];
            this.Entities.forEach(entity=>{
                entities.push(entity._id)
            })
        } */
        this.getAssessments();
        //this.getActions();
    }


    changePage = (event) => {
        const {pageIndex, pageSize} = event;
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.dataSource.data = this.actions.filter((elem, index) => {
            return index >= this.pageIndex * this.pageSize && index < ((this.pageIndex + 1) * this.pageSize);
        })
        this.dataSource.sort = this.sort;
    }

    changeAssessmentsPage = (event) => {
        const {pageIndex, pageSize} = event;
        this.pageIndexAssessments = pageIndex;
        this.pageSizeAssessments = pageSize;
        this.dataSourceAssessments.data = this.assessments.filter((elem, index) => {
            return index >= this.pageIndexAssessments * this.pageSizeAssessments && index < ((this.pageIndexAssessments + 1) * this.pageSizeAssessments);
        })
    }

    changeCommentsPage(event){
        const {pageIndex, pageSize} = event;
        this.pageIndexComments = pageIndex;
        this.pageSizeComments = pageSize;
        this.dataSourceComments.data = this.comments.filter((elem, index) => {
            return index >= this.pageIndexComments * this.pageSizeComments && index < ((this.pageIndexComments + 1) * this.pageSizeComments);
        })
    }

    selectAssessment(i){
        this.selectedAssessment = i;
        this.getActions();
    }

    changeDateFormat(DATE,full?:boolean){
        let date = new Date(DATE);
        let years = date.getFullYear();

        var months,hours,minutes,seconds,days;

        if(date.getMonth()<9){
            months = '0'+ (date.getMonth()+1);
        } else{
            months = (date.getMonth()+1);
        }
        if(date.getDate()<10){
            days = '0'+ (date.getDate());
        } else{
            days = (date.getDate());
        }

        

        let stringDate = years +'-'+months+"-"+days;

        if(full){
            if(date.getHours()<10){
                hours = '0'+date.getHours();
            }else{
                hours = date.getHours();
            }
            if(date.getMinutes()<10){
                minutes = '0'+date.getMinutes();
            }else{
                minutes = date.getMinutes();
            }
            if(date.getSeconds()<10){
                seconds = '0'+date.getSeconds();
            }else{
                seconds = date.getSeconds();
            }
            stringDate += ' '+hours+':'+minutes+':'+seconds;
        }

        return stringDate;
    }
  
}

function compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}