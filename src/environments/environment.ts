// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    env: 'demo',
    apiUrl : 'http://localhost/backend', 
    //apiUrl: 'http://10.16.128.24/backend', 
    //apiUrl: 'http://vibline.ocpms.net:2086/backend',
    //apiUrl: 'https://qms.um6p.ma/backend',
    //apiUrl: 'https://qmsbackend.ocpms.net/backend', 
    //apiUrl: 'https://self01-backend.herokuapp.com',
    //token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI1YzhjZWUwZDc2ZmRiYTAwMTczMjdiZmQiLCJpYXQiOjE1NTI3NTgxMDd9.kpd-Zhpk6cW-xtmiAPqQfR-xiCXMJOxGVpu_uJSotqA'

    maintenance:true,

    firebase: {
        apiKey: "AIzaSyAyFkFrk6dfk3J2yRhG3mAOXXyQqtqiC48",
        authDomain: "qm-qms.firebaseapp.com",
        databaseURL: "https://qm-qms.firebaseio.com",
        projectId: "qm-qms",
        storageBucket: "qm-qms.appspot.com",
        messagingSenderId: "533503653759"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
