const express = require('express');
const tls = require('tls');
const app = express();
const path =require('path')
const secure = require('express-force-https');
const http = require('http');
const https = require('https');
const fs = require("fs");


app.use(express.static(__dirname + '/dist'));

var privateKey = fs.readFileSync(__dirname + '/ssl/client-key.pem', 'utf8');
var certificate = fs.readFileSync(__dirname + '/ssl/client-cert.pem', 'utf8');

var credentials = tls.createSecureContext({key: privateKey, cert: certificate});

//app.setSecure(credentials);

//app.listen(process.env.PORT || 8080)

app.get('/*',function(req,res){
  res.sendFile(path.join(__dirname+'/dist/index.html'))
});

var httpServer = http.createServer(app);
/*
var httpsServer = https.createServer({
  key: fs.readFileSync(__dirname + '/ssl/client-key.pem'),
  cert: fs.readFileSync(__dirname + '/ssl/client-cert.pem'),
  requestCert: false,
  rejectUnauthorized: false,
  passphrase: 'Avempace1!'
}, app);*/

httpServer.listen(process.env.PORT || 8000);
//httpsServer.listen(443);

console.log("server listening")
