import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs, Headers } from '@angular/http';
import { map, catchError } from 'rxjs/operators';

import {throwError as observableThrowError,  Observable ,  Subject } from 'rxjs';
import { User } from '../../models/user';
import { environment } from '@env/environment';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor( private http: Http) { }

  public isCollapseMenu:boolean = false;

  public token : string = null;
  public currentUser : User = new User(null,null,null,null,null,null,null,null);
  public users : Array<User> = [];
  public addType:string;
  public userToEdit: User;

  url = environment.apiUrl+"/users/";

  authenticate(userEmail:string,password:string){
    let object = {
      Email : userEmail,
      Password: password
    }
    console.log(this.url)
    let options:RequestOptionsArgs = {
      headers: new Headers({
        env : environment.env,
        'Access-Control-Allow-Origin':'*'
      })
    }
    return this.http.post(this.url+"Auth_user",object,options).pipe(
      map((res:any)=>{
        return res.json();
      }),
      catchError((err:any)=>{
        return observableThrowError(err.json? err.json().err: err || 'server error');;
      }),
    );
  }

  getAllUsers(){
    let options:RequestOptionsArgs={
      headers: new Headers({Authorization: 'Bearer '+Cookie.get('token'),
      env : environment.env
      })
    }
    return this.http.get(this.url,options).pipe(
      map((res:any)=>{
        return res.json();
      }),
      catchError((error:any) => {
        return observableThrowError(error.json? error.json().error: error || 'server error');
      }), 
    );
  }

  deleteUSer(user){
    let options:RequestOptionsArgs={
      headers: new Headers({
        Authorization: 'Bearer '+Cookie.get('token'),
        env : environment.env
      })
    }
    return this.http.delete(this.url+user._id,options).pipe(
      map((res:any)=>{
        return res.json();
      }),
      catchError((error:any) => {
        return observableThrowError(error.json? error.json().error: error || 'server error');
      }), 
    );
  }

  addUser(user){
    let options:RequestOptionsArgs={
      headers: new Headers({
        'Content-Type': 'Application/json',
        env : environment.env
      })
    }
    let body={
      Name: user.Name,
      Email: user.Email,
      Password: user.Password,
      Role: user.Role,
      Entity: user.Entity,
      Status: user.Status,
      createdDate: new Date()
    }
    return this.http.post(this.url+'Add_user',body,options).pipe(
      map((res:any)=>{
        return res.json();
      }),
      catchError((error:any) => {
        return observableThrowError(error.json? error.json().error: error || 'server error');
      }), 
    );
  }

  updateUser(user, password? : boolean){
    let options:RequestOptionsArgs={
      headers: new Headers({
        Authorization: 'Bearer '+Cookie.get('token'),
        env : environment.env,
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest'
      })
    }
    let body = {
      Entity: user.Entity,
      Status: user.Status,
      Name: user.Name,
      Password : '',
      Email: user.Email,
      Role: user.Role,
      LikedAttachments: user.LikedAttachments
    }
    if(password) body.Password = user.Password;
    return this.http.put(this.url+user._id,body,options).pipe(
      map((res:any)=>{
        return res.json();
      }),
      catchError((error:any) => {
        return observableThrowError(error.json? error.json().error: error || 'server error');
      }), 
    );
  }

  resetPassword(user){
    let options:RequestOptionsArgs={
      headers: new Headers({
        Authorization: 'Bearer '+Cookie.get('token'),
        env : environment.env
      })
    }
    let body = {
      id : user._id,
      Entity: user.Entity,
      Status: user.Status,
      Name: user.Name,
      Password : user.Password,
      Email: user.Email,
      Role: user.Role,
    }
    console.log(body)
    return this.http.post(this.url+'resetPassword',body,options).pipe(
      map((res:any)=>{
        return res.json();
      }),
      catchError((error:any) => {
        return observableThrowError(error.json? error.json().error: error || 'server error');
      }), 
    );
  }

  getCurrentUser(){

    let options:RequestOptionsArgs={
      headers: new Headers({Authorization: 'Bearer '+Cookie.get('token'),
      env : environment.env
    })
    }

    return this.http.get(this.url+'current',options).pipe(
      map((res:any)=>{
        return res.json();
      }),
      catchError((error:any) => {
        return observableThrowError(error.json? error.json().error: error || 'server error');
      }), 
    );

  }

  getSubUsers(){

    let options:RequestOptionsArgs={
      headers: new Headers({Authorization: 'Bearer '+Cookie.get('token'),
      env : environment.env
    })
    }

    return this.http.get(this.url+'under',options).pipe(
      map((res:any)=>{
        return res.json();
      }),
      catchError((error:any) => {
        return observableThrowError(error.json? error.json().error: error || 'server error');
      }), 
    );

  }


}
