import {Component, OnDestroy} from '@angular/core';
import {Router} from "@angular/router";
import {TranslateService} from '@ngx-translate/core';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements  OnDestroy {

  title = 'self-assessment-frontend-app';
  panelMove = false;

  login:boolean = false;
  constructor(private router: Router,public translate: TranslateService){
    this.router.events.subscribe(value => {
      if(router.url.toString() === '/login') this.login = true;
      else this.login = false;
    });
    translate.setDefaultLang('en');
  }

  ngOnDestroy() {
  }

  get isLogIn() : any {
    return (this.router.url === '/login');
  }

  get isHome() : any {
    return (this.router.url === '/home');
  }
}
