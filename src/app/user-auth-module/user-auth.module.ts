import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { UserAuthRoutingModule } from './user-auth-routing.module';
import { LoginComponent } from './login/login.component';
import {
  MatButtonModule,
  MatIconModule,
  MatRippleModule,
  MatFormFieldModule,
  MatInputModule,
  MatTooltipModule,
  MatNativeDateModule,
  MatDatepickerModule,
  MatSortModule,
  MatSelectModule} from "@angular/material";
import { FormsModule } from '@angular/forms';
export function setTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
  }
@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatSortModule,
    MatSelectModule,
    UserAuthRoutingModule,
    FormsModule,
    ReactiveFormsModule,HttpClientModule,
    TranslateModule

  ]
})
export class UserAuthModule { }
