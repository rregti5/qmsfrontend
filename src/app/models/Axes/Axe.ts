export class Axe {
    constructor(
        public Description: Array<string>,
        public Visible: boolean,
        public Elements : Array<any>,
        ){ }
}