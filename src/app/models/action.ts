export class Action {
    constructor(
        public Description : string,
        public _id : string,
        public Who : string,
        public WhoMail : string,
        public CreatedBy : string,
        public CreatedDate : Date,
        public DueDate : Date,
        public Axe : number,
        public Element : number,
        public Entity : string,
        public Status : string,
        public Comment : string,
        public Attachment : Array<any>,
        public Submitted : boolean
    ){}
}