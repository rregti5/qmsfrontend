export class AppRoutesConstant {
    public static LOGIN = 'login';

    public static USER_LIST = 'users';
    public static USER_DETAILS = 'users/details';

    public static AXES_LIST = 'axes';
    public static AXES_DETAILS = 'axes/details';
    public static AXES_UPDATE = 'axes/edit';

    public static ENTITY_LIST = 'entities';
    public static ENTITY_DETAILS = 'entities/details';

    public static ASSESSMENT_PLAN_LIST = 'assessmentPlan';
    public static ASSESSMENT_PLAN_EDIT = 'assessmentPlan/edit';
    public static ASSESSMENT_PLAN_ADD = 'assessmentPlan/new';

    public static SELF_ASSESSMENT_VIEW = 'self-assessment/details';
    public static SELF_ASSESSMENT_LIST = 'self-assessment/list';

    public static DASHBOARD = 'dashboard';
    public static PDF_AXES_REPORT = 'report';

    public static GUIDE_QMS = 'guide-qms';
    public static BEST_PRACTICE = 'best-practice';
    public static HOME = 'home';
    public static DEFINITIONS = 'definitions';
    public static GLOSSAIRE = 'gloassaire';
    public static ACTIONS = 'Actions'
}
