import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {Router} from "@angular/router";
import {AppRoutesConstant} from "../../../_constants/app-routes";
import {UserService} from '../../../services/user/user.service';
import {EntititesService} from '../../../services/entities/entitites-service.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Validators, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import {SelfAssessmentService} from "../../services";
import { Cookie } from 'ng2-cookies/ng2-cookies';


@Component({
    selector: 'app-header',
    templateUrl: 'header.component.html',
    styleUrls: ['header.component.scss']
})
export class HeaderComponent implements OnInit {

    color: string;
    onOpenSubMenu = false;
    onOpenSubMenuReport = false;
    onOpenSubMenuAsse = false;
    onOpenSubMenuGuid = false;
    initials: string;

    isMenuCollapse = false;

    currentUser : any;

    // Input and Ouptut methos
    @Output() menuClick = new EventEmitter<boolean>();


    constructor(private _router: Router,
                public userService: UserService,
                public dialog: MatDialog,
                private entitiesService: EntititesService,public translate: TranslateService) {
                    if(Cookie.get('language')!==null)
                    translate.setDefaultLang(Cookie.get('language'));
        this.entitiesService.getAllEntities().subscribe(data=> {
        });
    }

    ngOnInit() {
        this.userService.currentUser.Name = Cookie.get('currentUserName');
        this.userService.currentUser._id = Cookie.get('currentUserId');
        this.userService.getCurrentUser().subscribe(data=> {
            //console.log(data)
            this.userService.currentUser.Email = data.Email;
            this.userService.currentUser.Entity = data.Entity
            this.userService.currentUser.Status = data.Status
            this.userService.currentUser.Role = data.Role;
        })
        let splited = this.userService.currentUser.Name.split(' ');
        if (splited.length == 2) {
            this.initials = splited[0][0] + splited[1][0];
        }
        else {
            this.initials = splited[0][0] + splited[0][1];
        }
        this.initials = this.initials.toUpperCase();
    }

    get userListUrl() {
        return ['/' + AppRoutesConstant.USER_LIST];
    }
    get definitions() {
        return ['/' + AppRoutesConstant.DEFINITIONS];
    }
    get addAssessementUrl(){
        //SelfAssessmentService.selfAssessmentToEdit = null;
        return ['/' + AppRoutesConstant.SELF_ASSESSMENT_VIEW];
    }

    get glossaire() {
        return ['/' + AppRoutesConstant.GLOSSAIRE];
    }
        get entityListUrl() {
        return ['/' + AppRoutesConstant.ENTITY_DETAILS];
    }

    get axesListUrl() {
        return ['/' + AppRoutesConstant.AXES_LIST];
    }

    get actionsUrl() {
        return ['/' + AppRoutesConstant.ACTIONS];
    }

    get assessmentUrl() {
        return ['/' + AppRoutesConstant.ASSESSMENT_PLAN_LIST];
    }

    get selfAssessmentUrl() {
        return ['/' + AppRoutesConstant.SELF_ASSESSMENT_LIST];
    }

    get dashboardUrl() {
        return ['/' + AppRoutesConstant.DASHBOARD];
    }
    get homeUrl() {
        return ['/' + AppRoutesConstant.HOME];
    }
    get reportUrl() {
        return ['/' + AppRoutesConstant.PDF_AXES_REPORT];
    }

    get guide_qms() {
        return ['/' + AppRoutesConstant.GUIDE_QMS];
    }

    get best_practice() {
        return ['/' + AppRoutesConstant.BEST_PRACTICE];
    }

    logOut() {
        Cookie.set('token', null,10);
        this.userService.token = null;
        this._router.navigate(['/login']);
    }

    onShowHideMenu = () => {
        this.isMenuCollapse = !this.isMenuCollapse;
        this.menuClick.emit(this.isMenuCollapse);
    }

    openchangePasswordModal(): void {
        const dialogRef = this.dialog.open(ChangePasswordModalComponent, {
          width: '50rem',
        });
    
        dialogRef.afterClosed().subscribe(() => {
          //console.log('The dialog was closed');
        });
    }
    
}


@Component({
    selector : 'change-password-modal',
    templateUrl: 'change-password-modal.html',
    styleUrls: ['header.component.scss']
  })
  export class ChangePasswordModalComponent implements OnInit {
  
    constructor(
      public dialogRef: MatDialogRef<ChangePasswordModalComponent>,
      public userService: UserService,) {}
      Password:string;
      NewPasswordConfirm:string;
      NewPassword:string;

      currentUser:any={};

      error : boolean = false;
      hide : boolean = true;
      hide1 : boolean = true;
      hide2 : boolean = true;

      password = new FormControl('', [Validators.required, Validators.minLength(8)]);
      newPasswordConfirm = new FormControl('', [Validators.required, Validators.minLength(8)]);
      newPassword = new FormControl('', [Validators.required, Validators.minLength(8)]);
  
    ngOnInit(){
        this.userService.getCurrentUser().subscribe(data=>{
            //console.log(data)
            this.currentUser.Email = data.Email;
            this.currentUser.Entity = data.Entity
            this.currentUser.Status = data.Status
            this.currentUser.Role = data.Role;
            this.currentUser.Name = data.Name;
            this.currentUser._id = data._id;
            this.currentUser.id = data.id;
        })
    }
  
    onSubmitChangePasswordform(){
        
        this.userService.authenticate(this.userService.currentUser.Email,this.Password).subscribe(response=>{
            //console.log(response);
            if(!!response.message){
                this.error = true;
            }else{
                this.currentUser.Password = this.NewPassword;
                this.userService.updateUser(this.currentUser,true).subscribe(data=>{
                    this.dialogRef.close();
                })
            }
        })
    }
  
    onNoClick(): void {
      this.dialogRef.close();
    }
  }
