import {Component, OnDestroy, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AppRoutesConstant} from "../../../../_constants/app-routes";
import {AssessmentPlanService, AxesService} from "../../../services";
import {Observable} from "rxjs";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { Axe } from 'app/models/Axes/Axe';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlattener, MatTreeFlatDataSource } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { expand } from 'rxjs/operators';
import { Cookie } from 'ng2-cookies/ng2-cookies';


interface Axis {
    axis: any;
    children?: Axis[];
  }
  
  var TREE_DATA: Axis[] = [];
  
  /** Flat node with expandable and level information */
  interface ExampleFlatNode {
    expandable: boolean;
    name: string;
    level: number;
  }
    

@Component({
    selector: 'app-assessment-plan-details',
    templateUrl: './assessment-plan-details.component.html',
    styleUrls: ['./assessment-plan-details.component.scss']
})
export class AssessmentPlanDetailsComponent implements OnInit, OnDestroy {

    axesList = [];
    // Data Variables
    planId = '';
    planDetail = null;
    isEditMode = false;
    add="ADD";
    edit="EDIT";
    enabled = false;
     
    
    assessmentPlanForm: FormGroup;
    private subParams$: any;
    @ViewChild('tree') tree;
    selectedAxes : Array<any> = [];
    selectedExhaustiveAxes : Array<any> = [];
    oldSelectedAxes : Array<any> = [];
    constructor(private _route: Router,
                private _fb: FormBuilder,
                private _axesService: AxesService,
                private _activeRoute: ActivatedRoute,
                private _assessmentPlanService: AssessmentPlanService,public translate: TranslateService) {
                    if(Cookie.get('language')!==null)
                    translate.setDefaultLang(Cookie.get('language'));
                    if(Cookie.get('language')==='fr')
                    {
                         this.add="Ajouter",
                         this.edit="Modifier"
                    }
                  
                   
    }
   

    ngOnInit() {
        this.createAssessmentPlanForm();
        this.bindAxes();
        this.routeSubscriber();
        
    }
   expand:boolean=false;
    expandAll() {
        console.log("aaa")
          console.log(this.expand);
        if(this.expand===false){
          this.tree.treeControl.expandAll();
          this.expand=true;  
          console.log(this.expand);
        }
        
      }

    createAssessmentPlanForm = () => {
        this.assessmentPlanForm = this._fb.group({
            Code: ['', [
                <any>Validators.required
            ]],
            Description: ['', [
                <any>Validators.required
            ]],
            Axes: ['', [
                <any>Validators.required
            ]]
        })
    };

    bindAxes = () => {
        this.getAxes().subscribe(response => {
            this.axesList = [];
            response.data.forEach(axis => {
                if ( axis.Visible != false ){
                    this.axesList.push(axis);
                }
                
            });
            this.initData();
            //console.log(this.axesList);
            //this.axesList = response.data;
        })
    }

    getAxes = (): Observable<any> => {
        return this._axesService.getAllAxes();
    }

    onSubmitSelfAssessmentForm = (form: FormGroup) => {
        if (form.valid) {
            let flag;            
            let Axes = [];
            let count = 0;
            this.axesList.forEach((axis,i)=>{
                if(this.selectedExhaustiveAxes.includes(axis._id)){
                    count++;
                }
            })
            this.axesList.forEach((axis,i)=>{
                if(this.selectedExhaustiveAxes.includes(axis._id)){
                    let elmts = [];
                    flag = true;
                    axis.Elements.forEach(element => {
                        if(!this.selectedExhaustiveAxes.includes(''+i+'||'+element.Description)){
                            flag = false;
                        }else{
                            elmts.push(element);
                        }
                    });
                    if(flag){
                        Axes.push(axis._id);
                        if(Axes.length == count){
                            let params = form.value;
                            params.Axes = Axes;
                            //console.log(params)
                            this._assessmentPlanService.createUpdateSelfAssessmentPlan(params, this.planId)
                            .subscribe(response => {
                                //console.log(response);
                                console.log(this.selectedExhaustiveAxes)
                                this.onBack();
                            })
                        }
                    }else{
                        let params = {
                            Description: axis.Description,
                            Visible : false,
                            Elements : elmts
                        };
                        this._axesService.saveAxes(params).subscribe(data=>{
                            //console.log(data.data)
                            Axes.push(data.data._id);
                            if(Axes.length == count){
                                let params = form.value;
                                params.Axes = Axes;
                                //console.log(params)
                                this._assessmentPlanService.createUpdateSelfAssessmentPlan(params, this.planId)
                                .subscribe(response => {
                                    //console.log(response);
                                    console.log(this.selectedExhaustiveAxes)
                                    this.onBack();
                                })
                            }
                        })
                    }
                }
            })
           
        }
    }

    onBack = () => {
        this._route.navigate(['/' + AppRoutesConstant.ASSESSMENT_PLAN_LIST]);
    }

    patchPlanForm = (planDetail) => {
        this.assessmentPlanForm.patchValue({
            Code: planDetail.Code,
            Description: planDetail.Description,
            Axes: planDetail.Axes
        });
        this.selectedAxes = [];
        this.selectedExhaustiveAxes = [];
        this.oldSelectedAxes = [];
        //console.log(planDetail)
        planDetail.Axes.forEach((axe,k) => {
            this._axesService.getSingleAxeDetail(axe).subscribe(response=>{
                
                let axis = response.data;
                this.axesList.forEach((AXE,i)=>{
                    if(AXE.Description === axis.Description){
                        this.selectedExhaustiveAxes.push(AXE._id);
                        this.oldSelectedAxes.push(AXE._id);
                        axis.Elements.forEach((element,j) => {
                            this.selectedExhaustiveAxes.push(i+'||'+element.Description);
                            this.oldSelectedAxes.push(i+'||'+element.Description);
                            if(k==planDetail.Axes.length - 1 && j == axis.Elements.length-1){
                                //console.log(this.selectedExhaustiveAxes);
                                this.selectedAxes = this.selectedExhaustiveAxes;
                                this.selectedAxes = [...this.selectedAxes]
                            }
                        });
                    }
                })
            })
        });
    };

    ngOnDestroy() {
        if (this.subParams$) {
            this.subParams$.unsubscribe();
        }
    }

    /**
     * router params subscriber
     */
    private routeSubscriber = () => {
        this.subParams$ = this._activeRoute.params.subscribe(params => {
            this.planId = params['id'];
            if (this.planId) {
                this.isEditMode = true;
                this._assessmentPlanService.getSingleAssessmentPlanDetail(this.planId)
                    .subscribe(response => {
                        this.planDetail = response.data;
                        this.patchPlanForm(this.planDetail);
                    });
            }
        });
    };


    private transformer = (node: Axis, level: number) => {
        return {
            expandable: !!node.children && node.children.length > 0,
            name: node.axis.Description,
            axis: node.axis,
            level: level,
        };
    }
    
    treeControl = new FlatTreeControl<ExampleFlatNode>(
        node => node.level, node => node.expandable);

    treeFlattener = new MatTreeFlattener(
        this.transformer, node => node.level, node => node.expandable, node => node.children);

    treeSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    hasChild = (_: number, node: ExampleFlatNode) => node.expandable;

    initData(){
        TREE_DATA = [];
        this.axesList.forEach((axis,i)=>{
            let Elements = [];
            axis.Elements.forEach(element => {
                element.axis = i;
                Elements.push({axis:element});
            });
            TREE_DATA.push({
                axis : axis,
                children : Elements
            })
        })

        this.treeSource.data = TREE_DATA;

        this.enabled = true;
    }
   

    
      
     
    

    SelectedAxeChange(event){
        //console.log(event.path.length ) 
        
        //console.log(this.selectedAxes);
        let axes=[];
        this.axesList.forEach(axis=>{
            axes.push(axis._id);
        })
        let value;
        if(event.path.length == 13){
            value = (event.path[2].id)
        }else if(event.path.length == 12){
            value = (event.path[1].id)
        }else{
            value = (event.path[0].id)
        }
        if(this.selectedExhaustiveAxes.includes(value) && !axes.includes(value) && value.split('||').length >1) {
            /* console.log(this.selectedExhaustiveAxes.length);*/
            //console.log(value)
            this.selectedExhaustiveAxes.splice(this.selectedExhaustiveAxes.indexOf(value),1);
            let indx = parseFloat(value.split('||'));
            let flag = false;
            this.axesList[indx].Elements.forEach((element,j) => {
                if(this.selectedExhaustiveAxes.includes(indx+"||"+element.Description)){
                    //console.log(flag)
                    flag = true;
                }
            });
            if(!flag){
                if(this.selectedExhaustiveAxes.includes(this.axesList[indx]._id)){
                    this.selectedExhaustiveAxes.splice(this.selectedExhaustiveAxes.indexOf(this.axesList[indx]._id),1)
                }
            }
            //console.log(this.selectedExhaustiveAxes.length);
        }else{
            this.selectedAxes.forEach(axe=>{
                if(!this.selectedExhaustiveAxes.includes(axe)){
                    this.selectedExhaustiveAxes.push(axe);
                }
            })
            this.selectedExhaustiveAxes.forEach(axis=>{
                if(!this.selectedAxes.includes(axis) && value.split('||').length == 1){
                    this.selectedExhaustiveAxes.splice(this.selectedExhaustiveAxes.indexOf(axis),1);
                }
            })
        }

        /* this.axesList.forEach(axis=>{
            if(!this.selectedAxes.includes(axis._id)&&this.selectedExhaustiveAxes.includes(axis._id)){
                this.selectedExhaustiveAxes.splice(this.selectedExhaustiveAxes.indexOf(axis._id),1);
            }
        }) */

        
        let oldSelectedAxes = this.oldSelectedAxes;
        let selectedAxes = this.selectedExhaustiveAxes;

        //detecting the type and the element changed
        for(var i = 0; i< selectedAxes.length; i++ ){
            if( !oldSelectedAxes.includes(selectedAxes[i]) ){
                //element i is added
                let axe = selectedAxes[i];
                let axis;
                let found = false;
                for(var j=0;j<this.axesList.length;j++){
                    axis = this.axesList[j];
                    if(axis._id === axe){
                        //found the added axe
                        axis.Elements.forEach(element => {
                            selectedAxes.push(j+'||'+element.Description);
                        });
                        found = true;
                        break;
                    }
                    axis.Elements.forEach(element => {
                        if(axe === j+'||'+element.Description){
                            if(!selectedAxes.includes(axis._id)) selectedAxes.push(axis._id)
                            found = true;
                        }
                    });
                    if(found) break;
                }
                break;
            }
        }
        for(var i = 0; i < oldSelectedAxes.length; i++ ){
            if( !selectedAxes.includes(oldSelectedAxes[i]) ){
                //element i is removed
                let axis;
                let axe = oldSelectedAxes[i];
                let found = false;
                for(var j=0;j<this.axesList.length;j++){
                    axis = this.axesList[j];
                    if(axis._id === axe){
                        //found the removed axe
                        axis.Elements.forEach(element => {
                            if(selectedAxes.includes(j+'||'+element.Description)){
                                selectedAxes.splice(selectedAxes.indexOf(j+'||'+element.Description),1);
                            }
                        });
                        found = true;
                        break;
                    }
                    axis.Elements.forEach(element => {
                        if(axe === j+'||'+element.Description){
                            
                            found = true;
                        }
                    });
                    if(found) break;
                }
                break;
            }
        }
        this.selectedAxes = selectedAxes;
        this.selectedExhaustiveAxes = selectedAxes;
        this.selectedExhaustiveAxes = [...this.selectedExhaustiveAxes];
        this.selectedAxes = [...this.selectedAxes];
        this.oldSelectedAxes = selectedAxes;
        this.oldSelectedAxes = [...this.oldSelectedAxes];

    }

    /* saveAxe(params) : any{
        this._axesService.saveAxes(params).subscribe(data=>{
            console.log(data.data)
           return data.data;
        })
    } */
    
    
}
