import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {UserAuthModule} from "./user-auth-module/user-auth.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {FeatureModule} from "./feature-module/feature.module";
import { HttpModule} from '@angular/http';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FileSelectDirective } from 'ng2-file-upload';
import {UtilityModule} from "@utility-module";
import { MaterialModule } from './material-module/material.module';
import {PublicModule} from "./public-module/public.module";
import { environment } from '@env/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule, StorageBucket } from '@angular/fire/storage';
import { ChartsModule } from 'ng2-charts';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HomeComponent } from './home/home.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { DefinitionsComponent } from './definitions/definitions.component';
import { GloassaireComponent } from './gloassaire/gloassaire.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DefinitionsComponent,
    GloassaireComponent
  ],
  imports: [
    SlickCarouselModule,
    ChartsModule,
    BrowserModule,
    BrowserAnimationsModule,
    UserAuthModule,
    FeatureModule,
    PublicModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    NgxDatatableModule,
    UtilityModule,
    MaterialModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule,HttpClientModule,TranslateModule ,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory:(createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [{ provide: StorageBucket, useValue: 'qm-qms.appspot.com' }/* ,
  {provide: LocationStrategy, useClass: HashLocationStrategy} */],
  bootstrap: [AppComponent]

})
export class AppModule {
}
// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
