import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntitiesDetailsComponent } from './entities-details.component';

describe('EntitiesDetailsComponent', () => {
  let component: EntitiesDetailsComponent;
  let fixture: ComponentFixture<EntitiesDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntitiesDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntitiesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
