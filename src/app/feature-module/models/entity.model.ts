export interface EntityModel {
    AssesmentPlan: string;
    AssessmentPlan: string;
    CalculatedScore: string | number;
    Code: string;
    Description: string;
    Score: string | number;
    ScoreGoal: number;
    SelfAssesment: []
    SubEntities: string[]
    Type: string;
    id: string
    __v: number;
    _id: string;
}

