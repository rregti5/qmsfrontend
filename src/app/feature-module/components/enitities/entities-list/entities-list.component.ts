import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AppRoutesConstant} from "../../../../_constants/app-routes";
import { TranslateService } from '@ngx-translate/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';

interface PeriodicElement {
  code: string;
  description: string;
  type: string;
  subEntity: string;
  assessmentPlan: string;
  selfAssessment: string;
  score: number;
  calculatedScore: number;
  scoreGoal: number;
  action: any;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {
    code: 'GM546',
    description: 'This is the dummy description',
    type: 'Plant 1',
    subEntity: 'Workshop 1',
    assessmentPlan: 'Excellence',
    selfAssessment: 'Site 1',
    score: 25,
    calculatedScore: 20,
    scoreGoal: 45,
    action: ''
  },
];

@Component({
  selector: 'app-entities-list',
  templateUrl: './entities-list.component.html',
  styleUrls: ['./entities-list.component.scss']
})
export class EntitiesListComponent implements OnInit {

  displayedColumns: string[] = ['code', 'description', 'type', 'subEntity', 'assessmentPlan', 'selfAssessment',
    'score', 'calculatedScore', 'scoreGoal', 'action'];
  dataSource = ELEMENT_DATA;

  color: string;

  constructor(private _router: Router,public translate: TranslateService) {
    if(Cookie.get('language')!==null)
    translate.setDefaultLang(Cookie.get('language'));
  }

  ngOnInit() {
  }

  onAddEntity = () => {
    this._router.navigate(['/' + AppRoutesConstant.ENTITY_DETAILS])
  }

}
