import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AppRoutesConstant} from "../../../../_constants/app-routes";
import {Observable} from "rxjs";
import {AxesService, AssessmentPlanService} from "../../../services";
import {AxesModelNew, ElementModel, ScoreModel, AxesModel, ScoreModelNew} from "../../../models";
import {PAGE_SIZE_OPTIONS} from "@constants/*";
import { ScoreDescirptionColorList } from 'app/utility-module/shared-functions/common-functions';

@Component({
    selector: 'app-axes-list',
    templateUrl: './axes-list.component.html',
    styleUrls: ['./axes-list.component.scss']
})

export class AxesListComponent implements OnInit {

    isEditElement = false;
    axesList: AxesModelNew[] = [];
    displayAxesList: AxesModelNew[] = [];
    removeAxeId;
    isRemoveAxe = false;
    assessmenPlan:any;
    colorList = ScoreDescirptionColorList
     displayerr=false;
    // Pagination Variables
    isLoadingResults = true;
    pageSizeOptions = PAGE_SIZE_OPTIONS;
    pageSize = PAGE_SIZE_OPTIONS[1];
    pageIndex = 0;
    resultsLength = 0;

    constructor(private _router: Router,
                private _axesService: AxesService,private _assessmentPlanService: AssessmentPlanService) {
    }

    ngOnInit() {
        this.bindAxesList();
        // this._axesService.removeAxes('5cf43c8035f5b839c8d6329a').subscribe(response => {
            
        // })
    }

    bindAxesList = () => {
        this.getAxesList().subscribe(response => {
            this.handleAxesListResponse(response);
            console.log(response)
        },err=>{},()=>{
            console.log("aaaa")
        })
    }

    getAxesList = (): Observable<any> => {
        return this._axesService.getAllAxes();
    }

    handleAxesListResponse = (response) => {
        this.axesList = response.data || [];
        this.axesList = this.axesList.filter((elme,index)=>{
            return (elme.Visible == null || elme.Visible==true)
        })
        this.resultsLength = this.axesList.length;
        this.displayAxesList = this.axesList.filter((elem, index) => {
            return index < this.pageSize
        })

        this.displayAxesList.sort(function (a, b) {
            if (a.Description < b.Description) {
                return -1;
            }
            if (a.Description > b.Description) {
                return 1;
            }
            return 0;
        })    
        console.log(this.displayAxesList)    
    }

    onAddAxis = () => {
        this._router.navigate(['/' + AppRoutesConstant.AXES_DETAILS])
    }

    onEditAxe = (event, axe: AxesModelNew) => {
        event.stopPropagation();
        this._router.navigate(['/' + AppRoutesConstant.AXES_UPDATE, axe.id]);
    }

    onRemoveAxe = (event, axe: AxesModelNew) => {
        event.stopPropagation();
        this.removeAxeId = axe.id;
        this.isRemoveAxe = true;
    }

    onRemoveConfirm = (axe) => {
        if (this.removeAxeId) {
            this._assessmentPlanService.getAllAssessmentPlans().subscribe(response=>{
                
                response.data.forEach(res => {
                   res.Axes.forEach(axe => {
                      
                    if(axe==this.removeAxeId){
                        this.displayerr=true;
                        this.onHideConfirmationModal();
                      
                    }
                 
                }); 
                });
                
                if(this.displayerr==false)
                {
                     this._axesService.removeAxes(this.removeAxeId).subscribe(response => {
                      
                this.axesList = this.axesList.filter(elem => elem.id !== this.removeAxeId);
                this.resultsLength = this.axesList.length;
                if (this.resultsLength / this.pageSize === this.pageIndex) {
                    this.pageIndex--;
                }
                this.bindDisplayAxesList();
                this.onHideConfirmationModal();
            }
              
            )
                }
                
               
            })
           console.log(this.displayerr)
        }
    }

    onHideConfirmationModal = () => {
        this.removeAxeId = null;
        this.isRemoveAxe = false;
    }

    onClickSave = (axe: AxesModelNew, score: ScoreModelNew, axeIndex: number, elementIndex: number, scoreIndex: number) => {
        const description= score.Description || [];
        for(let i=0;i< description.length;i++){
            let scoreElementId = axe.id + '_' + elementIndex + '_' + score.Note+'_'+i;
            const currentScoreElement = document.getElementById(scoreElementId);
            axe.Elements[elementIndex].Scores[scoreIndex].Description[i] = currentScoreElement['value'];
        }
        const axeId = axe.id;
        const axeParams = axe;
        this._axesService.updateAxes(axeId, axeParams).subscribe(response => {
            this.axesList[axeIndex].__v = response.data.__v;
            // this will update __v, so that we can call another update call after one update is made
            // if we don't do this it will get error
        })
    }

    onChangeAttachment = (event, element: ElementModel) => {
        element.AttachementControl = event.value;
    }

    changePage = (event) => {
        const {pageIndex, pageSize} = event;
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
        this.bindDisplayAxesList();
    }

    bindDisplayAxesList = () => {
        this.displayAxesList = this.axesList.filter((elem, index) => {
            return index >= this.pageIndex * this.pageSize && index < ((this.pageIndex + 1) * this.pageSize);
        })
    }
}

// onClickSave = (axe: AxesModelNew, score: ScoreModel, axeIndex: number, elementIndex: number, scoreIndex: number) => {
//     const id = axe.id + '_' + elementIndex + '_' + score.Note;
//     const currentScoreElement = document.getElementById(id);

//     axe.Elements[elementIndex].Scores[scoreIndex].Description = currentScoreElement['value'];

//     const axeId = axe.id;
//     const axeParams = axe;

//     this._axesService.updateAxes(axeId, axeParams).subscribe(response => {
//         this.axesList[axeIndex].__v = response.data.__v;
//         // this will update __v, so that we can calla nother update call after one update is made
//         // if we don't do this it will get error
//     })
// }