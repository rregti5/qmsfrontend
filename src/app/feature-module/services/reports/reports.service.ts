import { Injectable } from '@angular/core';
import { RequestOptionsArgs,Headers, Http } from '@angular/http';
import { environment } from '@env/environment';
import { map, catchError } from 'rxjs/operators';
import {Observable, throwError as observableThrowError} from 'rxjs';
import { Cookie } from 'ng2-cookies/ng2-cookies';


@Injectable({
  providedIn: 'root'
})
export class ReportsService {

    url1 =environment.apiUrl+'/reports/' ;
    url = environment.apiUrl+'/reporting/api/report' ;

  constructor( 
    private http : Http
    ) { }

  /* getAssessmentReport(body) {
    let options: RequestOptionsArgs = {
        responseType: 3,
        headers: new Headers({
            Authorization: 'Bearer ' + Cookie.get('token'),
            env : environment.env
        })
    }

    return this.http.post(this.url + 'assessment',body, options).pipe(
        map((res: any) => {
            return res.json();
        }),
        catchError((error: any) => {
            return observableThrowError(error.json ? error.json().error : error || 'server error');
        }),
    );
  } */
    getAssessmentReport(body) {
        let options: RequestOptionsArgs = {
            responseType: 3,
            headers: new Headers({
                Authorization: 'Bearer ' + Cookie.get('token'),
                env : environment.env,
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            })
        }

        return this.http.post(this.url,body, options).pipe(
            map((res: any) => {
                return res.json();
            }),
            catchError((error: any) => {
                return observableThrowError(error.json ? error.json().error : error || 'server error');
            }),
        );
    }

    getHistory(entity){
        let options: RequestOptionsArgs = {
            headers: new Headers({
                Authorization: 'Bearer ' + Cookie.get('token'),
                env : environment.env,
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            })
        }

        return this.http.get(this.url1+'getHistoryScore/'+entity, options).pipe(
            map((res: any) => {
                return res.json();
            }),
            catchError((error: any) => {
                return observableThrowError(error.json ? error.json().error : error || 'server error');
            }),
        );
    }

    getScore(entity){
        let options: RequestOptionsArgs = {
            headers: new Headers({
                Authorization: 'Bearer ' + Cookie.get('token'),
                env : environment.env,
                'Content-Type': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            })
        }

        return this.http.get(this.url1+'currentScore/'+entity, options).pipe(
            map((res: any) => {
                return res.json();
            }),
            catchError((error: any) => {
                return observableThrowError(error.json ? error.json().error : error || 'server error');
            }),
        );
    }
  
}
