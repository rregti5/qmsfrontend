import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {APIManager} from "@services/apimanager.service";
import {HttpMethodsTypeEnum, QMSApi} from "@constants/*";

@Injectable({
    providedIn: 'root'
})
export class AxesService {

    public axes: Array<any> = [];

    constructor(private _apiManager: APIManager) {
    }

    saveAxes = (params): Observable<any> => {
        return this._apiManager.httpHelperMethod(
            HttpMethodsTypeEnum.POST, QMSApi.AXES_ADD, params,
            this._apiManager.Authorised_HttpOptions, false, true);
    };

    updateAxes = (uuid, params): Observable<any> => {
        return this._apiManager.httpHelperMethod(
            HttpMethodsTypeEnum.PUT, `${QMSApi.AXES_UPDATE}/${uuid}`, params,
            this._apiManager.Authorised_HttpOptions, false, true);
    };

    getAllAxes = (): Observable<any> => {
        return this._apiManager.httpHelperMethod(
            HttpMethodsTypeEnum.GET, QMSApi.AXES_ALL, {},
            this._apiManager.Authorised_HttpOptions, false, true);
    };

    getSingleAxeDetail = (id): Observable<any> => {
        return this._apiManager.httpHelperMethod(
            HttpMethodsTypeEnum.GET, `${QMSApi.AXES_SINGLE}/${id}`, {},
            this._apiManager.Authorised_HttpOptions, false, true);
    };

    removeAxes = (id): Observable<any> => {
        return this._apiManager.httpHelperMethod(
            HttpMethodsTypeEnum.DELETE, `${QMSApi.AXES_REMOVE}/${id}`, {},
            this._apiManager.Authorised_HttpOptions_JSON, true, true);
    };
}
