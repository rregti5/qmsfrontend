import {Injectable} from '@angular/core';
import {Http, RequestOptionsArgs, Headers} from '@angular/http';
import {throwError as observableThrowError, Observable, Subject} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {UserService} from '../user/user.service';
import {Entity} from "../../models/Entity";
import { environment } from '@env/environment';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Injectable({
    providedIn: 'root'
})
export class EntititesService {

    entities: Array<Entity> = [];
    userEntities : Array<Entity>=[];
    url = environment.apiUrl + "/entity/";

    constructor(private http: Http,private userService: UserService) {
    }

    updateEntity(entity) {
        let options: RequestOptionsArgs = {
            headers: new Headers({
                Authorization: 'Bearer ' + Cookie.get('token'),
                env : environment.env,
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest'
            })
        }
        return this.http.put(this.url + 'entity_Update/' + entity._id, entity, options).pipe(
            map((res: any) => {
                return res.json();
            }),
            catchError((error: any) => {
                return observableThrowError(error.json ? error.json().error : error || 'server error');
            }),
        );
    }

    deleteEntity(entity) {
        let options: RequestOptionsArgs = {
            headers: new Headers({
                Authorization: 'Bearer ' + Cookie.get('token'),
                env : environment.env
            })
        }
        return this.http.delete(this.url + 'entity_Delete/' + entity._id, options).pipe(
            map((res: any) => {
                return res.json();
            }),
            catchError((error: any) => {
                return observableThrowError(error.json ? error.json().error : error || 'server error');
            }),
        );
    }

    getAllEntities() {
        this.entities = [];
        let options: RequestOptionsArgs = {
            headers: new Headers({
                Authorization: 'Bearer ' + Cookie.get('token'),
                env : environment.env
            })
        }
        return this.http.get(this.url + "all", options).pipe(
            map((res: any) => {
                this.entities =[]
                let obj = res.json();
                obj.entities.forEach(element => {
                    //console.log(element)
                    let ent = new Entity(element._id, element.Code, element.Description, element.Type, element.SubEntities, element.AssessmentPlan, element.SelfAssessment, element.Score, element.CalculatedScore, element.ScoreGoal);
                    this.entities.push(ent);
                });
                
                return res.json();
            }),
            catchError((error: any) => {
                return observableThrowError(error.json ? error.json().error : error || 'server error');
            }),);
    }

    addEntity(entity: Entity) {
        let options: RequestOptionsArgs = {
            headers: new Headers({
                Authorization: 'Bearer ' + Cookie.get('token'),
                env : environment.env
            })
        }
        let body = {
            code: entity.Code,
            description: entity.Description,
            type: entity.Type,
            subEntities: entity.SubEntities,
            assessmentPlan: entity.AssessmentPlan,
            selfAssessment: entity.SelfAssesment,
            score: entity.Score,
            scoreGoal: entity.ScoreGoal,
            calculatedScore: entity.CalculatedScore
        }
        return this.http.post(this.url + "entity_Create", body, options).pipe(
            map((res: any) => {
                return res.json();
            }),
            catchError((error: any) => {
                return observableThrowError(error.json ? error.json().error : error || 'server error');
            }),);

    }
}
