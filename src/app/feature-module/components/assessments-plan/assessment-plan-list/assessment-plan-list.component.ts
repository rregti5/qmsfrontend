import {Component, NgModule, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {AppRoutesConstant} from "../../../../_constants/app-routes";
import {AssessmentPlanService, AxesService} from "../../../services";
import {Observable} from "rxjs";
import {PAGE_SIZE_OPTIONS} from "@constants/*";
import {MatChipsModule ,MatPaginator, MatTableDataSource} from "@angular/material";
import { TranslateService } from '@ngx-translate/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
    selector: 'app-assessment-plan-list',
    templateUrl: './assessment-plan-list.component.html',
    styleUrls: ['./assessment-plan-list.component.scss']
})
@NgModule({
        imports: [MatChipsModule]
})
export class AssessmentPlanListComponent implements OnInit {

    displayedColumns: string[] = ['Position', 'Code', 'Description', 'Axes', 'Action'];
    @ViewChild(MatPaginator) paginator: MatPaginator;
    assessmentPlanList: MatTableDataSource<any> = new MatTableDataSource([]);
    isRemovePlan = false;
    color: string;
    removePlanId;

    // Pagination Variables
    isLoadingResults = true;
    pageSizeOptions = PAGE_SIZE_OPTIONS;
    pageSize = PAGE_SIZE_OPTIONS[1]
    resultsLength = 0;

    axes: Array<any> = [];

    constructor(private _router: Router,
                private _assessmentPlanService: AssessmentPlanService,
                private _axesService: AxesService,public translate: TranslateService) {
                    if(Cookie.get('language')!==null)
                    translate.setDefaultLang(Cookie.get('language'));
    }

    ngOnInit() {

        this.assessmentPlanList.paginator = this.paginator;
        this.bindAssessmentPlanList();

    }

    bindAssessmentPlanList = () => {
        this.getAssessmentPlanList().subscribe(response => {
            console.log(response)
            let AXES = []
            this.assessmentPlanList.data = response.data;
            response.data.forEach(dat => {
                this.axes.push([]);
            })
            this._axesService.getAllAxes().subscribe(data => {
                this._axesService.axes = data.data;
                this.assessmentPlanList.data.forEach(plan => {
                    let cont = [];
                    this._axesService.axes.forEach(axis => {
                        if (plan.Axes.indexOf(axis._id) != -1) {
                            cont.push(axis.Description);
                        }
                    })
                    AXES.push(cont);
                })
                this.axes = AXES;
            })
        })
    }
    

    getAssessmentPlanList = (): Observable<any> => {
        return this._assessmentPlanService.getAllAssessmentPlans();
        
    }

    onAddPlan = () => {
        this._router.navigate(['/' + AppRoutesConstant.ASSESSMENT_PLAN_ADD]);
    }

    onEditPlan = (plan) => {
        this._router.navigate(['/' + AppRoutesConstant.ASSESSMENT_PLAN_EDIT, plan.id]);
    }

    onRemovePlan = (plan) => {
        this.removePlanId = plan.id;
        this.isRemovePlan = true;
    }

    onRemoveConfirm = (plan) => {
        if (this.removePlanId) {
            this._assessmentPlanService.removeAssessmentPlan(this.removePlanId).subscribe(response => {
                this.assessmentPlanList.data = this.assessmentPlanList.data.filter(elem => elem.id !== this.removePlanId)
                setTimeout(() => {
                    this.assessmentPlanList.paginator = this.paginator;
                }, 2);
                this.onHideConfirmationModal();
            })
        }
    }

    onHideConfirmationModal = () => {
        this.removePlanId = null;
        this.isRemovePlan = false;
    }
}
