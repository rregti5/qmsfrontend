import {Component, OnInit, ViewChild} from '@angular/core';

import {SelfAssessmentService, AssessmentPlanService, DashboardService, EntitiesService} from 'app/feature-module/services';
import {UserService} from 'app/services/user/user.service';
import {EntititesService} from 'app/services/entities/entitites-service.service';
import {ReportsService} from 'app/feature-module/services/reports/reports.service';
import {  MatSort, MatTableDataSource, Sort, MatPaginator,PageEvent, MatTreeFlatDataSource, MatTreeFlattener } from "@angular/material";

//declare let jsPDF;
import {MultiDataSet, Label} from 'ng2-charts';
import {ChartOptions, ChartType, ChartDataSets} from 'chart.js';
import { TranslateService } from '@ngx-translate/core';
import { ActionsService } from 'app/services/actions/actions.service';
import { Entity } from 'app/models/Entity';
import { FlatTreeControl } from '@angular/cdk/tree';


interface Entit {
    entity: Entity;
    children?: Entit[];
}

var TREE_DATA: Entit[] = [];

/** Flat node with expandable and level information */
interface ExampleFlatNode {
    expandable: boolean;
    name: string;
    level: number;
}

@Component({
    selector: 'app-axes-pdf',
    templateUrl: './axes-pdf.component.html',
    styleUrls: ['./axes-pdf.component.scss']
})
export class AxesPdfComponent implements OnInit {


    data: any;

    date = new Date().toLocaleString();
    assessmentPlans: Array<any>;
    scores: any;
    goals: any;
    pourcentages: any;

    @ViewChild(MatSort) sort: MatSort;

    // Map lat and lng Variables
    lat = 51.678418;
    lng = 7.809007;


    // MatPaginator Output
    pageEvent: PageEvent;

    length = 100;
/*     pageSize = 10;
    pageSizeOptions: number[] = [5, 10, 25, 100]; */

    public lineChartLegend: boolean = false;
    public lineChartType: string = 'line';

    // Doughnut
    public doughnutChartLabels: Label[] = ['In Progress','Finished'];
    public doughnutChartData: MultiDataSet = [
        [0,0]
    ];
    public doughnutChartLabels3: Label[] = ['Score','Goal'];
    public doughnutChartData3: MultiDataSet = [
        [0,0]
    ];
    public doughnutChartLabels2: Label[] = ['In Progress','Finished','Published'];
    public doughnutChartData2: MultiDataSet = [
        [0,0,0]
    ];
    public doughnutChartType: ChartType = 'doughnut';
    public doughnutChartOptions : ChartOptions = {
        responsive: true,
        cutoutPercentage: 70,
    }
    public doughnutChartOptions3 : ChartOptions = {
        responsive : true,
        cutoutPercentage : 70,
        tooltips: {
            callbacks: {
                label: function(tooltipItem, data) {
                    let lbl = data.labels[tooltipItem.index]+' : ';
                    if(tooltipItem.index == 0){
                        lbl += data.datasets[0].data[0].toString();
                    }else{
                        let num = <number> data.datasets[0].data[0] + <number> data.datasets[0].data[1];
                        lbl += num.toString();
                    }
                    return lbl;
                }
            }
        }

    }

    public barChartOptions: ChartOptions = {
        responsive: true,
        // We use these empty structures as placeholders for dynamic theming.
        scales: {xAxes: [{}], yAxes: [{ticks:{suggestedMax:5,suggestedMin:0}}]},
        plugins: {
            datalabels: {
                anchor: 'end',
                align: 'end',
            }
        }
    };
    public barChartLabels: Label[] = [];
    public barChartType: ChartType = 'bar';
    public barChartLegend = true;


    public barChartData: ChartDataSets[] = [
        {data: [], label: 'Scores'}
    ];

    barChartColorData = ['#3dafe8', '#fac566', '#5aae52', '#a05fc2'];

    // lineChart
    public lineChartData: Array<any> = [
        {data: [], label: 'Score'}
    ];
    public lineChartLabels: Array<any> = [];
    public lineChartOptions: any = {
        responsive: true,
        scales: {
            showLine: false,
            xAxes: [{
                gridLines: {
                    height: 200,
                    display: false,
                    offsetGridLines: true,
                },
                ticks: {
                    fontSize: 10
                }
            }],
            yAxes: [{
                ticks: {
                    suggestedMax: 5,
                    suggestedMin: 0
                }
            }]
        }
    };


    public lineChartColors: Array<any> = [
        { // grey
            backgroundColor: 'rgba(185,216,231,0.5)',
            borderColor: '#b9d8e7',
            pointBackgroundColor: '#ffffff',
            pointBorderColor: '#b9d8e7',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)',
            pointBorderWidth: 2
        }
    ];

    barChartColors = [{
        backgroundColor: this.barChartColorData
    }];

    selectedEntity : string;
    selectedEntityName : string;
    entities : Array<any> = [];
    currentScores = {
        currentScore : 0,
        goal: 0,
        averageScore: 0,
        pourcentage :0,
    }


    constructor(private _selfAssessmentService: SelfAssessmentService,
                private _actionsService:ActionsService,
                private _userService: UserService,
                public _entityService: EntititesService,
                private _entitieService : EntitiesService,
                private _assPlanService: AssessmentPlanService,
                private _dashboardService: DashboardService,
                private _reportsService: ReportsService,
                public translate: TranslateService,) {
        this._userService.getAllUsers().subscribe(users=> {
            this._userService.users = users;
        })
        this._assPlanService.getAllAssessmentPlans().subscribe(response=> {
            this.assessmentPlans = response.data
        })
        this._entityService.getAllEntities().subscribe(data=> {
            this._entityService.entities = data.entities;
        })
        this._userService.getCurrentUser().subscribe(data=> {
            this._entitieService.getAllEntities().subscribe((response) => {
                this.entities = response.entities;
                this.selectedEntity = this.entities[0]._id;
                this.initData();
                this.entitySelectionChange();
            })

            /* this._userService.currentUser = data;
            if (data.Role <= 2 && data.Entity.length>0) {
                const entityObj = {entities: data.Entity};
                this._entitieService.getEntitiesById(entityObj).subscribe((response) => {
                    this.entities = response.entities;
                    this.selectedEntity = this.entities[0]._id;
                    this.entitySelectionChange()
                });

            } else {
                
                this._entitieService.getAllEntities().subscribe((response) => {
                    this.entities = response.entities;
                    this.selectedEntity = this.entities[0]._id;
                    this.entitySelectionChange();
                })
            } */

        })

    }

    ngOnInit() {
        
    }

    entitySelectionChange(){

        
        let entities = [];

        this.currentScores = {
            currentScore :0,
            averageScore: 0,
            goal: 0,
            pourcentage : 0
        }
        this.doughnutChartData3 = [[0,0]];
        this.lineChartData[0].data = [];
        this.lineChartLabels = [];
        this.barChartData[0].data = [0];
        this.barChartLabels = [''];

        this._entitieService.getEntityById(this.selectedEntity).subscribe(ENT=>{
            this.currentScores.goal = ENT.entity.ScoreGoal;
            this._entityService.entities.forEach(entity=>{
                if(entity._id === this.selectedEntity){
                    this.selectedEntityName = entity.Code +' | '+ entity.Description;
                    entities.push(this.selectedEntity)
                    //this.currentScores.goal = entity.ScoreGoal;
                }
                if(ENT.entity.SubEntities.includes(entity._id)){
                    entities.push(this.selectedEntity);
                }
            })
    
        })


        this._reportsService.getHistory(this.selectedEntity).subscribe(response=>{
            this.lineChartData[0].data = [];
            this.lineChartLabels = [];

            response.data.forEach(scoreElement => {
                this.lineChartLabels.push(this.changeDateFormat(scoreElement.date));
                this.lineChartData[0].data.push(scoreElement.score);
            });
        })

        this._reportsService.getScore(this.selectedEntity).subscribe(response=>{
            this.currentScores.currentScore = response.data.currentScore;
            this.currentScores.averageScore = response.data.scoresAvg;

            if(this.currentScores.goal != 0) this.currentScores.pourcentage = Math.round(this.currentScores.currentScore / this.currentScores.goal * 1000)/100;


            this._selfAssessmentService.getSelfAssessmentsByEntities([this.selectedEntity]).subscribe(data=>{
                //console.log(data);
                //let goal = 0;
                //let count = 0;
                for (let ass of data.assessments){
                    if(ass.Status == 2){
                        this._dashboardService.getAssessmentsScores([ass._id]).subscribe(res => {
                            //goal += res.data[0].assesment_goals.goal;
                            //count++;
                            //this.currentScores.goal = Math.round(goal / count * 100 ) /100.0;

                            this.currentScores.goal = res.data[0].assesment_goals.goal

                            if(this.currentScores.goal === 0) {
                                this.currentScores.pourcentage = 0;
                            }else{
                                this.currentScores.pourcentage = Math.round(this.currentScores.averageScore / this.currentScores.goal * 1000 ) /10;
                            }
                        })
                    }
                }
                /*
                this._dashboardService.getAssessmentsScores([this.assessment._id]).subscribe(res => {
                    this.currentScores.goal = res.data[0].assesment_goals.goal;
                })*/
            })

            /* this.currentScores.pourcentage = Math.round(this.currentScores.averageScore / this.currentScores.goal * 1000 ) /10;
            if(this.currentScores.goal === 0) this.currentScores.pourcentage = 0; */
            this.doughnutChartData3 = [[this.currentScores.averageScore,this.currentScores.goal-this.currentScores.averageScore]]

            let ents = response.data.subEntities.allSubEntities;
            ents.push(this.selectedEntity)

            let data = [];
            this.barChartData[0].data = [0];
            this.barChartLabels = [this.selectedEntityName];

            this._selfAssessmentService.getLastPublishedAssessmentScore(ents).subscribe(response=>{
                if(response.data.length > 0) {
                    this.barChartData[0].data = [];
                    this.barChartLabels = [];
                }
                response.data.forEach((element) => {
                    data.push(element.score);
                    this.barChartData[0].data = data;
                    this.barChartLabels.push(element.entity.Code)
                });
            })
        })

        this.doughnutChartLabels2 = ['In Progress','Finished','Published'];

        this.doughnutChartData = [
            [0,0]
        ]

        this._actionsService.getEntityOpenActions(this.selectedEntity).subscribe(response=>{
            this.doughnutChartLabels = ['In Progress','Finished'];
            this.doughnutChartData= [[response.data.inProgress.length,response.data.finished.length]];
        })

        this.doughnutChartData2 = [
            [0,0,0]
        ]

        this._selfAssessmentService.getAssessmentState(this.selectedEntity).subscribe(response=>{
            console.log([response.data.inProgressAssessments,response.data.finishedAssessments,response.data.publishedAssessments])
            this.doughnutChartData2 = [
                [response.data.inProgressAssessments,response.data.finishedAssessments,response.data.publishedAssessments]
            ]
        })
    }


    public randomize(): void {
        // Only Change 3 values
        const data = [
            Math.round(Math.random() * 100),
            59,
            80,
            (Math.random() * 100),
            56,
            (Math.random() * 100),
            40];
        this.barChartData[0].data = data;
    }

    changeDateFormat(DATE){
        let date = new Date(DATE);
            let years = date.getFullYear();
    
            var months,hours,minutes,seconds,days;
    
            if(date.getMonth()<9){
              months = '0'+ (date.getMonth()+1);
            } else{
              months = (date.getMonth()+1);
            }
            if(date.getDate()<10){
              days = '0'+ (date.getDate());
            } else{
               days = (date.getDate());
            }
            if(date.getMinutes()<10){
               minutes = '0'+ (date.getMinutes());
            } else{
               minutes = (date.getMinutes());
            }
            if(date.getHours()<10){
               hours = '0'+ (date.getHours());
            } else{
               hours = (date.getHours());
            }
    
            let stringDate = years +'-'+months+"-"+days+' '+hours+':'+minutes;
    
            return stringDate;
    }


    private transformer = (node: Entit, level: number) => {
        return {
            expandable: !!node.children && node.children.length > 0,
            name: ''+node.entity.Code+' | '+node.entity.Description,
            entity: node.entity,
            level: level
        };
    }

    treeControl = new FlatTreeControl<ExampleFlatNode>(
        node => node.level, node => node.expandable);

    treeFlattener = new MatTreeFlattener(
        this.transformer, node => node.level, node => node.expandable, node => node.children);

    treeSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    hasChild = (_: number, node: ExampleFlatNode) => node.expandable;


    initData(){
        this.treeSource.data = [];
        TREE_DATA = [];
        let found = false;
        this.entities.forEach(entity=>{
            if(entity.Type === "Site"){
                found = true;
                TREE_DATA.push({entity: entity,children:[]});
                this.entities.forEach(sub=>{
                    for(var i =0 ; i<TREE_DATA[TREE_DATA.length-1].entity.SubEntities.length; i++ ){
                        if(sub._id === TREE_DATA[TREE_DATA.length-1].entity.SubEntities[i]){
                            TREE_DATA[TREE_DATA.length-1].children.push({entity:sub,children:[]});
                            let chilLen = TREE_DATA[TREE_DATA.length-1].children.length;
                            this.entities.forEach(sub1=>{
                                let len = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities.length;                                
                                for(var j = 0 ; j<len; j++ ){
                                    if(sub1._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities[j]){
                                        TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.push({entity:sub1,children:[]});
                                        let chilLen1 = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.length;
                                        this.entities.forEach(sub2=>{
                                            let len2 = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].entity.SubEntities.length;                                
                                            for(var k = 0 ; k<len2; k++ ){
                                                if(sub2._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].entity.SubEntities[k]){
                                                    TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].children.push({entity:sub2,children:[]});
                                                }
                                            }
                                        })
                                    } 
                                }
                            })
                        }    
                    }
                })
            }
        });
        if(!found){
            this.entities.forEach(entity=>{
                if(entity.Type === "Usine"){
                    found = true;
                    TREE_DATA.push({entity: entity,children:[]});
                    this.entities.forEach(sub=>{
                        for(var i =0 ; i<TREE_DATA[TREE_DATA.length-1].entity.SubEntities.length; i++ ){
                            if(sub._id === TREE_DATA[TREE_DATA.length-1].entity.SubEntities[i]){
                                TREE_DATA[TREE_DATA.length-1].children.push({entity:sub,children:[]});
                                let chilLen = TREE_DATA[TREE_DATA.length-1].children.length;
                                this.entities.forEach(sub1=>{
                                    let len = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities.length;                                
                                    for(var j = 0 ; j<len; j++ ){
                                        if(sub1._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].entity.SubEntities[j]){
                                            TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.push({entity:sub1,children:[]});
                                            /* let chilLen1 = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children.length;
                                            this.entities.forEach(sub2=>{
                                                let len2 = TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].entity.SubEntities.length;                                
                                                for(var k = 0 ; k<len2; k++ ){
                                                    if(sub2._id === TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].entity.SubEntities[k]){
                                                        TREE_DATA[TREE_DATA.length-1].children[chilLen-1].children[chilLen1-1].children.push({entity:sub2,children:[]});
                                                    }
                                                }
                                            }) */
                                        } 
                                    }
                                })
                            }    
                        }
                    })
                }
            });
        }
        if(!found){
            this.entities.forEach(entity=>{
                found = true;
                TREE_DATA.push({entity: entity,children:[]});
            });
        }
        this.treeSource.data = [];
        this.treeSource.data = TREE_DATA;
    }


}

  