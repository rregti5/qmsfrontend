import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AxesUpdateComponent } from './axes-update.component';

describe('AxesUpdateComponent', () => {
  let component: AxesUpdateComponent;
  let fixture: ComponentFixture<AxesUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AxesUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AxesUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
