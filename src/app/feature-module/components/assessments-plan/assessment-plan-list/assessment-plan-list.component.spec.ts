import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssessmentPlanListComponent } from './assessment-plan-list.component';

describe('AssessmentPlanListComponent', () => {
  let component: AssessmentPlanListComponent;
  let fixture: ComponentFixture<AssessmentPlanListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssessmentPlanListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssessmentPlanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
