import {Component, OnInit, EventEmitter, HostListener, Output} from '@angular/core';

@Component({
    selector: 'confirmation-modal',
    templateUrl: './confirmation-modal.component.html',
    styleUrls: ['./confirmation-modal.component.css']
})
export class ConfirmationModalComponent implements OnInit {

    @Output()
    cancelRemove: EventEmitter<boolean> = new EventEmitter();

    @Output()
    confirmRemove: EventEmitter<boolean> = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

    closeModal() {
        this.cancelRemove.emit(true);
    }

    onConfirmRemove = () => {
        this.confirmRemove.emit(true)
    }

    @HostListener('window:keydown', ['$event'])
    keyboardInput(event: any) {
        if (event.keyCode === 27) {
            this.closeModal();
        }
    }
}
