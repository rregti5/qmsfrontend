import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { AxesPdfComponent } from './axes-pdf/axes-pdf.component';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from 'app/material-module/material.module';
import {ChartsModule} from "ng2-charts";
import {AgmCoreModule} from "@agm/core";
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [AxesPdfComponent],
  imports: [
    CommonModule,
    ChartsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyATXRPfjS2JxrjkCMfxXJu5wBM5QpkSAKo'
    }),
    PublicRoutingModule,
    MaterialModule,
    FormsModule,
    BrowserModule,
    TranslateModule,
  ]
})
export class PublicModule { }
