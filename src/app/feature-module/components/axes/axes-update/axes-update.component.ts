import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {AppRoutesConstant} from "../../../../_constants/app-routes";
import {Observable} from "rxjs";
import {AxesModel, ScoreModel, AxesModelNew, ScoreModelNew} from "../../../models";
import {AxesService} from "../../../services";
import {addElement, ScoreDescirptionColorList} from "../../../../utility-module/shared-functions/common-functions";
import { TranslateService } from '@ngx-translate/core';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
    selector: 'app-axes-update',
    templateUrl: './axes-update.component.html',
    styleUrls: ['./axes-update.component.scss']
})

export class AxesUpdateComponent implements OnInit, OnDestroy {

    selectedAxe: AxesModel;
    isRemoveElement = false;
    updatedAxesParams: AxesModel;
    private subParams$: any;
    colorList = ScoreDescirptionColorList;

    constructor(private _router: Router,
                private _activeRoute: ActivatedRoute,
                private _axesService: AxesService,public translate: TranslateService) {
                    if(Cookie.get('language')!==null)
                    translate.setDefaultLang(Cookie.get('language'))
    }

    ngOnInit() {
        this.routeSubscriber();
    }

    getSingleAxe = (id): Observable<any> => {
        return this._axesService.getSingleAxeDetail(id);
    }

    onBack = () => {
        this._router.navigate(['/' + AppRoutesConstant.AXES_LIST]);
    }

    onAddElement = () => {
        this.selectedAxe.Elements.push(addElement());
        this.updateAxesApiCall(this.selectedAxe.id, this.selectedAxe).subscribe(response => {
            this.selectedAxe.__v = response.data.__v;
        }, error => {
            this.selectedAxe.Elements.pop();
        });
    }

    onRemoveElement = (event, elementIndex) => {
        event.stopPropagation();
        this.updatedAxesParams = JSON.parse(JSON.stringify(this.selectedAxe))
        this.updatedAxesParams.Elements = this.updatedAxesParams.Elements.filter((elem, index) => index !== elementIndex);
        this.isRemoveElement = true;
    }

    onChangeAttachment = (event, element) => {
        element.AttachementControl = event.value;
    }

    onClickSave = (axe: AxesModelNew, score: ScoreModelNew, elementIndex: number, scoreIndex: number) => {
        
        const description= score.Description || [];

        for(let i=0;i<description.length;i++){
            let scoreElementId = axe.id + '_' + elementIndex + '_' + score.Note+'_'+i;
            const currentScoreElement = document.getElementById(scoreElementId);
            axe.Elements[elementIndex].Scores[scoreIndex].Description[i] = currentScoreElement['value'];
        }
        const axeElementElement = document.getElementById(axe.id + '_' + elementIndex);
        axe.Elements[elementIndex].Description = axeElementElement['value'];

        const axeId = axe.id;
        const axeParams = axe;

        this.updateAxesApiCall(axeId, axeParams).subscribe(response => {
            this.selectedAxe.__v = response.data.__v;
        })

        // const scoreElementId = axe.id + '_' + elementIndex + '_' + score.Note;

        // const currentScoreElement = document.getElementById(scoreElementId);
        // const axeElementElement = document.getElementById(axe.id + '_' + elementIndex);

        // axe.Elements[elementIndex].Scores[scoreIndex].Description = currentScoreElement['value'];
        // axe.Elements[elementIndex].Description = axeElementElement['value'];

        // const axeId = axe.id;
        // const axeParams = axe;
        // console.log(axe)
        // this.updateAxesApiCall(axeId, axeParams).subscribe(response => {
        //     this.selectedAxe.__v = response.data.__v;
        // })
    }

    updateAxesApiCall = (axeId, axeParams) => {
        return this._axesService.updateAxes(axeId, axeParams)
    }

    onRemoveConfirm = () => {
        if (this.updatedAxesParams) {
            this.updateAxesApiCall(this.selectedAxe.id, this.updatedAxesParams)
                .subscribe(response => {
                    this.handleRemoveAxe(response);
                })
        }
    }

    handleRemoveAxe = (response) => {
        const updatedAxe: AxesModel = response.data;
        this.selectedAxe.Elements = updatedAxe.Elements;
        this.selectedAxe.__v = updatedAxe.__v;
        this.onHideConfirmationModal();
    }

    onHideConfirmationModal = () => {
        this.isRemoveElement = false;
    }

    ngOnDestroy() {
        if (this.subParams$) {
            this.subParams$.unsubscribe();
        }
    }

    handleSpacebar(event) {
        if (event.keyCode === 32) {
            event.stopPropagation();
        }
    }

    onClickAxe(event) {
        event.stopPropagation();
    }

    onBlurAxe(event, selectedAxe: AxesModel) {
        const currentValue = event.target.value;
        const axeId = selectedAxe.id;
        if (currentValue !== selectedAxe.Description) {
            //console.log(event.target.value);
            const axeParams: AxesModel = JSON.parse(JSON.stringify(selectedAxe));
            axeParams.Description = currentValue;
            this.updateAxesApiCall(axeId, axeParams).subscribe(response => {
                this.selectedAxe.Description = response.data.Description;
                this.selectedAxe.__v = response.data.__v;
            })
        }
        event.stopPropagation();
    }

    onBlurElement = (event, axe: AxesModel, elementIndex: number) => {

        const axeElementElement = document.getElementById(axe.id + '_' + elementIndex);

        const axeId = axe.id;
        const axeParams = axe;
        //console.log(axe)
        //console.log(axeParams)
        if(axeElementElement['value'] !== event.target.value){
            this.updateAxesApiCall(axeId, axeParams).subscribe(response => {
                this.selectedAxe.__v = response.data.__v;
            })
        }
        event.stopPropagation();
    }

    /**
     * router params subscriber
     */
    private routeSubscriber = () => {
        this.subParams$ = this._activeRoute.params.subscribe(params => {
            const axeId = params['id'];
            if (axeId) {
                this.getSingleAxe(axeId)
                    .subscribe(response => {
                        this.selectedAxe = response.data;
                    });
            }
        });
    };
}
