export class CommonRegexp {
  public static NUMERIC_REGEXP = '^[0-9]*$';
  public static ALPHA_NUMERIC_REGEXP = '^[A-Za-z0-9]*$';
  public static ALPHA_SPACE_REGEXP = '^[A-Za-z ]*$';
  public static ALPHA_NUMERIC_SPACE_REGEXP = '^[A-Za-z0-9 ]*$';
  public static DOCUMENT_NAME_SPACE_REGEXP = '^[A-Za-z0-9 ,_\/-]*$';
  public static COMPANY_NUMBER_REGEXP = '^[A-Za-z0-9,_\/-]*$';
  public static COMPANY_NAME_SPACE_REGEXP = '^[A-Za-z0-9 ,.&-]*$';
  public static ALPHA_NUMERIC_DOT_REGEXP = '^[A-Za-z0-9. ]*$';
  public static USER_NAME_REGEXP = '^[A-Za-z0-9-_. ]*$';
  public static EMAIL_ADDRESS_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  public static PASSWORD_REGEXP = /^(?=.*[a-zA-Z])(?=.*[A-Za-z])(?=.*\d)[a-zA-Z\d#?!@$%^&*-]{8,}$/;
  public static WEBSITE_REGEXP = '(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})';
  public static ADDRESS_REGEX = '^[A-Za-z0-9-.,\/&@\' ]*$';
}

export class ValidationConstant {

  public REQUIRED = ` is required`;
  public EMAIL_LENGTH_VARIABLE = `6 to 100`;

  public USERNAME = `UserName'${this.REQUIRED}.replace('is', 'are')`;
  public VALID_EMAIL = `Enter valid email address`;
  public EMAIL_LENGTH = `Email address length between ${this.EMAIL_LENGTH_VARIABLE} characters`;
  public VALID_DOCUMENT_TYPE = `Valid Document type is PDF`;
  public VALID_IMAGE_TYPE = `Valid Image types are png, jpg, jpeg`;
}
